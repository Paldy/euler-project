package com.euler.problems.p034x

import org.scalatest.{FlatSpec, Matchers}

class Test_034x extends FlatSpec with Matchers {
  "Problem solver #345" should "return 3315 as the 'matrix sum' of the input matrix" in {
    MatrixSum.solve("7 53 183 439 863\n497 383 563 79 973\n287 63 343 169 583\n627 343 773 959 943\n767 473 103 699 303") should be (3315)
  }

  "Problem solver #346" should "say 171 is the sum of strong repunits below 50" in {
    StrongRepunits.solve(50) should be (171)
  }

  it should "say 15864 is the sum of strong repunits below 1000" in {
    StrongRepunits.solve(1000) should be (15864)
  }

  "Problem solver #347" should "return 2262 as the sum of distinct M(p,q,100)" in {
    LargestIntegerDivisibleByTwoPrimes.solve(100) should be (2262)
  }
}
