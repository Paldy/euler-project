package com.euler.problems.p003x

import org.scalatest.{FlatSpec, Matchers}

class Test_003x extends FlatSpec with Matchers {
  "Problem solver #30" should "return 19316 for 4" in {
    DigitFifthPower.solve(4) should be (19316)
  }

  "Problem solver #31" should "return 3 for desired sum of 4 and list of 1 and 2" in {
    CoinSums.solve(List(1, 2), 4) should be (3)
  }

  "Problem solver #32" should "return 52 for 5" in {
    PandigitalProducts.solve(5) should be (52)
  }

  "Problem solver #33" should "return 100" in {
    DigitCancellingFractions.solve() should be (100)
  }

  "Problem solver #34" should "return 100" in {
    DigitFactorials.solve(150) should be (145)
  }

  "Problem solver #35" should "return 13 for 100" in {
    CircularPrimes.solve(100) should be (13)
  }

  "Problem solver #36" should "return 1772 for 1000" in {
    DoubleBasePalindromes.solve(1000) should be (1772)
  }

  "Problem solver #37" should "return 499 for 5" in {
    TruncatablePrimes.solve(5) should be (499)
  }

  "Problem solver #38" should "return 918273645 for 200" in {
    PandigitalMultiples.solve(200) should be (918273645)
  }

  "Problem solver #39" should "return 3 for 120" in {
    IntegerRightTriangles.solve(120) should be (120)
  }
}
