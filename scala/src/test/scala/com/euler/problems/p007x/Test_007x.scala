package com.euler.problems.p007x

import org.scalatest.{FlatSpec, Matchers}

class Test_007x extends FlatSpec with Matchers {
  "Problem solver #70" should "say 21 is the number under 100 for which the totient is its permutation and n/totient is the minimum" in {
    TotientPermutation.solve(100) should be (21)
  }

  "Problem solver #71" should "return 2 for 8" in {
    OrderedFractions.solve(8, (3, 7)) should be (2)
  }

  "Problem solver #72" should "say 21 reduced proper fractions are for d = 8" in {
    CountingFractions.solve(8) should be (21)
  }

  "Problem solver #73" should "count 3 proper fractions between 1/3 and 1/2 for d=3" in {
    CountingFractionsInARange.solve(8, (1, 3), (1, 2)) should be (3)
  }

  "Problem solver #74" should "count exactly 1 term below 70 that has 5 non-repeating terms" in {
    DigitFactorialChains.solve(70, 5) should be (1)
  }

  "Problem solver #76" should "return 6 for 5" in {
    CountingSummations.solve(5) should be (6)
  }

  "Problem solver #79" should "return for default parameters" in {
    PasscodeDerivation.solve() should be (73162890)
  }
}
