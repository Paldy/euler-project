package com.euler.problems.p002x

import org.scalatest.{FlatSpec, Matchers}

class Test_002x extends FlatSpec with Matchers {
  "Problem solver #20" should "return 27 for 10" in {
    FactorialDigitSum.solve(10) should be (27)
  }

  "Problem solver #21" should "return 504 for 300" in {
    AmicableNumbers.solve(300) should be (504)
  }

  "Problem solver #22" should "return 159 for \"COLIN\", \"COLIN\"" in {
    NamesScores.solve("\"COLIN\",\"COLIN\"") should be (159)
  }

  "Problem solver #23" should "return 210 for 20" in {
    NonAbundantSums.solve(20) should be (210)
  }

  "Problem solver #24" should "return 120 for 2 and 4" in {
    LexicographicPermutations.solve((0 to 2).toList, 4) should be (120)
  }

  "Problem solver #25" should "return 12 for 3" in {
    _1000DigitFibonacciNumber.solve(3) should be (12)
  }

  "Problem solver #26" should "return 7 for 10" in {
    ReciprocalCycles.solve(10) should be (7)
  }

  "Problem solver #27" should "return -235 for 50" in {
    QuadraticPrimes.solve(50) should be (-235)
  }

  "Problem solver #28" should "return 101 for 5" in {
    NumberSpiralDiagonals.solve(5) should be (101)
  }

  "Problem solver #29" should "return 15 for 5" in {
    DistinctPowers.solve(5) should be (15)
  }
}
