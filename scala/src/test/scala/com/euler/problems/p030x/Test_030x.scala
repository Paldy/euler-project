package com.euler.problems.p030x

import org.scalatest.{FlatSpec, Matchers}

class Test_030x extends FlatSpec with Matchers {
  "Problem solver #301" should "return 1 for n <= 1, because X(1,2,3)=0" in {
    Nim.solve(1) should be (1)
  }

  it should "return 1 for n <= 100" in {
    Nim.solve(100) should be (33)
  }
}
