package com.euler.problems.p006x

import org.scalatest.{FlatSpec, Matchers}

class Test_006x extends FlatSpec with Matchers {
  "Problem solver #60" should "say 792 is the lowest sum of 4 primes, from which any two of them concatenated gives a prime" in {
    PrimePairSets.solve(4) should be (792)
  }

  "Problem solver #61" should "say 19291 is the sum of cyclical figurate numbers for 3 polygonal types" in {
    CyclicalFigurateNumbers.solve(3) should be (19291)
  }

  "Problem solver #62" should "return 41063625 as the smallest cube which has exactly 3 permutations of its digits which are also cube" in {
    CubicPermutations.solve(3) should be (41063625)
  }

  "Problem solver #63" should "return 49 for default parameters" in {
    PowerfulDigitCounts.solve() should be (49)
  }

  "Problem solver #64" should "say there are exactly 4 continued fractions of square roots of numbers below 13 which have an odd period" in {
    OddPeriodSquareRoots.solve(13) should be (4)
  }

  "Problem solver #65" should "return 17 as the sum of digits of the numerator for the 10th convergent of e" in {
    ConvergentsOfE.solve(10) should be (17)
  }

  "Problem solver #67" should "return 23 for 3\n7 4\n2 4 6\n8 5 9 3" in {
    MaximumPathSum_II.solve("3\n7 4\n2 4 6\n8 5 9 3") should be (23)
  }

  "Problem solver #69" should "return 6 for 10" in {
    TotientMaximum.solve(10) should be (6)
  }
}
