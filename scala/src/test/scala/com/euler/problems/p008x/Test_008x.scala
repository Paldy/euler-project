package com.euler.problems.p008x

import org.scalatest.{FlatSpec, Matchers}

class Test_008x extends FlatSpec with Matchers {
  "Problem solver #80" should "say 475 is the sum of the first 100 digits of the irrational square roots for numbers under 2" in {
    SquareRootDigitalExpansion.solve(2, 100) should be (475)
  }

  "Problem solver #81" should "return 2427 for the specified input" in {
    PathSumTwoWays.solve("131,673,234,103,18\n201,96,342,965,150\n630,803,746,422,111\n537,699,497,121,956\n805,732,524,37,331") should be (2427)
  }

  "Problem solver #82" should "say 994 is the minimal path sum for the specified input" in {
    PathSumThreeWays.solve("131,673,234,103,18\n201,96,342,965,150\n630,803,746,422,111\n537,699,497,121,956\n805,732,524,37,331") should be (994)
  }

  "Problem solver #83" should "say 2297 is the minimal path sum for the specified input" in {
    PathSumFourWays.solve("131,673,234,103,18\n201,96,342,965,150\n630,803,746,422,111\n537,699,497,121,956\n805,732,524,37,331") should be (2297)
  }

  "Problem solver #85" should "return 6 as the area of the grid which has the nearest to 18 rectangles" in {
    CountingRectangles.solve(18) should be (6)
  }

  "Problem solver #87" should "say that there are 4 numbers below 50 that can be expressed as a sum of a prime square, prime cube, prime fourth power" in {
    PrimePowerTriples.solve(50, List(2, 3, 4)) should be (4)
  }

  "Problem solver #89" should "say 4 characters were save from converting to the minimal form List(\"XIIIIII\"))" in {
    RomanNumerals.solve(List("XIIIIII")) should be (4)
  }
}
