package com.euler.problems.p049x

import org.scalatest.{FlatSpec, Matchers}

class Test_049x extends FlatSpec with Matchers {
  "Problem solver #493" should "return 1,666 for 2 colors, 2 balls for each color and picking 2 balls from the urn" in {
    UnderTheRainbow.solve(2, 2, 2) should be (BigDecimal("1.666666666"))
  }
}
