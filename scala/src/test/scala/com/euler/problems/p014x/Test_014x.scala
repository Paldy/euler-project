package com.euler.problems.p014x

import org.scalatest.{FlatSpec, Matchers}

class Test_014x extends FlatSpec with Matchers {
  "Problem solver #145" should "say 120 reversible numbers are below 1000" in {
    ReversibleNumbers.solve(1000) should be (120)
  }
}
