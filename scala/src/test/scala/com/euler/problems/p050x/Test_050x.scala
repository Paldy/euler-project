package com.euler.problems.p050x

import org.scalatest.{FlatSpec, Matchers}

class Test_050x extends FlatSpec with Matchers {
  "Problem solver #500" should "say 120 is the smallest number having 2^4 divisors" in {
    Problem500.solve(4) should be (120)
  }

  "Problem solver #504" should "say 42 is the number of quadrilaterals containing square number of lattice points for m=4" in {
    SquareOnTheInside.solve(4) should be (42)
  }
}
