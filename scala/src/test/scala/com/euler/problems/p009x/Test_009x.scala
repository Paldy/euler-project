package com.euler.problems.p009x

import org.scalatest.{FlatSpec, Matchers}

class Test_009x extends FlatSpec with Matchers {
  "Problem solver #92" should "return 100 for 80" in {
    SquareDigitChains.solve(100) should be (80)
  }

  "Problem solver #97" should "return 8739992577 for default parameters" in {
    LargeNonMersennePrime.solve() should be (8739992577L)
  }

  "Problem solver #99" should "return 0 for List((632382,518061), (519432,525806))" in {
    LargestExponential.solve(Iterator((632382, 518061), (519432, 525806))) should be (1)
  }
}
