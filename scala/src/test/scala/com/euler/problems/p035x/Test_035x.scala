package com.euler.problems.p035x

import org.scalatest.{FlatSpec, Matchers}

class Test_035x extends FlatSpec with Matchers {
  "Problem solver #357" should "return 71 for 30" in {
    PrimeGeneratingIntegers.solve(30) should be (71)
  }
}
