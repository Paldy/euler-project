package com.euler.problems.p010x

import org.scalatest.{FlatSpec, Matchers}

class Test_010x extends FlatSpec with Matchers {
  "Problem solver #102" should "return 1, because triangle ((-340,495), (-153,-910), (835,-947)) contains origin" in {
    TriangleContainment.solve(List(((-340,495), (-153,-910), (835,-947)))) should be (1)
  }

  it should "return 0, because triangle ((-175,41), (-421,-714), (574,-645)) does not contain origin" in {
    TriangleContainment.solve(List(((-175,41), (-421,-714), (574,-645)))) should be (0)
  }

  it should "return 1, because triangle ((0,1), (-1,-1), (1,-1)) contains origin" in {
    TriangleContainment.solve(List(((0,1), (-1,-1), (1,-1)))) should be (1)
  }

  it should "return 1, because triangle ((2,2), (-2,-2), (1,-1)) contains origin" in {
    TriangleContainment.solve(List(((2,2), (-2,-2), (1,-1)))) should be (1)
  }
}
