package com.euler.problems.p000x

import org.scalatest.{FlatSpec, Matchers}

class Test_000x extends FlatSpec with Matchers {

  "Problem solver #1" should "return 23 for 10" in {
    MultiplesOf3And5.solve(10) should be (23)
  }

  "Problem solver #2" should "return 19 for parameter 10" in {
    EvenFibonacciNumbers.solve(10) should be (10)
  }

  "Problem solver #3" should "return 29 for 13195" in {
    LargestPrimeFactor.solve(13195) should be (29)
  }

  "Problem solver #4" should "return 9009 for 2" in {
    LargestPalindromeProduct.solve(2) should be (9009)
  }

  "Problem solver #5" should "return 2520 for 10" in {
    SmallestMultiple.solve(10) should be (2520)
  }

  "Problem solver #6" should "return 2640 for 10" in {
    SumSquareDifference.solve(10) should be (2640)
  }

  "Problem solver #7" should "return 13 for 6" in {
    _10001stPrime.solve(6) should be (13)
  }

  "Problem solver #8" should "return 5832 for 4" in {
    LargestProductInASeries.solve(4) should be (5832)
  }

  it should "return 23514624000 for 13" in {
    LargestProductInASeries.solve(13) should be (23514624000L)
  }

  "Problem solver #9" should "return 60 for 12" in {
    SpecialPythagoreanTriplet.solve(12) should be (60)
  }
}
