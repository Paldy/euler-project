package com.euler.problems.p038x

import org.scalatest.{FlatSpec, Matchers}

class Test_038x extends FlatSpec with Matchers {
  "Problem solver #381" should "say 480 is the sum of S(p)-s for 5 <= p < 100" in {
    PrimeKFactorial.solve(100) should be (480)
  }

  "Problem solver #387" should "return 90619 for 4 digits" in {
    HarshadNumbers.solve(4) should be (90619)
  }
}
