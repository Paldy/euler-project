package com.euler.problems.p020x

import org.scalatest.{FlatSpec, Matchers}

class Test_020x extends FlatSpec with Matchers {
  "Problem solver #205" should "say that Peter wins with 0.5731441 possibility" in {
    DiceGame.solve() should be (BigDecimal("0.5731441"))
  }
}
