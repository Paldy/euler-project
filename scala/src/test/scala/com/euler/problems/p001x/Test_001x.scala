package com.euler.problems.p001x

import org.scalatest.{FlatSpec, Matchers}

class Test_001x extends FlatSpec with Matchers {
  "Problem solver #10" should "return 17 for 10" in {
    SummationOfPrimes.solve(10) should be (17)
  }

  "Problem solver #11" should "return 99 for 1" in {
    LargestProductInAGrid.solve(1) should be (99)
  }

  "Problem solver #12" should "return 28 for 5" in {
    HighlyDivisibleTriangularNumber.solve(5) should be (28)
  }

  "Problem solver #13" should "return 51 for '205\\n205\\n100' and first 2 digits" in {
    LargeSum.solve(2, "205\n205\n100") should be ("51")
  }

  "Problem solver #14" should "return 9 for 13" in {
    LongestCollatzSequence.solve(13) should be (9)
  }

  "Problem solver #15" should "return 6 for 2" in {
    LatticePaths.solve(2) should be (6)
  }

  "Problem solver #16" should "return 26 for 15" in {
    PowerDigitSum.solve(2, 15) should be (26)
  }

  "Problem solver #17" should "return 19 for 5" in {
    NumberLetterCounts.solve(5) should be (19)
  }

  "Problem solver #18" should "return 23 for 3\n7 4\n2 4 6\n8 5 9 3" in {
    MaximumPathSum_I.solve("3\n7 4\n2 4 6\n8 5 9 3") should be (23)
  }

  "Problem solver #19" should "return 171 for default values" in {
    CountingSundays.solve() should be (171)
  }
}
