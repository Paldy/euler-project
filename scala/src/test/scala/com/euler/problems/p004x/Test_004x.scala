package com.euler.problems.p004x

import org.scalatest.{FlatSpec, Matchers}

class Test_004x extends FlatSpec with Matchers {
  "Problem solver #40" should "return 1 for List(10)" in {
    ChampernownesConstant.solve(List(10)) should be (1)
  }

  it should "return 1 for List(1)" in {
    ChampernownesConstant.solve(List(1)) should be (1)
  }

  "Problem solver #41" should "return 4231 for 4" in {
    PandigitalPrime.solve(4) should be (4231)
  }

  "Problem solver #42" should "return 162 for default parameters" in {
    CodedTriangleNumbers.solve() should be (162)
  }

  "Problem solver #43" should "return 16695334890 for default parameters" in {
    SubStringDivisibility.solve() should be (16695334890L)
  }

  "Problem solver #44" should "return 5482660 for default parameters" in {
    PentagonNumbers.solve() should be (5482660)
  }

  "Problem solver #45" should "return 40755 for 0" in {
    TriangularPentagonalAndHexagonal.solve(1) should be (40755)
  }

  "Problem solver #46" should "return 5777 for default parameters" in {
    GoldbachsOtherConjecture.solve() should be (5777)
  }

  "Problem solver #47" should "return 14 for 2" in {
    DistinctPrimesFactors.solve(2) should be (14)
  }

  it should "return 644 for 3" in {
    DistinctPrimesFactors.solve(3) should be (644)
  }

  "Problem solver #48" should "return 405071317 for 10" in {
    SelfPowers.solve(10) should be (405071317)
  }

  "Problem solver #49" should "return 296962999629 for default parameters" in {
    PrimePermutations.solve() should be (296962999629L)
  }
}
