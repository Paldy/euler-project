package com.euler.problems.p005x

import com.euler.common.poker.{Card, Hand}
import org.scalatest.{FlatSpec, Matchers}

class Test_005x extends FlatSpec with Matchers {
  "Problem solver #50" should "return 41 for 100" in {
    ConsecutivePrimeSum.solve(100) should be (41)
  }

  it should "return 953 for 1000" in {
    ConsecutivePrimeSum.solve(1000) should be (953)
  }

  "Problem solver #51" should "return 13 as the smallest prime part of a 6 prime value family" in {
    PrimeDigitReplacements.solve(6) should be (13)
  }

  it should "return 56003 as the smallest prime part of a 7 prime value family" in {
    PrimeDigitReplacements.solve(7) should be (56003)
  }

  "Problem solver #52" should "return 125874 for 2" in {
    PermutedMultiples.solve(2) should be (125874)
  }

  "Problem solver #53" should "return 2 for 9 and 5" in {
    CombinatoricSelections.solve(9, 5) should be (2)
  }

  "Problem solver #54" should "return 3 for test data" in {
    PokerHands.solve(
      List(
        (Hand(Card("5H").get, Card("5C").get, Card("6S").get, Card("7S").get, Card("KD").get),
          Hand(Card("2C").get, Card("3S").get, Card("8S").get, Card("8D").get, Card("TD").get)),
        (Hand(Card("2D").get, Card("9C").get, Card("AS").get, Card("AH").get, Card("AC").get),
          Hand(Card("3D").get, Card("6D").get, Card("7D").get, Card("TD").get, Card("QD").get)),
        (Hand(Card("5D").get, Card("8C").get, Card("9S").get, Card("JS").get, Card("AC").get),
          Hand(Card("2C").get, Card("5C").get, Card("7D").get, Card("8S").get, Card("QH").get)),
        (Hand(Card("4D").get, Card("6S").get, Card("9H").get, Card("QH").get, Card("QC").get),
          Hand(Card("3D").get, Card("6D").get, Card("7H").get, Card("QD").get, Card("QS").get)),
        (Hand(Card("2H").get, Card("2D").get, Card("4C").get, Card("4D").get, Card("4S").get),
          Hand(Card("3C").get, Card("3D").get, Card("3S").get, Card("9S").get, Card("9D").get))
      )) should be (3)
  }

  "Problem solver #55" should "return 1 for numbers under 200 with maximum 50 iterations" in {
    LychrelNumbers.solve(200, 50) should be (1)
  }

  "Problem solver #56" should "return 45 for 10" in {
    PowerfulDigitSum.solve(10) should be (45)
  }

  "Problem solver #57" should "return 1 for 10" in {
    SquareRootConvergents.solve(10) should be (1)
  }

  "Problem solver #58" should "return 11 for 50" in {
    SpiralPrimes.solve(50) should be (11)
  }

  "Problem solver #59" should "return 2711 for the input" in {
    XorDecryption.solve(List(2,79,19,6,28,68,16,6,16,15,79,35,8,11,72,71,14,10,3,79,12,2,79,19,6,28,68,32,0,0,73,79,86,71), ('a'.toInt to 'z'.toInt).toList, 3) should be (2711)
  }
}
