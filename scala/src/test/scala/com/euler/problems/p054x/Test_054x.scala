package com.euler.problems.p054x

import org.scalatest.{FlatSpec, Matchers}

class Test_054x extends FlatSpec with Matchers {
  "Problem solver #549" should "return 2012 for 100" in {
    DivisibilityOfFactorials.solve(100) should be (2012)
  }
}
