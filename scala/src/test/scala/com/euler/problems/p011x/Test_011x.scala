package com.euler.problems.p011x

import org.scalatest.{FlatSpec, Matchers}

class Test_011x extends FlatSpec with Matchers {
  "Problem solver #112" should "return 538 as the first number, when bouncy numbers first reach the proportion of 50%" in {
    BouncyNumbers.solve(50) should be (538)
  }

  it should "return 21780 as the first number, when bouncy numbers first reach the proportion of 90%" in {
    BouncyNumbers.solve(90) should be (21780)
  }
}
