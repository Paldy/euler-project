package com.euler.common.numerals

import org.scalatest.{FlatSpec, Matchers}

class RomanTest extends FlatSpec with Matchers {
  "Roman parser" should "return 16 for XIIIIII" in {
    Roman.parse("XIIIIII") should be (Some(16))
  }

  "Roman parser" should "return 16 for XVI" in {
    Roman.parse("XVI") should be (Some(16))
  }

  "Roman parser" should "return 19 for XIX" in {
    Roman.parse("XIX") should be (Some(19))
  }

  "Roman parser" should "return 49 for XLIX" in {
    Roman.parse("XLIX") should be (Some(49))
  }

  "Roman parser" should "return 1 for I" in {
    Roman.parse("I") should be (Some(1))
  }

  "Roman parser" should "return 5 for V" in {
    Roman.parse("V") should be (Some(5))
  }

  "Roman parser" should "return 10 for X" in {
    Roman.parse("X") should be (Some(10))
  }

  "Roman parser" should "return 50 for L" in {
    Roman.parse("L") should be (Some(50))
  }

  "Roman parser" should "return 100 for C" in {
    Roman.parse("C") should be (Some(100))
  }

  "Roman parser" should "return 500 for D" in {
    Roman.parse("D") should be (Some(500))
  }

  "Roman parser" should "return 1000 for M" in {
    Roman.parse("M") should be (Some(1000))
  }

  "Roman creator" should "return M for 1000" in {
    Roman.create(1000) should be ("M")
  }

  "Roman creator" should "return D for 500" in {
    Roman.create(500) should be ("D")
  }

  "Roman creator" should "return C for 100" in {
    Roman.create(100) should be ("C")
  }

  "Roman creator" should "return L for 50" in {
    Roman.create(50) should be ("L")
  }

  "Roman creator" should "return X for 10" in {
    Roman.create(10) should be ("X")
  }

  "Roman creator" should "return V for 5" in {
    Roman.create(5) should be ("V")
  }

  "Roman creator" should "return I for 1" in {
    Roman.create(1) should be ("I")
  }

  "Roman creator" should "return XIV for 16" in {
    Roman.create(16) should be ("XVI")
  }

  "Roman creator" should "return XLIX for 49" in {
    Roman.create(49) should be ("XLIX")
  }
}