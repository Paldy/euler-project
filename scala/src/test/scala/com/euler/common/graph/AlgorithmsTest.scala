package com.euler.common.graph

import org.scalatest.{FlatSpec, Matchers}

class AlgorithmsTest extends FlatSpec with Matchers {
  "The Dijsktra algorithm" should "return the shortest path" in {
    Algorithms.dijkstra(Map(
      "a" -> List((7, "b"), (9, "c"), (14, "f")),
      "b" -> List((10, "c"), (15, "d")),
      "c" -> List((11, "d"), (2, "f")),
      "d" -> List((6, "e")),
      "e" -> List((9, "f")),
      "f" -> Nil
    ), "a", "e") should be ((26, List("a", "c", "d", "e")))
  }
}
