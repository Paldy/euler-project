package com.euler.common

import org.scalatest.{FlatSpec, Matchers}

class FractionTest extends FlatSpec with Matchers {
  "Fraction" should "have reduced numerator 5 for Fraction(10, 2)" in {
    Fraction(10, 2).reducedNumerator should be (5)
  }

  it should "have reduced denominator 1 for Fraction(10, 10)" in {
    Fraction(10, 10).reducedNumerator should be (1)
  }

  it should "implement equality between Fraction(60, 36) and Fraction((5, 3))" in {
    Fraction(60, 36).equals(Fraction((5, 3))) should be (true)
  }

  it should "result in Fraction(33, 5) when adding Fraction(13, 5) and Fraction(4, 1)" in {
    Fraction(13, 5) + Fraction(4, 1) should be (Fraction(33, 5))
  }

  it should "result in Fraction(13, 5) when adding Fraction(13, 5) and Fraction(0, 1)" in {
    Fraction(13, 5) + Fraction(0, 1) should be (Fraction(13, 5))
  }

  it should "return Fraction(5, 13) as the reciprocal of Fration(13, 5)" in {
    Fraction(13, 5).reciprocal should be (Fraction(5, 13))
  }
}
