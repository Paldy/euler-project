package com.euler.common

import org.scalatest.{FlatSpec, Matchers}

class PrimeFactorsTest extends FlatSpec with Matchers {
  "Least Common Multiple" should "be 2 for 2 and 1" in {
    PrimeFactors(2).leastCommonMultiple(1).toLong should be (2)
  }

  it should "be 2 for 2 and 2" in {
    PrimeFactors(2).leastCommonMultiple(2).toLong should be (2)
  }

  it should "be 4 for 2 and 4" in {
    PrimeFactors(2).leastCommonMultiple(4).toLong should be (4)
  }

  "All divisors" should "be 1, 2, 5, 10 for 10" in {
    PrimeFactors(10).allDivisors.size should be (4)
    PrimeFactors(10).allDivisors.toSet should be (Set(1, 2, 5, 10))
  }

  it should "be 1, 2, 3, 4, 5, 6, 10, 12, 15, 20, 30, 60 for 60" in {
    PrimeFactors(60).allDivisors.size should be (12)
    PrimeFactors(60).allDivisors.toSet should be (Set(1, 2, 3, 4, 5, 6, 10, 12, 15, 20, 30, 60))
  }

  "To list" should "be 3, 2, 2, for 12" in {
    PrimeFactors(12).toList should be (List(3, 2, 2))
  }
}
