package com.euler.common.fast

import org.scalatest.{FlatSpec, Matchers}

class FastPrimeFactorsTest extends FlatSpec with Matchers {
  "Fast Prime Factors toList" should "be 5, 2, 2 for 20" in {
    FastPrimes(20).computePrimeFactors(20).toList should be (List(5, 2, 2))
  }

  "Fast Prime Factors toList" should "be empty for 1" in {
    FastPrimes(20).computePrimeFactors(1).toList should be (List())
  }

  "Fast Prime Factors toInt" should "be 1 for 1" in {
    FastPrimes(20).computePrimeFactors(1).toInt should be (1)
  }

  "Fast Prime Factors all divisors" should "be 1, 2, 5, 10 for 10" in {
    FastPrimes(10).computePrimeFactors(10).allDivisors.size should be (4)
    FastPrimes(10).computePrimeFactors(10).allDivisors.toSet should be (Set(1, 2, 5, 10))
  }

  "Fast Prime Factors toMap" should "be Map(2 -> 2, 3 -> 1) for 12" in {
    FastPrimes(12).computePrimeFactors(12).toMap should be (Map(2 -> 2, 3 -> 1))
  }
}
