package com.euler.common.fast

import org.scalatest.{FlatSpec, Matchers}

class FastPrimesTest extends FlatSpec with Matchers {
  "Fast primes" should "say 13 is a prime" in {
    FastPrimes(13).isPrime(13) should be (true)
  }

  it should "say 14 is not a prime" in {
    FastPrimes(14).isPrime(14) should be (false)
  }
}
