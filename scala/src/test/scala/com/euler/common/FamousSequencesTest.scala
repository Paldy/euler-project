package com.euler.common

import java.time.LocalDate

import org.scalatest.{FlatSpec, Matchers}

class FamousSequencesTest extends FlatSpec with Matchers {

  "Fibonacci" should "be 1, 1, 2, 3, 5, 8 which are smaller than 10" in {
    FamousSequences.fibonacci(1, 1).takeWhile(_ < 10) should be (Array(1, 1, 2, 3, 5, 8))
  }

  "Fibonacci with BigInts" should "be 1, 1, 2, 3, 5, 8 which are smaller than 10" in {
    FamousSequences.fibonacciBigInt(1, 1).takeWhile(_ < 10) should be (Array(1, 1, 2, 3, 5, 8))
  }

  "Primes" should "be 2, 3, 5, 7 which are smaller than 10" in {
    FamousSequences.primes.takeWhile(_ < 10) should be (Array(2, 3, 5, 7))
  }

  "Triangle numbers" should "be 1, 3, 6, 10, 15 which are smaller than 20" in {
    FamousSequences.triangleNumbers.takeWhile(_ < 20) should be (Array(1, 3, 6, 10, 15))
  }

  "Days" should "be (2001, 1, 1), (2001, 1, 2), (2001, 1, 3) for the first 3 dates from (2001, 1, 1)" in {
    FamousSequences.days(LocalDate.of(2001, 1, 1)).take(3) should be (Array(LocalDate.of(2001, 1, 1), LocalDate.of(2001, 1, 2), LocalDate.of(2001, 1, 3)))
  }

  "Abundants" should "be 12, 18 for the first 2" in {
    FamousSequences.abundants.take(2) should be (Array(12, 18))
  }

  "Pentagon numbers" should "be 1, 5, 12, 22, 35 the first 5 numbers in the stream" in {
    FamousSequences.pentagonNumbers.take(5) should be (Array(1, 5, 12, 22, 35))
  }

  "Hexagon numbers" should "be 1, 6, 15, 28, 45 the first 5 numbers in the stream" in {
    FamousSequences.hexagonNumbers.take(5) should be (Array(1, 6, 15, 28, 45))
  }

  "Composites" should "be 4, 6, 8, 9, 10 the first 5 numbers in the stream" in {
    FamousSequences.composites.take(5) should be (Array(4, 6, 8, 9, 10))
  }
}
