package com.euler.common

import org.scalatest.{FlatSpec, Matchers}

class CombinatoricsTest extends FlatSpec with Matchers {
  "The number of permutation of multisets" should "be 34650 for (4, 4, 2, 1)" in {
    Combinatorics.computePermutationOfMultisets(List(4, 4, 2, 1)) should be (34650)
  }

  "Generate permutations with repetition" should "be (1,1), (1,2), (2,1), (2,2) for (1,2)" in {
    Combinatorics.permutationsWithRepetition(List(1, 2), 2) should be (List(List(1,1), List(1,2), List(2,1), List(2,2)))
  }
}
