package com.euler.common.poker

import org.scalatest.{FlatSpec, Matchers}

class HandTest extends FlatSpec with Matchers {
  "Hand of 5H 5C 6S 7S KD" should "be lower than hand of 2C 3S 8S 8D TD" in {
    Hand(Card("5H").get, Card("5C").get, Card("6S").get, Card("7S").get, Card("KD").get) <
      Hand(Card("2C").get, Card("3S").get, Card("8S").get, Card("8D").get, Card("TD").get) should
      be (true)
  }

  "Hand of 2D 9C AS AH AC" should "be lower than hand of 3D 6D 7D TD QD" in {
    Hand(Card("2D").get, Card("9C").get, Card("AS").get, Card("AH").get, Card("AC").get) <
      Hand(Card("3D").get, Card("6D").get, Card("7D").get, Card("TD").get, Card("QD").get) should
      be (true)
  }

  "Hand of 5D 8C 9S JS AC" should "be lower than hand of 2C 5C 7D 8S QH" in {
    Hand(Card("5D").get, Card("8C").get, Card("9S").get, Card("JS").get, Card("AC").get) >
      Hand(Card("2C").get, Card("5C").get, Card("7D").get, Card("8S").get, Card("QH").get) should
      be (true)
  }

  "Hand of 4D 6S 9H QH QC" should "be lower than hand of 3D 6D 7H QD QS" in {
    Hand(Card("4D").get, Card("6S").get, Card("9H").get, Card("QH").get, Card("QC").get) >
      Hand(Card("3D").get, Card("6D").get, Card("7H").get, Card("QD").get, Card("QS").get) should
      be (true)
  }

  "Hand of 2H 2D 4C 4D 4S" should "be lower than hand of 3C 3D 3S 9S 9D" in {
    Hand(Card("2H").get, Card("2D").get, Card("4C").get, Card("4D").get, Card("4S").get) >
      Hand(Card("3C").get, Card("3D").get, Card("3S").get, Card("9S").get, Card("9D").get) should
      be (true)
  }
}
