package com.euler.common.poker

import org.scalatest.{FlatSpec, Matchers}

class CardTest extends FlatSpec with Matchers {
  "Card from string" should "be 3 of Hearts for 3H" in {
    Card("3H").get should be (Card(CardSuit.HEARTS, CardValue._3))
  }

  it should "be Queen of Clubs for QC" in {
    Card("QC").get should be (Card(CardSuit.CLUBS, CardValue.QUEEN))
  }
}
