package com.euler.common

import org.scalatest.{FlatSpec, Matchers}

class TriangleTest extends FlatSpec with Matchers {
  "Generated triangles" should "be (3, 3, 4) and (2, 4, 4) for 10" in {
    Triangle.generateFromPerimeter(10) should be (List(Triangle(3, 3, 4), Triangle(2, 4, 4)))
  }

  it should "be (3, 4, 5) and (2, 5, 5) for 12" in {
    Triangle.generateFromPerimeter(12) should be (List(Triangle(3, 4, 5), Triangle(2, 5, 5)))
  }

  "Triangle is Pythagorean" should "be true for (3, 4, 5)" in {
    Triangle(3, 4, 5).isPythagorean should be (true)
  }

  it should "be false for (3, 3, 5)" in {
    Triangle(3, 3, 5).isPythagorean should be (false)
  }
}
