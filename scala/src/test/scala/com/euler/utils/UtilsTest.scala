package com.euler.utils

import org.scalatest.{FlatSpec, Matchers}

class UtilsTest extends FlatSpec with Matchers {
  "Palindrome" should "be 'abccba'" in {
    "abccba".isPalindrome should be(true)
  }

  it should "not be 123331" in {
    123331L.isPalindrome should be(false)
  }

  it should "be 123321" in {
    123321L.isPalindrome should be(true)
  }

  it should "be 1235321" in {
    1235321L.isPalindrome should be(true)
  }

  "Digits" should "be 1, 2, 3 for 123" in {
    BigInt(123).toDigits should be(Array(1, 2, 3))
  }

  "Amicable" should "be 220" in {
    220L.isAmicable should be(true)
  }

  it should "be 284" in {
    284L.isAmicable should be(true)
  }

  it should "be 2" in {
    2L.isAmicable should be(false)
  }

  "Abundant" should "be 12" in {
    12L.isAbundant should be(true)
  }

  it should "not be 6" in {
    6L.isAbundant should be(false)
  }

  "PrimeFactors" should "be Map() for 1" in {
    1L.primeFactors.toMap should be(Map.empty)
  }

  it should "be Map(2 -> 1) for 2" in {
    2L.primeFactors.toMap should be(Map(2 -> 1))
  }

  "Number of all divisors" should "be 6 for 28" in {
    28L.numberOfAllDivisors should be(6)
  }

  "Proper divisors" should "be 1, 2, 5 for 10" in {
    10L.properDivisors.size should be(3)
    10L.properDivisors.toSet should be(Set(1, 2, 5))
  }

  "All divisors" should "be 1, 2, 5, 10 for 10" in {
    10L.divisors.size should be(4)
    10L.divisors.toSet should be(Set(1, 2, 5, 10))
  }

  "IndexedSeq of Ints to BigInt" should "be 123 for IndexedSeq(1, 2, 3)" in {
    IndexedSeq(1, 2, 3).toBigInt should be(123)
  }

  "Highest Common Divisor" should "be 1 for 2 and 1" in {
    2L.highestCommonDivisor(1) should be(1)
  }

  it should "be 4 for 12 and 20" in {
    12L.highestCommonDivisor(20) should be(4)
  }

  "Factorial" should "be 6 for 3" in {
    3L.factorial should be(6)
  }

  it should "be 120 for 5" in {
    5L.factorial should be(120)
  }

  it should "be 1 for 0" in {
    0L.factorial should be(1)
  }

  it should "be 20 for 5 until 3" in {
    5L.factorialUntil(3) should be(20)
  }

  "IndexedSeq of Ints to Long" should "be 123 for IndexedSeq(1, 2, 3)" in {
    IndexedSeq(1, 2, 3).toLong should be(123)
  }

  "Character index" should "be 1 for A" in {
    'A'.letterIndex should be(1)
  }

  it should "be 8 for H" in {
    'H'.letterIndex should be(8)
  }

  "Digits fold left" should "be 6 for 123 and (sum, digit) => sum + digit" in {
    123L.foldDigits(0)((sum, digit) => sum + digit) should be(6)
  }

  "Totient number" should "be 4 for 10" in {
    10L.totient should be(4)
  }

  "Choose" should "be 10 for 3 from 5" in {
    5L.choose(3) should be(10)
  }

  "Power set of Set(1, 2)" should "be Set(Set(1), Set(2), Set(1, 2))" in {
    Set(1, 2).powerSet should be(Set(Set(1), Set(2), Set(1, 2)))
  }

  "Sum of 2 biggest Ints" should "not be -3" in {
    Stream.from(Int.MaxValue, -1).take(2).sumLong should not be(-3)
  }
}
