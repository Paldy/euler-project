package com.euler.format

import com.euler.format.Solve.{problems, solve}
import com.euler.format.Tag.Tag

import scala.collection.mutable

class PerformanceTest {
  private val filterByTag: mutable.Set[Tag] = mutable.Set.empty[Tag]
  private var filterByDifficulty: Option[Map[Int, Problem]] = None

  private var filterSlowSolutions: Boolean = false

  def filterByDifficulty(difficulty: Int): this.type = {
    filterByDifficulty = Solve.problemsByDifficulty.get(difficulty)
    this
  }

  def filterByTag(tag: Tag): this.type = {
    filterByTag.add(tag)
    this
  }

  def withoutSlow(): this.type = {
    filterSlowSolutions = true
    this
  }

  def run(): Unit = {
    filterByDifficulty.getOrElse(problems)
      .filter(problem => filterByTag.forall(problem._2.tags().contains(_)))
      .filter(!filterSlowSolutions || !_._2.tags().contains(Tag.SLOW))
      .keySet
      .map(solve)
      .toList
      .sortBy(_._3)
      .foreach(ps => println("Problem #" + ps._1 + " lasted " + ps._3 / 1000000.0 + " ms"))
  }
}

object PerformanceTest {
  def apply(): PerformanceTest = new PerformanceTest()
}
