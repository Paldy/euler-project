package com.euler.format

import com.euler.common.GenericFraction
import com.euler.problems.p000x._
import com.euler.problems.p001x._
import com.euler.problems.p002x._
import com.euler.problems.p003x._
import com.euler.problems.p004x._
import com.euler.problems.p005x._
import com.euler.problems.p006x._
import com.euler.problems.p007x._
import com.euler.problems.p008x._
import com.euler.problems.p009x._
import com.euler.problems.p010x._
import com.euler.problems.p011x._
import com.euler.problems.p014x._
import com.euler.problems.p020x._
import com.euler.problems.p030x._
import com.euler.problems.p034x._
import com.euler.problems.p035x._
import com.euler.problems.p038x._
import com.euler.problems.p049x._
import com.euler.problems.p050x._
import com.euler.problems.p054x._

import scala.collection.mutable

object Solve {
  val problems_5: Map[Int, Problem] = (
    new mutable.MapBuilder[Int, Problem, Map[Int, Problem]](Map.empty)
      += 1 -> MultiplesOf3And5
      += 2 -> EvenFibonacciNumbers
      += 3 -> LargestPrimeFactor
      += 4 -> LargestPalindromeProduct
      += 5 -> SmallestMultiple
      += 6 -> SumSquareDifference
      += 7 -> _10001stPrime
      += 8 -> LargestProductInASeries
      += 9 -> SpecialPythagoreanTriplet
      += 10 -> SummationOfPrimes
      += 11 -> LargestProductInAGrid
      += 12 -> HighlyDivisibleTriangularNumber
      += 13 -> LargeSum
      += 14 -> LongestCollatzSequence
      += 15 -> LatticePaths
      += 16 -> PowerDigitSum
      += 17 -> NumberLetterCounts
      += 18 -> MaximumPathSum_I
      += 19 -> CountingSundays
      += 20 -> FactorialDigitSum
      += 21 -> AmicableNumbers
      += 22 -> NamesScores
      += 23 -> NonAbundantSums
      += 24 -> LexicographicPermutations
      += 25 -> _1000DigitFibonacciNumber
      += 26 -> ReciprocalCycles
      += 27 -> QuadraticPrimes
      += 28 -> NumberSpiralDiagonals
      += 29 -> DistinctPowers
      += 30 -> DigitFifthPower
      += 31 -> CoinSums
      += 32 -> PandigitalProducts
      += 33 -> DigitCancellingFractions
      += 34 -> DigitFactorials
      += 35 -> CircularPrimes
      += 36 -> DoubleBasePalindromes
      += 37 -> TruncatablePrimes
      += 38 -> PandigitalMultiples
      += 39 -> IntegerRightTriangles
      += 40 -> ChampernownesConstant
      += 41 -> PandigitalPrime
      += 42 -> CodedTriangleNumbers
      += 43 -> SubStringDivisibility
      += 44 -> PentagonNumbers
      += 45 -> TriangularPentagonalAndHexagonal
      += 46 -> GoldbachsOtherConjecture
      += 47 -> DistinctPrimesFactors
      += 48 -> SelfPowers
      += 49 -> PrimePermutations
      += 50 -> ConsecutivePrimeSum
      += 52 -> PermutedMultiples
      += 53 -> CombinatoricSelections
      += 55 -> LychrelNumbers
      += 56 -> PowerfulDigitSum
      += 57 -> SquareRootConvergents
      += 58 -> SpiralPrimes
      += 59 -> XorDecryption
      += 63 -> PowerfulDigitCounts
      += 67 -> MaximumPathSum_II
      += 79 -> PasscodeDerivation
      += 92 -> SquareDigitChains
      += 97 -> LargeNonMersennePrime
      += 206 -> ConcealedSquare
    ).result()

  val problems_10: Map[Int, Problem] = (
    new mutable.MapBuilder[Int, Problem, Map[Int, Problem]](Map.empty)
      += 54 -> PokerHands
      += 69 -> TotientMaximum
      += 71 -> OrderedFractions
      += 76 -> CountingSummations
      += 81 -> PathSumTwoWays
      += 99 -> LargestExponential
      += 357 -> PrimeGeneratingIntegers
      += 387 -> HarshadNumbers
      += 493 -> UnderTheRainbow
      += 549 -> DivisibilityOfFactorials
    ).result()

  val problems_15: Map[Int, Problem] = (
    new mutable.MapBuilder[Int, Problem, Map[Int, Problem]](Map.empty)
      += 51 -> PrimeDigitReplacements
      += 62 -> CubicPermutations
      += 65 -> ConvergentsOfE
      += 73 -> CountingFractionsInARange
      += 74 -> DigitFactorialChains
      += 85 -> CountingRectangles
      += 102 -> TriangleContainment
      += 112 -> BouncyNumbers
      += 205 -> DiceGame
      += 301 -> Nim
      += 345 -> MatrixSum
      += 346 -> StrongRepunits
      += 347 -> LargestIntegerDivisibleByTwoPrimes
      += 381 -> PrimeKFactorial
      += 500 -> Problem500
      += 504 -> SquareOnTheInside
    ).result()

  val problems_20: Map[Int, Problem] = (
    new mutable.MapBuilder[Int, Problem, Map[Int, Problem]](Map.empty)
      += 60 -> PrimePairSets
      += 61 -> CyclicalFigurateNumbers
      += 64 -> OddPeriodSquareRoots
      += 70 -> TotientPermutation
      += 72 -> CountingFractions
      += 80 -> SquareRootDigitalExpansion
      += 82 -> PathSumThreeWays
      += 87 -> PrimePowerTriples
      += 89 -> RomanNumerals
      += 145 -> ReversibleNumbers
    ).result()


  val problems_25: Map[Int, Problem] = (
    new mutable.MapBuilder[Int, Problem, Map[Int, Problem]](Map.empty)
      += 83 -> PathSumFourWays
    ).result()

  val problemsByDifficulty: Map[Int, Map[Int, Problem]] = (
    new mutable.MapBuilder[Int, Map[Int, Problem], Map[Int, Map[Int, Problem]]](Map.empty)
      += 5 -> problems_5
      += 10 -> problems_10
      += 15 -> problems_15
      += 20 -> problems_20
      += 25 -> problems_25
    ).result()

  val problems: Map[Int, Problem] = problemsByDifficulty.values.flatten.toMap

  def solve(problemNumber: Int) = {
    val t0 = System.nanoTime()
    val problemOutput = problems(problemNumber).solve()
    val t1 = System.nanoTime()
    (problemNumber, problemOutput, t1 - t0)
  }

  def solveAndPrint(problemNumber: Int) = {
    val problemSolved = solve(problemNumber)

    println("Problem #" + problemSolved._1 + ": " + problemSolved._2)
    println("Elapsed time: " + problemSolved._3 / 1000000.0 + " ms")
  }

  def main(args: Array[String]) = {
    solveAndPrint(145)
    //PerformanceTest().filterByDifficulty(15).run()
  }
}
