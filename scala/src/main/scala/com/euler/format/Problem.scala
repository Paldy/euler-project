package com.euler.format

import com.euler.format.Tag.Tag

trait Problem {
  def solve(): Any

  def tags(): Set[Tag] = Set.empty

  def answer(): Any
}