package com.euler.format

/**
  * Generating description for problems
  */
object Description {
  val description = "How many reversible numbers are there below one-billion?\nProblem 145\n\nSome positive integers n have the property that the sum [ n + reverse(n) ] consists entirely of odd (decimal) digits. For instance, 36 + 63 = 99 and 409 + 904 = 1313. We will call such numbers reversible; so 36, 63, 409, and 904 are reversible. Leading zeroes are not allowed in either n or reverse(n).\n\nThere are 120 reversible numbers below one-thousand.\n\nHow many reversible numbers are there below one-billion (109)?"

  def main(args: Array[String]) = {
    def process(l: List[String], prevEmpty: Boolean = false, r: String = ""): String = {
      def processLine(s: String, isEmpty: Boolean, prevEmpty: Boolean): String = {
        (if (!prevEmpty && !isEmpty) "<br>" else "") + "\n  * " + (if (isEmpty) "<p>" else s)
      }

      if (l.isEmpty) r
      else process(l.tail, l.head.trim.isEmpty, r + processLine(l.head, l.head.trim.isEmpty, prevEmpty))
    }

    println(process(description.split("\n").toList))
  }
}
