package com.euler.problems.p005x

import com.euler.common.Combinatorics
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

import scala.io.Source

/**
  * XOR decryption<br>
  * Problem 59
  * <p>
  * Each character on a computer is assigned a unique code and the preferred standard is ASCII (American Standard Code for Information Interchange). For example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.
  * <p>
  * A modern encryption method is to take a text file, convert the bytes to ASCII, then XOR each byte with a given value, taken from a secret key. The advantage with the XOR function is that using the same encryption key on the cipher text, restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.
  * <p>
  * For unbreakable encryption, the key is the same length as the plain text message, and the key is made up of random bytes. The user would keep the encrypted message and the encryption key in different locations, and without both "halves", it is impossible to decrypt the message.
  * <p>
  * Unfortunately, this method is impractical for most users, so the modified method is to use a password as a key. If the password is shorter than the message, which is likely, the key is repeated cyclically throughout the message. The balance for this method is using a sufficiently long password key for security, but short enough to be memorable.
  * <p>
  * Your task has been made easy, as the encryption key consists of three lower case characters. Using cipher.txt (right click and 'Save Link/Target As...'), a file containing the encrypted ASCII codes, and the knowledge that the plain text must contain common English words, decrypt the message and find the sum of the ASCII values in the original text.
  */
object XorDecryption extends Problem {

  val commonEnglishWords = Source.fromFile("resources/other/commonEnglishWords.txt").getLines().toSet

  def loadFileToString(s: String) = Source.fromFile(s).getLines.mkString.split(",").map(_.toInt).toList

  def decode(encrypted: List[Int], cipher: List[Int]): String =
    encrypted.zipWithIndex.map({ case (char, index) => (char ^ cipher(index % cipher.size)).toChar }).mkString

  def numberOfCommonWords(s: String): Int = s.split("\\W").toStream.count(commonEnglishWords.contains)

  def decodeAndCountCommonWords(encrypted: List[Int], cipher: List[Int]) = {
    val decodedText = decode(encrypted, cipher)
    (decodedText, numberOfCommonWords(decodedText))
  }

  def solve(encrypted: List[Int], cipherChars: List[Int], cipherLength: Int) =
    Combinatorics.permutationsWithRepetition(cipherChars, cipherLength)
      .map(decodeAndCountCommonWords(encrypted, _)).maxBy(_._2)._1.chars.sum

  override def solve(): Any = solve(loadFileToString("resources/input/p059_cipher.txt"), ('a'.toInt to 'z'.toInt).toList, 3)

  override def tags(): Set[Tag] = Set(Tag.COMBINATORICS)

  override def answer(): Any = 107359
}
