package com.euler.problems.p005x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Permuted multiples<br>
  * Problem 52
  * <p>
  * It can be seen that the number, 125874, and its double, 251748, contain exactly the same digits, but in a different order.
  * <p>
  * Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.
  */
object PermutedMultiples extends Problem {
  def sameDigits(n: Int, maxMultiple: Int): Boolean = {
    val searchDigits = n.toString.sorted
    (2 to maxMultiple).forall(multiple => (n * multiple).toString.sorted == searchDigits)
  }

  def solve(maxMultiple: Int): Int = Stream.from(1).filter(sameDigits(_, maxMultiple)).head

  override def solve(): Any = solve(6)

  override def tags(): Set[Tag] = Set(Tag.DIGITS)

  override def answer(): Any = 142857
}
