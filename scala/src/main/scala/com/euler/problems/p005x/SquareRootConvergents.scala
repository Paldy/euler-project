package com.euler.problems.p005x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

/**
  * Square root convergents<br>
  * Problem 57
  * <p>
  * It is possible to show that the square root of two can be expressed as an infinite continued fraction.
  * <p>
  * √ 2 = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) = 1.414213...
  * <p>
  * By expanding this for the first four iterations, we get:
  * <p>
  * 1 + 1/2 = 3/2 = 1.5<br>
  * 1 + 1/(2 + 1/2) = 7/5 = 1.4<br>
  * 1 + 1/(2 + 1/(2 + 1/2)) = 17/12 = 1.41666...<br>
  * 1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...
  * <p>
  * The next three expansions are 99/70, 239/169, and 577/408, but the eighth expansion, 1393/985, is the first example where the number of digits in the numerator exceeds the number of digits in the denominator.
  * <p>
  * In the first one-thousand expansions, how many fractions contain a numerator with more digits than denominator?
  */
object SquareRootConvergents extends Problem {

  val squareRootFractions = {
    def innerSquareRootFractions(previousFraction: (BigInt, BigInt) = (1, 1)): Stream[(BigInt, BigInt)] = {
      val fraction = (previousFraction._1 + 2 * previousFraction._2, previousFraction._1 + previousFraction._2)
      fraction #:: innerSquareRootFractions(fraction)
    }

    innerSquareRootFractions()
  }

  def solve(max: Int): Int =
    squareRootFractions.take(max).count(fraction => fraction._1.toString.length > fraction._2.toString.length)

  override def solve(): Any = solve(1000)

  override def tags(): Set[Tag] = Set(Tag.CONTINUED_FRACTIONS)

  override def answer(): Any = 153
}
