package com.euler.problems.p005x

import com.euler.common.{FamousSequences, NumberConstant, StreamLong}
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.utils._

/**
  * Consecutive prime sum<br>
  * Problem 50
  * <p>
  * The prime 41, can be written as the sum of six consecutive primes:<br>
  * 41 = 2 + 3 + 5 + 7 + 11 + 13
  * <p>
  * This is the longest sum of consecutive primes that adds to a prime below one-hundred.
  * <p>
  * The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.
  * <p>
  * Which prime, below one-million, can be written as the sum of the most consecutive primes?
  */
object ConsecutivePrimeSum extends Problem {

  def solve(limit: Long): Long = {
    def mostConsecutivePrimesToReachLimit(ps: Stream[Long] = FamousSequences.primes, sum: Long = 0, mostConsecutivePrimes: Int = 0): Int = {
      val newSum = sum + ps.head
      if (newSum >= limit) mostConsecutivePrimes
      else mostConsecutivePrimesToReachLimit(ps.tail, newSum, mostConsecutivePrimes + 1)
    }

    (for (numberOfConsecutivePrimes <- Stream.from(mostConsecutivePrimesToReachLimit(), -1).takeWhile(_ > 1);
          sumOfPrimes <- Stream.from(0).map(i => FamousSequences.primes.slice(i, numberOfConsecutivePrimes + i).sum).takeWhile(_ < limit))
      yield sumOfPrimes).find(_.isPrime).get
  }

  override def solve(): Any = solve(NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.PRIMES)

  override def answer(): Any = 997651
}
