package com.euler.problems.p005x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Spiral primes<br>
  * Problem 58
  * <p>
  * Starting with 1 and spiralling anticlockwise in the following way, a square spiral with side length 7 is formed.
  * <p>
  * 37 36 35 34 33 32 31<br>
  * 38 17 16 15 14 13 30<br>
  * 39 18  5  4  3 12 29<br>
  * 40 19  6  1  2 11 28<br>
  * 41 20  7  8  9 10 27<br>
  * 42 21 22 23 24 25 26<br>
  * 43 44 45 46 47 48 49
  * <p>
  * It is interesting to note that the odd squares lie along the bottom right diagonal, but what is more interesting is that 8 out of the 13 numbers lying along both diagonals are prime; that is, a ratio of 8/13 ≈ 62%.
  * <p>
  * If one complete new layer is wrapped around the spiral above, a square spiral with side length 9 will be formed. If this process is continued, what is the side length of the square spiral for which the ratio of primes along both diagonals first falls below 10%?
  */
object SpiralPrimes extends Problem {

  val spiralPrimes = {
    def checkPrimesInCorners(sideLength: Long): Long = {
      var numberOfPrimes = 0
      if ((sideLength * sideLength - sideLength + 1).isPrime) numberOfPrimes += 1
      if ((sideLength * sideLength - 2 * sideLength + 2).isPrime) numberOfPrimes += 1
      if ((sideLength * sideLength - 3 * sideLength + 3).isPrime) numberOfPrimes += 1
      numberOfPrimes
    }

    def nextSpiralPrime(sideLength: Long = 3, numberOfPrimes: Long = 0): Stream[(Long, Long)] = {
      val newNumberOfPrimes = numberOfPrimes + checkPrimesInCorners(sideLength)
      (sideLength, newNumberOfPrimes) #:: nextSpiralPrime(sideLength + 2, newNumberOfPrimes)
    }

    nextSpiralPrime()
  }

  def solve(percent: Int): Long = spiralPrimes.filter({ case (sideLength, numberOfPrimes) => (100 * numberOfPrimes) / (sideLength * 2 - 1) < percent }).head._1

  override def solve(): Any = solve(10)

  override def tags(): Set[Tag] = Set(Tag.NUMBER_SPIRAL, Tag.PRIMES)

  override def answer(): Any = 26241
}