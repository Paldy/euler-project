package com.euler.problems.p005x

import com.euler.common.{Combinatorics, FamousSequences, NumberConstant}
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.utils._

import scala.annotation.tailrec

/**
  * Prime digit replacements<br>
  * Problem 51
  * <p>
  * By replacing the 1st digit of the 2-digit number *3, it turns out that six of the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.
  * <p>
  * By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit number is the first example having seven primes among the ten generated numbers, yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993. Consequently 56003, being the first member of this family, is the smallest prime with this property.
  * <p>
  * Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits) with the same digit, is part of an eight prime value family.
  */
object PrimeDigitReplacements extends Problem {
  def createMasks(digits: IndexedSeq[Int], digitToReplace: Int): List[Long] = {
    @tailrec
    def inner(rest: IndexedSeq[Int], ret: List[IndexedSeq[Int]]): List[IndexedSeq[Int]] = {
      if (rest.isEmpty) ret
      else {
        val newRet =
          for (maskedValue <- if (rest.last == digitToReplace) List(0, 1) else List(0); mask <- ret)
            yield maskedValue +: mask
        inner(rest.init, newRet)
      }
    }

    inner(digits, List(IndexedSeq.empty)).map(_.toLong).filter(_ != 0)
  }

  def solve(groupSize: Int): Long = {
    def computeGroupSize(p: Long): Int = {
      val digits = p.toDigits

      (for (d <- digits.distinct; mask <- createMasks(digits, d))
        yield 1 + (d + 1 to 9).count(multiplier => (p + mask * (multiplier - d)).isPrime)).max
    }

    FamousSequences.primes
      .dropWhile(_ < 10)
      .find(computeGroupSize(_) == groupSize)
      .get
  }

  override def solve(): Any = solve(8)

  override def tags(): Set[Tag] = Set(Tag.PRIMES, Tag.DIGITS)

  override def answer(): Any = 121313

  def main(args: Array[String]) {
    Combinatorics.permutationsWithRepetition(NumberConstant.DecimalDigits.toList, 8).foreach(println(_))
  }
}
