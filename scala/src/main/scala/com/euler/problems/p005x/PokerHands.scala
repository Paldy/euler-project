package com.euler.problems.p005x

import com.euler.common.poker.{Card, Hand}
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

import scala.io.Source

/**
  * Poker hands<br>
  * Problem 54
  * <p>
  * In the card game poker, a hand consists of five cards and are ranked, from lowest to highest, in the following way:
  * <p>
  *     High Card: Highest value card.<br>
  *     One Pair: Two cards of the same value.<br>
  *     Two Pairs: Two different pairs.<br>
  *     Three of a Kind: Three cards of the same value.<br>
  *     Straight: All cards are consecutive values.<br>
  *     Flush: All cards of the same suit.<br>
  *     Full House: Three of a kind and a pair.<br>
  *     Four of a Kind: Four cards of the same value.<br>
  *     Straight Flush: All cards are consecutive values of same suit.<br>
  *     Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
  * <p>
  * The cards are valued in the order:<br>
  * 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.
  * <p>
  * If two players have the same ranked hands then the rank made up of the highest value wins; for example, a pair of eights beats a pair of fives (see example 1 below). But if two ranks tie, for example, both players have a pair of queens, then highest cards in each hand are compared (see example 4 below); if the highest cards tie then the next highest cards are compared, and so on.
  * <p>
  * Consider the following five hands dealt to two players:<br>
  * Hand	 	Player 1	 	Player 2	 	Winner<br>
  * 1	 	5H 5C 6S 7S KD<br>
  * Pair of Fives<br>
  * 	 	2C 3S 8S 8D TD<br>
  * Pair of Eights<br>
  * 	 	Player 2<br>
  * 2	 	5D 8C 9S JS AC<br>
  * Highest card Ace<br>
  * 	 	2C 5C 7D 8S QH<br>
  * Highest card Queen<br>
  * 	 	Player 1<br>
  * 3	 	2D 9C AS AH AC<br>
  * Three Aces<br>
  * 	 	3D 6D 7D TD QD<br>
  * Flush with Diamonds<br>
  * 	 	Player 2<br>
  * 4	 	4D 6S 9H QH QC<br>
  * Pair of Queens<br>
  * Highest card Nine<br>
  * 	 	3D 6D 7H QD QS<br>
  * Pair of Queens<br>
  * Highest card Seven<br>
  * 	 	Player 1<br>
  * 5	 	2H 2D 4C 4D 4S<br>
  * Full House<br>
  * With Three Fours<br>
  * 	 	3C 3D 3S 9S 9D<br>
  * Full House<br>
  * with Three Threes<br>
  * 	 	Player 1
  * <p>
  * The file, poker.txt, contains one-thousand random hands dealt to two players. Each line of the file contains ten cards (separated by a single space): the first five are Player 1's cards and the last five are Player 2's cards. You can assume that all hands are valid (no invalid characters or repeated cards), each player's hand is in no specific order, and in each hand there is a clear winner.
  * <p>
  * How many hands does Player 1 win?
  */
object PokerHands extends Problem {
  def loadHandsFromFile(file: String): List[(Hand, Hand)] =
    Source.fromFile(file).getLines.map(line => {
      val cards = line.split(" ")
      (Hand(Card(cards(0)).get, Card(cards(1)).get, Card(cards(2)).get, Card(cards(3)).get, Card(cards(4)).get),
        Hand(Card(cards(5)).get, Card(cards(6)).get, Card(cards(7)).get, Card(cards(8)).get, Card(cards(9)).get))
    }).toList

  def solve(listOfHands: List[(Hand, Hand)]): Int = listOfHands.count(hands => hands._1 > hands._2)

  override def solve(): Any = solve(loadHandsFromFile("resources/input/p054_poker.txt"))

  override def tags(): Set[Tag] = Set(Tag.PLAYING_CARDS, Tag.POKER)

  override def answer(): Any = 376
}
