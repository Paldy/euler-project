package com.euler.problems.p005x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.utils._

/**
  * Powerful digit sum<br>
  * Problem 56
  * <p>
  * A googol (10<sup>100</sup>) is a massive number: one followed by one-hundred zeros; 100<sup>100</sup> is almost unimaginably large: one followed by two-hundred zeros. Despite their size, the sum of the digits in each number is only 1.
  * <p>
  * Considering natural numbers of the form, a<sup>b</sup>, where a, b < 100, what is the maximum digital sum?
  */
object PowerfulDigitSum extends Problem {

  def solve(max: Int): Long = (for (a <- 1 to max; b <- 1 to max) yield BigInt(a).pow(b).toDigits.sum).max

  override def solve(): Any = solve(100)

  override def tags(): Set[Tag] = Set(Tag.DIGITS)

  override def answer(): Any = 972
}
