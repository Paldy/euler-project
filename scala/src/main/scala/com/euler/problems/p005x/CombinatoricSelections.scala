package com.euler.problems.p005x

import com.euler.common.Combinatorics
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Combinatoric selections<br>
  * Problem 53
  * <p>
  * There are exactly ten ways of selecting three from five, 12345:
  * <p>
  * 123, 124, 125, 134, 135, 145, 234, 235, 245, and 345
  * <p>
  * In combinatorics, we use the notation, 5C3 = 10.
  * <p>
  * In general,<br>
  * nCr = 	<br>
  * n!<br>
  * r!(n−r)!<br>
  * 	,where r ≤ n, n! = n×(n−1)×...×3×2×1, and 0! = 1.
  * <p>
  * It is not until n = 23, that a value exceeds one-million: 23C10 = 1144066.
  * <p>
  * How many, not necessarily distinct, values of  nCr, for 1 ≤ n ≤ 100, are greater than one-million?
  */
object CombinatoricSelections extends Problem {

  def solve(greater: Int, maxN: Int) =
    (for (n <- 1 to maxN; r <- n to 0 by -1)
      yield n.toLong.choose(r)
      ).count(_ > greater)

  override def solve(): Any = solve(1000000, 100)

  override def tags(): Set[Tag] = Set(Tag.COMBINATORICS)

  override def answer(): Any = 4075
}
