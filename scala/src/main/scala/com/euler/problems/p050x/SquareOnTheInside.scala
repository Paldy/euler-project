package com.euler.problems.p050x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

import scala.collection.mutable

/**
  * Square on the Inside<br>
  * Problem 504
  * <p>
  * Let ABCD be a quadrilateral whose vertices are lattice points lying on the coordinate axes as follows:
  * <p>
  * A(a, 0), B(0, b), C(−c, 0), D(0, −d), where 1 ≤ a, b, c, d ≤ m and a, b, c, d, m are integers.
  * <p>
  * It can be shown that for m = 4 there are exactly 256 valid ways to construct ABCD. Of these 256 quadrilaterals, 42 of them strictly contain a square number of lattice points.
  * <p>
  * How many quadrilaterals ABCD strictly contain a square number of lattice points for m = 100?
  */
object SquareOnTheInside extends Problem {

  def solve(m: Int) = {
    val cacheHCD = mutable.Map[(Int, Int), Int]()

    for (
      x <- 1 to m;
      y <- 1 to m
    ) cacheHCD((x, y)) = x.highestCommonDivisor(y).toInt

    def isSquareNumber(n: Long): Boolean = {
      val sqrtN = math.sqrt(n).round
      sqrtN * sqrtN == n
    }

    def hasSquareNumberOfLattice(a: Int, b: Int, c: Int, d: Int): Boolean = {
      val allPointsInBigSquare = (a + c) * (b + d)

      val pointsOnAB = cacheHCD((a, b))
      val pointsOnBC = cacheHCD((c, b))
      val pointsOnCD = cacheHCD((c, d))
      val pointsOnAD = cacheHCD((a, d))

      val pointsOnBorder = pointsOnAB + pointsOnBC + pointsOnCD + pointsOnAD
      val pointsInABCD = (allPointsInBigSquare - pointsOnBorder) / 2 + 1

      isSquareNumber(pointsInABCD)
    }

    (for (
      a <- 1 to m;
      b <- 1 to m;
      c <- 1 to m;
      d <- 1 to m
      if hasSquareNumberOfLattice(a, b, c, d)
    ) yield 1)
      .size
  }

  override def solve(): Any = solve(100)

  override def tags(): Set[Tag] = Set(Tag.CARTESIAN_PLANE, Tag.PICKS_THEOREM)

  override def answer(): Any = 694687
}
