package com.euler.problems.p050x

import com.euler.common.fast.FastPrimes
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

import scala.annotation.tailrec
import scala.collection.SortedSet
import scala.collection.immutable.TreeSet

/**
  * Problem 500!!!<br>
  * Problem 500
  * <p>
  * The number of divisors of 120 is 16.<br>
  * In fact 120 is the smallest number having 16 divisors.
  * <p>
  * Find the smallest number with 2<sup>500500</sup> divisors.<br>
  * Give your answer modulo 500500507.
  */
object Problem500 extends Problem {
  def solve(power: Int) = {
    val fastPrimes = FastPrimes(38500039)

    def multiplyAndModulo(m1: Long, m2: Long): Long = (m1 * m2) % 500500507

    @tailrec
    def inner(power: Int, primes: Stream[Int] = Stream.from(2).filter(fastPrimes.isPrime), squares: SortedSet[Long] = TreeSet.empty, ret: Long = 1): Long =
      (power, squares, primes) match {
        case (0, _, _) => ret
        case (_, xxs, pps) if xxs.nonEmpty && xxs.head < pps.head
        => inner(power - 1, pps, xxs.tail + (xxs.head * xxs.head), multiplyAndModulo(ret, xxs.head))
        case (_, xxs, p #:: ps)
        => inner(power - 1, ps, xxs + (p.toLong * p), multiplyAndModulo(ret, p))
      }

    inner(power)
  }

  override def solve(): Any = solve(500500)

  override def tags(): Set[Tag] = Set(Tag.DIVISORS, Tag.PRIMES)

  override def answer(): Any = 35407281
}
