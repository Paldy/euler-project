package com.euler.problems.p030x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Nim<br>
  * Problem 301
  * <p>
  * Nim is a game played with heaps of stones, where two players take it in turn to remove any number of stones from any heap until no stones remain.
  * <p>
  * We'll consider the three-heap normal-play version of Nim, which works as follows:<br>
  * - At the start of the game there are three heaps of stones.<br>
  * - On his turn the player removes any positive number of stones from any single heap.<br>
  * - The first player unable to move (because no stones remain) loses.
  * <p>
  * If (n1,n2,n3) indicates a Nim position consisting of heaps of size n1, n2 and n3 then there is a simple function X(n1,n2,n3) — that you may look up or attempt to deduce for yourself — that returns:
  * <p>
  *     zero if, with perfect strategy, the player about to move will eventually lose; or<br>
  *     non-zero if, with perfect strategy, the player about to move will eventually win.
  * <p>
  * For example X(1,2,3) = 0 because, no matter what the current player does, his opponent can respond with a move that leaves two heaps of equal size, at which point every move by the current player can be mirrored by his opponent until no stones remain; so the current player loses. To illustrate:<br>
  * - current player moves to (1,2,1)<br>
  * - opponent moves to (1,0,1)<br>
  * - current player moves to (0,0,1)<br>
  * - opponent moves to (0,0,0), and so wins.
  * <p>
  * For how many positive integers n ≤ 2<sup>30</sup> does X(n,2n,3n) = 0 ?
  */
object Nim extends Problem {
  def solve(max: Int): Int = (1L to max).count(n => (n ^ (2 * n) ^ (3 * n)) == 0)

  override def solve(): Any = solve(BigInt(2).pow(30).toInt)

  override def tags(): Set[Tag] = Set(Tag.NIM)

  override def answer(): Any = 2178309
}
