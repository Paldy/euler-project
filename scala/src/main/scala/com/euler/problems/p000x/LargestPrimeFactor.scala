package com.euler.problems.p000x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Largest prime factor <br>
  * Problem 3
  * <p>
  * The prime factors of 13195 are 5, 7, 13 and 29.
  * <p>
  * What is the largest prime factor of the number 600851475143 ?
  */
object LargestPrimeFactor extends Problem {

  def solve(n: Long) = n.primeFactors.toMap.keySet.max

  override def solve() = solve(600851475143L)

  override def tags(): Set[Tag] = Set(Tag.PRIME_FACTORS, Tag.PRIMES)

  override def answer(): Any = 6857
}
