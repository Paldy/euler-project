package com.euler.problems.p000x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

/**
  * Multiples of 3 and 5 <br>
  * Problem 1
  * <p>
  * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
  * <p>
  * Find the sum of all the multiples of 3 or 5 below 1000.
  */
object MultiplesOf3And5 extends Problem {

  def solve(limit: Int) = (1 until limit).filter(n => n % 3 == 0 || n % 5 == 0).sum

  override def solve() = solve(1000)

  override def tags(): Set[Tag] = Set(Tag.DIVISORS)

  override def answer(): Any = 233168
}
