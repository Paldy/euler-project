package com.euler.problems.p000x

import com.euler.common.Triangle
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Special Pythagorean triplet<br>
  * Problem 9
  * <p>
  * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,<br>
  *     a<sup>2</sup> + b<sup>2</sup> = c<sup>2</sup>
  * <p>
  * For example, 3<sup>2</sup> + 4<sup>2</sup> = 9 + 16 = 25 = 5<sup>2</sup>.
  * <p>
  * There exists exactly one Pythagorean triplet for which a + b + c = 1000.<br>
  * Find the product abc.
  */
object SpecialPythagoreanTriplet extends Problem {

  def productOfTriplet(t: (Long, Long, Long)): Long = t._1 * t._2 * t._3

  def solve(perimeter: Int): Long = productOfTriplet(Triangle.generateFromPerimeter(perimeter).filter(_.isPythagorean).last.toTriplet)

  override def solve(): Any = solve(1000)

  override def tags(): Set[Tag] = Set(Tag.PYTHAGOREAN_TRIPLET, Tag.TRIANGLES)

  override def answer(): Any = 31875000
}
