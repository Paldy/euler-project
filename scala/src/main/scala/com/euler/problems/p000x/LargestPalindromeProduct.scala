package com.euler.problems.p000x

import com.euler.utils._
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

import scala.math.pow

/**
  * Largest palindrome product <br>
  * Problem 4
  * <p>
  * A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
  * <p>
  * Find the largest palindrome made from the product of two 3-digit numbers.
  */
object LargestPalindromeProduct extends Problem {

  def solve(numberOfHalfDigits: Int): Int = {
    val range = pow(10, numberOfHalfDigits-1).toInt until pow(10, numberOfHalfDigits).toInt
    (for (i <- range; j <- range) yield i * j).filter(_.toLong.isPalindrome).max
  }

  override def solve() = solve(3)

  override def tags(): Set[Tag] = Set(Tag.PALINDROME)

  override def answer(): Any = 906609
}
