package com.euler.problems.p000x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Smallest multiple <br>
  * Problem 5
  * <p>
  * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
  * <p>
  * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
  */
object SmallestMultiple extends Problem {

  def solve(max: Int): Long = (1 to max).foldLeft(1L.primeFactors)((pf, n) => pf.leastCommonMultiple(n)).toLong

  override def solve(): Any = solve(20)

  override def tags(): Set[Tag] = Set(Tag.LEAST_COMMON_MULTIPLE, Tag.PRIME_FACTORS)

  override def answer(): Any = 232792560
}
