package com.euler.problems.p000x

import com.euler.common.FamousSequences
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

/**
  * 10001st prime<br>
  * Problem 7
  * <p>
  * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
  * <p>
  * What is the 10001st prime number?
  */
object _10001stPrime extends Problem {
  def solve(index: Int) = FamousSequences.primes(index - 1)

  override def solve(): Any = solve(10001)

  override def tags(): Set[Tag] = Set(Tag.PRIMES)

  override def answer(): Any = 104743
}
