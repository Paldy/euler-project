package com.euler.problems.p000x

import com.euler.format.Problem

/**
  * Sum square difference<br>
  * Problem 6
  * <p>
  * The sum of the squares of the first ten natural numbers is,<br>
  *   1<sup>2</sup> + 2<sup>2</sup> + ... + 10<sup>2</sup> = 385
  * <p>
  * The square of the sum of the first ten natural numbers is,<br>
  *   (1 + 2 + ... + 10)<sup>2</sup> = 55<sup>2</sup> = 3025
  * <p>
  * Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
  * <p>
  * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
  */
object SumSquareDifference extends Problem {
  def solve(max: Int) = (1 to max).sum * (1 to max).sum - (1 to max).map(x => x * x).sum

  override def solve(): Any = solve(100)

  override def answer(): Any = 25164150
}
