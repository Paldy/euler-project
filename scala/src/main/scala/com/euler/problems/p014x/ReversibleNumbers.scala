package com.euler.problems.p014x

import com.euler.common.NumberConstant._
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * How many reversible numbers are there below one-billion?<br>
  * Problem 145
  * <p>
  * Some positive integers n have the property that the sum [ n + reverse(n) ] consists entirely of odd (decimal) digits. For instance, 36 + 63 = 99 and 409 + 904 = 1313. We will call such numbers reversible; so 36, 63, 409, and 904 are reversible. Leading zeroes are not allowed in either n or reverse(n).
  * <p>
  * There are 120 reversible numbers below one-thousand.
  * <p>
  * How many reversible numbers are there below one-billion (10<sup>9</sup>)?
  */
object ReversibleNumbers extends Problem {
  val OddDigits = "13579".toCharArray.toSet

  def solve(limit: Int): Int =
    (12 until limit)
      .count(n => {
        if (n % 10 != 0) {
          val s = n + n.toString.reverse.toInt
          if (s % 2 != 0) {
            s.toString.forall(OddDigits.contains)
          } else false
        } else false
      })

  override def solve(): Any = solve(Billion)

  override def tags(): Set[Tag] = Set(Tag.DIGITS, Tag.SLOW)

  override def answer(): Any = 608720
}
