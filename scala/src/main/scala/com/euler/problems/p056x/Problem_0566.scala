package com.euler.problems.p056x

import scala.annotation.tailrec
import scala.collection.mutable

object Problem_0566 {
  val WHOLE: BigInt = 1


  def F(abc: List[(Int, Boolean)]): Long = {
    val sqrtPart = if (abc.exists(!_._2)) abc.filter(!_._2).map(_._1).head else 1
    val normalPart = abc.filter(_._2).map(_._1).product

    def addToBeginning(part: (IntWithSqrt, Boolean), cake: List[(IntWithSqrt, Boolean)]): List[(IntWithSqrt, Boolean)] = {
      if (cake.nonEmpty && part._2 == cake.head._2) (part._1 + cake.head._1, part._2) :: cake.tail
      else part :: cake
    }

    def mergeCakes(cake1: List[(IntWithSqrt, Boolean)], cake2: List[(IntWithSqrt, Boolean)]): List[(IntWithSqrt, Boolean)] = {
      if (cake1.nonEmpty && cake2.nonEmpty && cake1.last._2 == cake2.head._2) cake1.init ::: ((cake1.last._1 + cake2.head._1, cake2.head._2) :: cake2.tail)
      else cake1 ::: cake2
    }

    @tailrec
    def innerSolve(abc: Circular[IntWithSqrt], cake: List[(IntWithSqrt, Boolean)], i: Long = 0): Long = {
      if (i > 0 && cake.forall(_._2)) i
      else {
        if (i % 1000000 == 0) print("+")

        var restPart = abc.next
        var newCake = cake
        var rotatedPart: List[(IntWithSqrt, Boolean)] = List()
        while (restPart >= newCake.head._1) {
          restPart = restPart - newCake.head._1
          rotatedPart = addToBeginning((newCake.head._1, !newCake.head._2), rotatedPart)
          newCake = newCake.tail
        }
        if (!restPart.empty) {
          rotatedPart = addToBeginning((restPart, !newCake.head._2), rotatedPart)
          newCake = addToBeginning((restPart.neg(), newCake.head._2), newCake)
        }
        newCake = mergeCakes(newCake, rotatedPart)
        innerSolve(abc, newCake, i + 1)
      }
    }

    print(abc)
    val ret = innerSolve(
      new Circular(abc.map(fr => if (fr._2) IntWithSqrt(sqrtPart, normalPart / fr._1, 0) else IntWithSqrt(1, 0, normalPart))),
      List((IntWithSqrt(sqrtPart, normalPart, 0), true)))
    println(": " + ret)
    ret
  }

  def F2(abc: List[(Int, Boolean)]): Long = {
    val sqrtPart = abc.filter(!_._2).map(_._1).product
    val normalPart = abc.filter(_._2).map(_._1).product

    def reduceCake(cake: List[(FractionWithSqrt, Boolean)]): List[(FractionWithSqrt, Boolean)] = cake match {
      case Nil => List()
      case _ =>
        val reduceBy = cake.head._2
        (cake.takeWhile(_._2 == reduceBy).foldLeft(FractionWithSqrt(1, 0, 0, 1))((reduced, part) => reduced + part._1), reduceBy) :: reduceCake(cake.dropWhile(_._2 == reduceBy))
    }

    def addToBeginning(part: (FractionWithSqrt, Boolean), cake: List[(FractionWithSqrt, Boolean)]) =
      if (cake.nonEmpty && part._2 == cake.head._2) (part._1 + cake.head._1, part._2) :: cake.tail
      else part :: cake

    def addToEnd(cake1: List[(FractionWithSqrt, Boolean)], cake2: List[(FractionWithSqrt, Boolean)]): List[(FractionWithSqrt, Boolean)] =
      if (cake1.nonEmpty && cake2.nonEmpty && cake1.last._2 == cake2.head._2) cake1.init ::: ((cake1.last._1 + cake2.head._1, cake2.head._2) :: cake2.tail)
      else cake1 ::: cake2

    @tailrec
    def innerSolve(abc: Circular[(Int, Boolean)], cake: List[(FractionWithSqrt, Boolean)], i: Long = 0): Long = {
      if (i > 0 && cake.forall(_._2)) i
      else {
        if (i % 1000000 == 0) print("+")

        val denominator = abc.next
        val part = if (denominator._2) FractionWithSqrt(sqrtPart, normalPart / denominator._1, 0, 1) else FractionWithSqrt(1, 0, normalPart, 1)

        var restPart = part
        var newCake = cake
        var rotatedPart: List[(FractionWithSqrt, Boolean)] = List()
        while (restPart >= newCake.head._1) {
          restPart = restPart - newCake.head._1
          rotatedPart = addToBeginning((newCake.head._1, !newCake.head._2), rotatedPart)
          newCake = newCake.tail
        }
        if (!restPart.empty) {
          rotatedPart = addToBeginning((restPart, !newCake.head._2), rotatedPart)
          newCake = addToBeginning((restPart.neg(), newCake.head._2), newCake)
        }
        newCake = addToEnd(newCake, rotatedPart)
        innerSolve(abc, newCake, i + 1)
      }
    }
    print(abc)
    val ret = innerSolve(new Circular(abc), List((FractionWithSqrt(sqrtPart, normalPart, 0, 1), true)))
    println(": " + ret)
    ret
  }

  def sqrtOrNot(c: Int) = {
    val sqrtV = scala.math.sqrt(c).round.toInt
    if (sqrtV * sqrtV == c) (sqrtV, true) else (c, false)
  }

  def solve(n: Int) =
    (for (a <- 9 to n - 2; b <- a + 1 until n; c <- b + 1 to n)
      yield F(List((a, true), (b, true), sqrtOrNot(c)))).sum

  def main(args: Array[String]): Unit = {
    println(solve(53))
    //println(8589934592L.highestCommonDivisor(12884901888L))
    //println(F(List((6, true))))
//    println(F(List((9, true), (10, true), (11, false))))
//    println(F(List((10, true), (14, true), (4, true))))
//    println(F(List((15, true), (16, true), (17, false))))
//    println(FractionWithSqrt(1, 0, 360, 1) - FractionWithSqrt(1, 0, 360, 9))
//    println(FractionWithSqrt(1, 0, 320, 1) - FractionWithSqrt(1, 0, 360, 10))
//    println(FractionWithSqrt(1, 0, 284, 1) - FractionWithSqrt(11, 0, 360, 1))
//    println(IntWithSqrt(1, 0, 4) + IntWithSqrt(11, 90, 5))
//    println(IntWithSqrt(11, -90, 4) + IntWithSqrt(11, 90, 4))
//    println(IntWithSqrt(11, 90, 4) - IntWithSqrt(11, 90, 4))
  }
}

/**
  * (SQRT * X + Y)
  */
class IntWithSqrt(val squareRooted: Int, val x: Long, val y: Long) extends Ordered[IntWithSqrt] {
  def +(that: IntWithSqrt): IntWithSqrt = IntWithSqrt(if (squareRooted != 1) squareRooted else that.squareRooted, x + that.x, y + that.y)

  def -(that: IntWithSqrt): IntWithSqrt = this + that.neg()

  def neg(): IntWithSqrt = IntWithSqrt(squareRooted, -x, -y)

  lazy val evaluate = 1000.0 * scala.math.sqrt(squareRooted) * x.toDouble + 1000.0 * y.toDouble

  lazy val empty = x == 0 && y == 0

  override def toString =
    (if (x != 0) "(" + squareRooted + ")" + x else "") + (if (x != 0 && y > 0) "+" else "") + (if (y != 0) y else "")

  override def compare(that: IntWithSqrt): Int =
    if ((x == 0 || squareRooted == that.squareRooted) && x == that.x && y == that.y) 0
    else evaluate.compare(that.evaluate)
}

object IntWithSqrt {
  def apply(squareRooted: Int, x: Long, y: Long): IntWithSqrt = {
    if (squareRooted == 1) new IntWithSqrt(1, 0, x + y)
    else if (x == 0) new IntWithSqrt(1, 0, y)
    else new IntWithSqrt(squareRooted, x, y)
  }
}

/**
  * (SQRT * X + Y) / (SQRT * Z)
  */
class FractionWithSqrt(val squareRooted: Int, val x: BigInt, val y: BigInt, val z: BigInt) extends Ordered[FractionWithSqrt] {
  def +(that: FractionWithSqrt): FractionWithSqrt =
    if (squareRooted == 1)
      if (that.squareRooted == 1) FractionWithSqrt(1, 0, y * that.z + that.y * z, z * that.z)
      else FractionWithSqrt(that.squareRooted, y * that.z + that.x * z, that.y * z, z * that.z)
    else
      if (that.squareRooted == 1) FractionWithSqrt(squareRooted, x * that.z + that.y * z, y * that.z, z * that.z)
      else
        if (y * that.z + that.y * z == 0) FractionWithSqrt(1, 0, x * that.z + that.x * z, z * that.z)
        else FractionWithSqrt(that.squareRooted, x * that.z + that.x * z, y * that.z + that.y * z, z * that.z)

  def -(that: FractionWithSqrt): FractionWithSqrt = this + that.neg()

  def neg(): FractionWithSqrt = FractionWithSqrt(squareRooted, -x, -y, z)

  lazy val evaluate = y.toDouble / (scala.math.sqrt(squareRooted) * z.toDouble) + (x.toDouble / z.toDouble)

  lazy val empty = x == 0 && y == 0

  override def compare(that: FractionWithSqrt): Int =
    if (squareRooted == that.squareRooted && x * that.z == that.x * z && y * that.z == that.y * z) 0
    else evaluate.compare(that.evaluate)

  override def toString = "(SQRT(" + squareRooted + ") * " + x + " + " + y + ") / (SQRT(" + squareRooted + ") * " + z + ")"
}

object FractionWithSqrt {
  def apply(squareRooted: Int, x: BigInt, y: BigInt, z: BigInt): FractionWithSqrt = {
//    var hcd = z
//    if (x != 0) hcd = hcd.highestCommonDivisor(x)
//    if (y != 0) hcd = hcd.highestCommonDivisor(y)

    new FractionWithSqrt(squareRooted, x, y, z)
  }
}

class Circular[A](list: Seq[A]) extends Iterator[A]{

  val elements = new mutable.Queue[A] ++= list
  var pos = 0

  def next = {
    if (pos == elements.length)
      pos = 0
    val value = elements(pos)
    pos = pos + 1
    value
  }

  def hasNext = elements.nonEmpty
  def add(a: A): Unit = { elements += a }
  override def toString = elements.toString

}