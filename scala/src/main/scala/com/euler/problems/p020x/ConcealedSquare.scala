package com.euler.problems.p020x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Concealed Square<br>
  * Problem 206
  * <p>
  * Find the unique positive integer whose square has the form 1_2_3_4_5_6_7_8_9_0,<br>
  * where each “_” is a single digit.
  */
object ConcealedSquare extends Problem {
  val roughStart = scala.math.sqrt(1020304050607080900L).round

  val rightStart = roughStart - (roughStart % 10) - 10 * ((roughStart / 10) % 10)

  lazy val streamPossibleNumbers: Stream[Long] = {
    def innerStream(n: Long, endsWithThirty: Boolean): Stream[Long] = {
      n #:: innerStream(if (endsWithThirty) n + 40 else n + 60, !endsWithThirty)
    }

    innerStream(rightStart + 30, endsWithThirty = true)
  }

  override def solve() = streamPossibleNumbers.find(n => (n * n).toString.matches("1.2.3.4.5.6.7.8.9.0")).get

  override def tags(): Set[Tag] = Set(Tag.DIGITS, Tag.MISSING_TEST)

  override def answer(): Any = 1389019170
}
