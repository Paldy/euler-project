package com.euler.problems.p020x

import com.euler.common.Combinatorics
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

/**
  * Dice Game<br>
  * Problem 205
  * <p>
  * Peter has nine four-sided (pyramidal) dice, each with faces numbered 1, 2, 3, 4.<br>
  * Colin has six six-sided (cubic) dice, each with faces numbered 1, 2, 3, 4, 5, 6.
  * <p>
  * Peter and Colin roll their dice and compare totals: the highest total wins. The result is a draw if the totals are equal.
  * <p>
  * What is the probability that Pyramidal Pete beats Cubic Colin? Give your answer rounded to seven decimal places in the form 0.abcdefg
  */
object DiceGame extends Problem {
  override def solve(): Any = {
    val cubic =
      Combinatorics.permutationsWithRepetition((1 to 6).toList, 6)
        .map(_.sum)
        .groupBy(identity)
        .mapValues(_.size)
        .toList
        .sortBy(_._1)

    val pyramidal =
      Combinatorics.permutationsWithRepetition((1 to 4).toList, 9)
        .map(_.sum)
        .groupBy(identity)
        .mapValues(_.size)
        .toList
        .sortBy(_._1)

    val peterWins =
      pyramidal
        .map(p => p._2.toLong * cubic.takeWhile(_._1 < p._1).map(_._2).sum)
        .sum

    val all = pyramidal.map(_._2).sum.toLong * cubic.map(_._2).sum

    (BigDecimal(peterWins) / BigDecimal(all)).setScale(7, BigDecimal.RoundingMode.HALF_UP)
  }

  override def tags(): Set[Tag] = Set(Tag.COMBINATORICS)

  override def answer(): Any = "0.5731441"
}
