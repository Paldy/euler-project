package com.euler.problems.p010x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

import scala.io.Source

/**
  * Triangle containment<br>
  * Problem 102
  * <p>
  * Three distinct points are plotted at random on a Cartesian plane, for which -1000 ≤ x, y ≤ 1000, such that a triangle is formed.
  * <p>
  * Consider the following two triangles:
  * <p>
  * A(-340,495), B(-153,-910), C(835,-947)
  * <p>
  * X(-175,41), Y(-421,-714), Z(574,-645)
  * <p>
  * It can be verified that triangle ABC contains the origin, whereas triangle XYZ does not.
  * <p>
  * Using triangles.txt (right click and 'Save Link/Target As...'), a 27K text file containing the co-ordinates of one thousand "random" triangles, find the number of triangles for which the interior contains the origin.
  * <p>
  * NOTE: The first two examples in the file represent the triangles in the example given above.
  */
object TriangleContainment extends Problem {
  val O = (0, 0)

  def max(a: Int, b: Int, c: Int) = List(a, b, c).max
  def min(a: Int, b: Int, c: Int) = List(a, b, c).min

  def computeDupArea(A: (Int, Int), B: (Int, Int), C: (Int, Int)): Int = {
    val listByX = List(A, B, C).sortBy(_._1)
    val listByY = List(A, B, C).sortBy(_._2)
    if (listByX(1) == listByY(1)) {
      val twoEdges = if (listByX(1) == A) (B, C) else if (listByX(1) == B) (A, C) else (A, B)
      val area1 = math.abs(twoEdges._1._1 - listByX(1)._1) * math.abs(twoEdges._2._2 - listByX(1)._2)
      val area2 = math.abs(twoEdges._2._1 - listByX(1)._1) * math.abs(twoEdges._1._2 - listByX(1)._2)

      (2 * (listByX(2)._1 - listByX.head._1) * (listByY(2)._2 - listByY.head._2)
        - 2 * math.min(area1, area2)
        - math.abs(A._1 - B._1) * math.abs(A._2 - B._2)
        - math.abs(B._1 - C._1) * math.abs(B._2 - C._2)
        - math.abs(A._1 - C._1) * math.abs(A._2 - C._2))
    } else (
      2 * (listByX(2)._1 - listByX.head._1) * (listByY(2)._2 - listByY.head._2)
        - math.abs(A._1 - B._1) * math.abs(A._2 - B._2)
        - math.abs(B._1 - C._1) * math.abs(B._2 - C._2)
        - math.abs(A._1 - C._1) * math.abs(A._2 - C._2)
      )
  }

  def containsOrigin(triangle: ((Int, Int), (Int, Int), (Int, Int))): Boolean = {
    val A = triangle._1
    val B = triangle._2
    val C = triangle._3

    val areaABC = computeDupArea(A, B, C)
    val areaABO = computeDupArea(A, B, O)
    val areaAOC = computeDupArea(A, O, C)
    val areaOBC = computeDupArea(O, B, C)

    areaABC == areaABO + areaAOC + areaOBC
  }

  def solve(triangles: List[((Int, Int), (Int, Int), (Int, Int))]): Int = triangles.count(containsOrigin)

  override def solve(): Any = solve(Source.fromFile("resources/input/p102_triangles.txt").getLines.map(line => line.split(",").map(_.toInt)).map(t => ((t(0), t(1)), (t(2), t(3)), (t(4), t(5)))).toList)

  override def tags(): Set[Tag] = Set(Tag.TRIANGLES, Tag.CARTESIAN_PLANE)

  override def answer(): Any = 228
}
