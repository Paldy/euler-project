package com.euler.problems.p007x

import com.euler.common.{Fraction, NumberConstant}
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Ordered fractions<br>
  * Problem 71
  * <p>
  * Consider the fraction, n/d, where n and d are positive integers. If n < d and HCF(n,d)=1, it is called a reduced proper fraction.
  * <p>
  * If we list the set of reduced proper fractions for d ≤ 8 in ascending order of size, we get:
  * <p>
  * 1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, 4/5, 5/6, 6/7, 7/8
  * <p>
  * It can be seen that 2/5 is the fraction immediately to the left of 3/7.
  * <p>
  * By listing the set of reduced proper fractions for d ≤ 1,000,000 in ascending order of size, find the numerator of the fraction immediately to the left of 3/7.
  */
object OrderedFractions extends Problem {
  def solve(max: Int, before: (Int, Int)): Long =
    (1 to max)
      .map(d => ((before._1 * d - 1) / before._2, d))
      .map(fraction => (Fraction(fraction._1, fraction._2), BigDecimal(fraction._1) / fraction._2))
      .maxBy(_._2)
      ._1
      .reducedNumerator

  override def solve(): Any = solve(NumberConstant.Million, (3, 7))

  override def tags(): Set[Tag] = Set(Tag.FRACTIONS)

  override def answer(): Any = 428570
}
