package com.euler.problems.p007x

import com.euler.common.NumberConstant._
import com.euler.common.fast.FastPrimes
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Totient permutation<br>
  * Problem 70
  * <p>
  * Euler's Totient function, φ(n) [sometimes called the phi function], is used to determine the number of positive numbers less than or equal to n which are relatively prime to n. For example, as 1, 2, 4, 5, 7, and 8, are all less than nine and relatively prime to nine, φ(9)=6.<br>
  * The number 1 is considered to be relatively prime to every positive number, so φ(1)=1.
  * <p>
  * Interestingly, φ(87109)=79180, and it can be seen that 87109 is a permutation of 79180.
  * <p>
  * Find the value of n, 1 < n < 10<sup>7</sup>, for which φ(n) is a permutation of n and the ratio n/φ(n) produces a minimum.
  */
object TotientPermutation extends Problem {
  def solve(limit: Int): Long = {
    val fastPrimes = FastPrimes(limit)

    (2 until limit)
      .map(n => (n, fastPrimes.totient(n)))
      .filter(nAndTotient => nAndTotient._1.toString.sorted == nAndTotient._2.toString.sorted)
      .minBy(nAndTotient => nAndTotient._1 * 1.0 / nAndTotient._2)
      ._1
  }

  override def solve(): Any = solve(10 * Million)

  override def tags(): Set[Tag] = Set(Tag.TOTIENT_FUNCTION, Tag.DIGITS)

  override def answer(): Any = 8319823
}
