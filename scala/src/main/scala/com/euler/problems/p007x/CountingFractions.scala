package com.euler.problems.p007x

import com.euler.common.NumberConstant._
import com.euler.common.fast.FastPrimes
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.utils._

/**
  * Counting fractions<br>
  * Problem 72
  * <p>
  * Consider the fraction, n/d, where n and d are positive integers. If n < d and HCF(n,d)=1, it is called a reduced proper fraction.
  * <p>
  * If we list the set of reduced proper fractions for d ≤ 8 in ascending order of size, we get:
  * <p>
  * 1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, 4/5, 5/6, 6/7, 7/8
  * <p>
  * It can be seen that there are 21 elements in this set.
  * <p>
  * How many elements would be contained in the set of reduced proper fractions for d ≤ 1,000,000?
  */
object CountingFractions extends Problem {
  def solve(max: Int): Long = {
    val fastPrimes = FastPrimes(max)

    (2 to max).map(fastPrimes.totient).sumLong()
  }

  override def solve(): Any = solve(Million)

  override def tags(): Set[Tag] = Set(Tag.FRACTIONS)

  override def answer(): Any = 303963552391L
}
