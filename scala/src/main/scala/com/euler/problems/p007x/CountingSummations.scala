package com.euler.problems.p007x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

import scala.collection.mutable

/**
  * Counting summations<br>
  * Problem 76
  * <p>
  * It is possible to write five as a sum in exactly six different ways:
  * <p>
  * 4 + 1<br>
  * 3 + 2<br>
  * 3 + 1 + 1<br>
  * 2 + 2 + 1<br>
  * 2 + 1 + 1 + 1<br>
  * 1 + 1 + 1 + 1 + 1
  * <p>
  * How many different ways can one hundred be written as a sum of at least two positive integers?
  */
object CountingSummations extends Problem {
  def solve(sum: Int): Int = {
    val terms = (1 until sum).toList.reverse
    val cachedResults = mutable.Map[(Int, Int), Int]()

    def innerSolve(desiredSum: Int, terms: List[Int]): Int = {
      cachedResults.getOrElseUpdate((desiredSum, terms.head),
        if (desiredSum == 0) 1
        else if (desiredSum < terms.head) innerSolve(desiredSum, terms.filter(_ <= desiredSum))
        else innerSolve(desiredSum - terms.head, terms) + (if (terms.size > 1) innerSolve(desiredSum, terms.tail) else 0)
      )
    }

    innerSolve(sum, terms)
  }

  override def solve(): Any = solve(100)

  override def tags(): Set[Tag] = Set(Tag.CACHE)

  override def answer(): Any = 190569291
}
