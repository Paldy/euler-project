package com.euler.problems.p007x

import com.euler.common.NumberConstant
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

import scala.annotation.tailrec

/**
  * Digit factorial chains<br>
  * Problem 74
  * <p>
  * The number 145 is well known for the property that the sum of the factorial of its digits is equal to 145:
  * <p>
  * 1! + 4! + 5! = 1 + 24 + 120 = 145
  * <p>
  * Perhaps less well known is 169, in that it produces the longest chain of numbers that link back to 169; it turns out that there are only three such loops that exist:
  * <p>
  * 169 → 363601 → 1454 → 169<br>
  * 871 → 45361 → 871<br>
  * 872 → 45362 → 872
  * <p>
  * It is not difficult to prove that EVERY starting number will eventually get stuck in a loop. For example,
  * <p>
  * 69 → 363600 → 1454 → 169 → 363601 (→ 1454)<br>
  * 78 → 45360 → 871 → 45361 (→ 871)<br>
  * 540 → 145 (→ 145)
  * <p>
  * Starting with 69 produces a chain of five non-repeating terms, but the longest non-repeating chain with a starting number below one million is sixty terms.
  * <p>
  * How many chains, with a starting number below one million, contain exactly sixty non-repeating terms?
  */
object DigitFactorialChains extends Problem {
  val digitToFactorial = (0L to 9).map(n => (n, n.factorial.toInt)).toMap

  def solve(limit: Int, nonRepeating: Int): Int = {
    @tailrec
    def findRepeating(chain: List[Int] = List()): Int = {
      val next = chain.head.toDigits.map(digitToFactorial(_)).sum
      if (chain.contains(next)) chain.size
      else findRepeating(next +: chain)
    }

    (1 until limit).count(n => findRepeating(List(n)) == nonRepeating)
  }

  override def solve(): Any = solve(NumberConstant.Million, 60)

  override def tags(): Set[Tag] = Set(Tag.DIGITS, Tag.COMBINATORICS)

  override def answer(): Any = 402
}
