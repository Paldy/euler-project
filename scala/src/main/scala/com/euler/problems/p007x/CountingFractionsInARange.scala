package com.euler.problems.p007x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

import scala.annotation.tailrec

/**
  * Counting fractions in a range<br>
  * Problem 73
  * <p>
  * Consider the fraction, n/d, where n and d are positive integers. If n < d and HCF(n,d)=1, it is called a reduced proper fraction.
  * <p>
  * If we list the set of reduced proper fractions for d ≤ 8 in ascending order of size, we get:
  * <p>
  * 1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, 4/5, 5/6, 6/7, 7/8
  * <p>
  * It can be seen that there are 3 fractions between 1/3 and 1/2.
  * <p>
  * How many fractions lie between 1/3 and 1/2 in the sorted set of reduced proper fractions for d ≤ 12,000?
  */
object CountingFractionsInARange extends Problem {

  def hcd(a: Int, b: Int) = {
    @tailrec
    def hcd(a: Int, b: Int): Int = b match {
      case 0 => a
      case _ => hcd(b, a % b)
    }

    hcd(a, b)
  }

  def solve(max: Int, after: (Int, Int), before: (Int, Int)) = {
    def countFractions(d: Int) = {
      val maxBefore = (before._1 * d - 1) / before._2
      val minAfter = after._1 * d / after._2 + 1
      (minAfter to maxBefore).count(hcd(_, d) == 1)
    }

    (2 to max).map(countFractions).sum
  }

  override def solve(): Any = solve(12000, (1, 3), (1, 2))

  override def tags(): Set[Tag] = Set(Tag.FRACTIONS)

  override def answer(): Any = 7295372
}
