package com.euler.problems.p007x

import com.euler.format.Problem
import com.euler.utils._

import scala.io.Source

/**
  * Passcode derivation<br>
  * Problem 79
  * <p>
  * A common security method used for online banking is to ask the user for three random characters from a passcode. For example, if the passcode was 531278, they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.
  * <p>
  * The text file, keylog.txt, contains fifty successful login attempts.
  * <p>
  * Given that the three characters are always asked for in order, analyse the file so as to determine the shortest possible secret passcode of unknown length.
  */
object PasscodeDerivation extends Problem {

  def combineLists(firstList: List[Int], secondList: List[Int]): Set[List[Int]] = (firstList, secondList) match {
    case (Nil, _) => Set(secondList)
    case (_, Nil) => Set(firstList)
    case (l1, l2) if l1.head == l2.head => combineLists(l1.tail, l2.tail).map(l1.head +: _)
    case (l1, l2) => combineLists(l1.tail, l2).map(l1.head +: _) ++ combineLists(l1, l2.tail).map(l2.head +: _)
  }

  class PasswordCracker(ps: Set[List[Int]] = Set(List())) {
    lazy val password = ps.head.toIndexedSeq.toBigInt

    def add(key: List[Int]): PasswordCracker = {
      val newPasswords = ps.flatMap(combineLists(_, key))

      val minLength = newPasswords.foldLeft(Int.MaxValue)((min, password) => scala.math.min(min, password.length))

      new PasswordCracker(newPasswords.filter(_.length == minLength))
    }
  }

  def solve(keys: List[String]) =
    keys.map(_.toList.map(_.toString.toInt)).foldLeft(new PasswordCracker)((pc, key) => pc.add(key)).password

  override def solve(): Any = solve(Source.fromFile("resources/input/p079_keylog.txt").getLines.toList)

  override def answer(): Any = 73162890
}
