package com.euler.problems.p004x

import com.euler.common.FamousSequences
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

import scala.io.Source

/**
  * Coded triangle numbers<br>
  * Problem 42
  * <p>
  * The n-th term of the sequence of triangle numbers is given by, t<sub>n</sub> = ½n(n+1); so the first ten triangle numbers are:
  * <p>
  * 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
  * <p>
  * By converting each letter in a word to a number corresponding to its alphabetical position and adding these values we form a word value. For example, the word value for SKY is 19 + 11 + 25 = 55 = t<sub>10</sub>. If the word value is a triangle number then we shall call the word a triangle word.
  * <p>
  * Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly two-thousand common English words, how many are triangle words?
  */
object CodedTriangleNumbers extends Problem {
  def parseWords(s: String) = s.replace("\"", "").split(",")

  def valueOfWord(s: String) = s.toCharArray.map(_.letterIndex).sum

  def solve(allWords: String) = parseWords(allWords).map(valueOfWord).count(x => FamousSequences.triangleNumbers.dropWhile(_ < x).head == x)

  override def solve(): Any = solve(Source.fromFile("resources/input/p042_words.txt").getLines.mkString)

  override def tags(): Set[Tag] = Set(Tag.POLYGONAL_NUMBERS)

  override def answer(): Any = 162
}
