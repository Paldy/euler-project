package com.euler.problems.p004x

import com.euler.common.FamousSequences
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag._
import com.euler.utils._

/**
  * Prime permutations<br>
  * Problem 49
  * <p>
  * The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.
  * <p>
  * There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.
  * <p>
  * What 12-digit number do you form by concatenating the three terms in this sequence?
  */
object PrimePermutations extends Problem {

  def concat(sequence: (Long, Long, Long), step: Long): Long = sequence._1 * step * step + sequence._2 * step + sequence._3

  def arePermutations(sequence: (Long, Long, Long)): Boolean = {
    val digitsSorted = sequence._1.toString.sorted
    digitsSorted == sequence._2.toString.sorted && digitsSorted == sequence._3.toString.sorted
  }

  def arePrimes(sequence: (Long, Long, Long)): Boolean = sequence._2.isPrime && sequence._3.isPrime

  def solve(i: Int): Long = {
    val minN = scala.math.pow(10, i - 1).toLong
    val maxN = scala.math.pow(10, i).toLong
    (for (p <- FamousSequences.primes.dropWhile(_ < minN).takeWhile(_ < maxN);
          i <- Stream.from(1).takeWhile(_ < (maxN - p) / 2))
      yield (p, p + i, p + i + i)).filter(arePermutations).filter(arePrimes).map(concat(_, maxN)).toList.find(_ != 148748178147L).get
  }

  def main(args: Array[String]) = println(solve(4))

  override def solve(): Any = solve(4)

  override def tags(): Set[Tag] = Set(Tag.PRIMES, Tag.DIGITS)

  override def answer(): Any = 296962999629L
}
