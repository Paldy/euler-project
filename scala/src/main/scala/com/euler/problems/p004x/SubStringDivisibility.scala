package com.euler.problems.p004x

import com.euler.common.NumberConstant
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Sub-string divisibility<br>
  * Problem 43
  * <p>
  * The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9 in some order, but it also has a rather interesting sub-string divisibility property.
  * <p>
  * Let d<sub>1</sub> be the 1st digit, d<sub>2</sub> be the 2nd digit, and so on. In this way, we note the following:
  * <p>
  * d<sub>2</sub>d<sub>3</sub>d<sub>4</sub>=406 is divisible by 2<br>
  * d<sub>3</sub>d<sub>4</sub>d<sub>5</sub>=063 is divisible by 3<br>
  * d<sub>4</sub>d<sub>5</sub>d<sub>6</sub>=635 is divisible by 5<br>
  * d<sub>5</sub>d<sub>6</sub>d<sub>7</sub>=357 is divisible by 7<br>
  * d<sub>6</sub>d<sub>7</sub>d<sub>8</sub>=572 is divisible by 11<br>
  * d<sub>7</sub>d<sub>8</sub>d<sub>9</sub>=728 is divisible by 13<br>
  * d<sub>8</sub>d<sub>9</sub>d<sub>10</sub>=289 is divisible by 17
  * <p>
  * Find the sum of all 0 to 9 pandigital numbers with this property.
  */
object SubStringDivisibility extends Problem {

  def isSubStringDivisible(n: IndexedSeq[Int]): Boolean = {
    n(3) % 2 == 0 && // n.slice(1, 4).toLong % 2 == 0
      n.slice(2, 5).toLong % 3 == 0 &&
      n(5) % 5 == 0 && // n.slice(3, 6).toLong % 5 == 0
      n.slice(4, 7).toLong % 7 == 0 &&
      n.slice(5, 8).toLong % 11 == 0 &&
      n.slice(6, 9).toLong % 13 == 0 &&
      n.slice(7, 10).toLong % 17 == 0
  }

  override def solve(): Any =
    NumberConstant
      .DecimalDigits
      .permutations
      .filter(_.head != 0)
      .filter(isSubStringDivisible)
      .map(_.toLong)
      .sum

  override def tags(): Set[Tag] = Set(Tag.PANDIGITAL, Tag.SLOW_TEST)

  override def answer(): Any = 16695334890L
}
