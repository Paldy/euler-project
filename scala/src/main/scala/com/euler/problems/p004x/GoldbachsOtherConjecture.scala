package com.euler.problems.p004x

import com.euler.common.FamousSequences
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Goldbach's other conjecture<br>
  * Problem 46
  * <p>
  * It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.
  * <p>
  * 9 = 7 + 2×1²<br>
  * 15 = 7 + 2×2²<br>
  * 21 = 3 + 2×3²<br>
  * 25 = 7 + 2×3²<br>
  * 27 = 19 + 2×2²<br>
  * 33 = 31 + 2×1²
  * <p>
  * It turns out that the conjecture was false.
  * <p>
  * What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?
  */
object GoldbachsOtherConjecture extends Problem {

  def failsGoldbach(composite: Long): Boolean =
    !Stream.from(1).map(n => 2 * n * n)
      .takeWhile(_ < composite)
      .exists(twiceASquare => (composite - twiceASquare).isPrime)

  override def solve(): Long = FamousSequences.composites.filter(_ % 2 == 1).find(failsGoldbach).get

  override def tags(): Set[Tag] = Set(Tag.COMPOSITES, Tag.GOLDBACHS_OTHER_CONJECTURE)

  override def answer(): Any = 5777
}
