package com.euler.problems.p004x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

import scala.annotation.switch

/**
  * Distinct primes factors<br>
  * Problem 47
  * <p>
  * The first two consecutive numbers to have two distinct prime factors are:
  * <p>
  * 14 = 2 × 7<br>
  * 15 = 3 × 5
  * <p>
  * The first three consecutive numbers to have three distinct prime factors are:
  * <p>
  * 644 = 2² × 7 × 23<br>
  * 645 = 3 × 5 × 43<br>
  * 646 = 2 × 17 × 19.
  * <p>
  * Find the first four consecutive integers to have four distinct prime factors. What is the first of these numbers?
  */
object DistinctPrimesFactors extends Problem {

  def solve(size: Int): Int = {
    def innerSolve(current: Int = 1): Int = {
      val indexOfFailing = (0 until size).map(i => (i + current).toLong.primeFactors.toMap.size).lastIndexWhere(_ != size)
      (indexOfFailing: @switch) match {
        case -1 => current
        case i => innerSolve(current + i + 1)
      }
    }

    innerSolve()
  }

  override def solve(): Any = solve(4)

  override def tags(): Set[Tag] = Set(Tag.PRIME_FACTORS)

  override def answer(): Any = 134043
}
