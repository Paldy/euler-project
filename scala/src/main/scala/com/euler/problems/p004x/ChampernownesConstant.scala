package com.euler.problems.p004x

import com.euler.common.NumberConstant
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.utils._

/**
  * Champernowne's constant<br>
  * Problem 40
  * <p>
  * An irrational decimal fraction is created by concatenating the positive integers:
  * <p>
  * 0.123456789101112131415161718192021...
  * <p>
  * It can be seen that the 12th digit of the fractional part is 1.
  * <p>
  * If d<sub>n</sub> represents the nth digit of the fractional part, find the value of the following expression.
  * <p>
  * d<sub>1</sub> × d<sub>10</sub> × d<sub>100</sub> × d<sub>1000</sub> × d<sub>10000</sub> × d<sub>100000</sub> × d<sub>1000000</sub>
  */
object ChampernownesConstant extends Problem {

  def solve(indices: List[Long]): Long = {
    def genResult(indices: List[Long], findIndex: Long, currentIndex: Long = 1, integer: Long = 1, product: Long = 1): Long = {
      val digits: IndexedSeq[Int] = integer.toDigits
      val nextIndex = currentIndex + digits.size

      if (findIndex < nextIndex) {
        val nextProduct = product * digits((findIndex - currentIndex).toInt)

        if (indices.isEmpty) nextProduct
        else genResult(indices.tail, indices.head, nextIndex, integer + 1, nextProduct)
      }
      else {
        genResult(indices, findIndex, nextIndex, integer + 1, product)
      }
    }

    val sortedIndices = indices.sorted
    genResult(sortedIndices.tail, sortedIndices.head)
  }

  override def solve(): Any = solve(List(1, 10, 100, 1000, 10000, 100000, NumberConstant.Million))

  override def tags(): Set[Tag] = Set(Tag.CHAMPERNOWNES_CONSTANT, Tag.DIGITS)

  override def answer(): Any = 210
}