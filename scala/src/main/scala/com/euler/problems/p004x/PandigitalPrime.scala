package com.euler.problems.p004x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Pandigital prime<br>
  * Problem 41
  * <p>
  * We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is also prime.
  * <p>
  * What is the largest n-digit pandigital prime that exists?
  */
object PandigitalPrime extends Problem {
  def solve(maxDigit: Int): Long = (for (d <- maxDigit to 1 by -1; n <- (d to 1 by -1).permutations) yield n.toLong).find(_.isPrime).get

  override def solve(): Any = solve(9)

  override def tags(): Set[Tag] = Set(Tag.PANDIGITAL, Tag.PRIMES)

  override def answer(): Any = 7652413
}
