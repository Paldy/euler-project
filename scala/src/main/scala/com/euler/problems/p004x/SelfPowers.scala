package com.euler.problems.p004x

import com.euler.common.NumberConstant
import com.euler.format.Problem

/**
  * Self powers<br>
  * Problem 48
  * <p>
  * The series, 1<sup>1</sup> + 2<sup>2</sup> + 3<sup>3</sup> + ... + 10<sup>10</sup> = 10405071317.
  * <p>
  * Find the last ten digits of the series, 1<sup>1</sup> + 2<sup>2</sup> + 3<sup>3</sup> + ... + 1000<sup>1000</sup>.
  */
object SelfPowers extends Problem {
  def solve(max: Int): Long = (Stream.from(1).takeWhile(_ <= max).map(n => BigInt(n).pow(n)).sum % NumberConstant._10Digits).toLong

  override def solve(): Any = solve(1000)

  override def answer(): Any = 9110846700L
}
