package com.euler.problems.p003x

import com.euler.common.NumberConstant
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Double-base palindromes<br>
  * Problem 36
  * <p>
  * The decimal number, 585 = 1001001001<sub>2</sub> (binary), is palindromic in both bases.
  * <p>
  * Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
  * <p>
  * (Please note that the palindromic number, in either base, may not include leading zeros.)
  */
object DoubleBasePalindromes extends Problem {

  def solve(limit: Long) = (1L to limit).filter(_.isPalindrome).filter(_.toBinaryString.isPalindrome).sum

  override def solve(): Any = solve(NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.PALINDROME, Tag.BINARY)

  override def answer(): Any = 872187
}
