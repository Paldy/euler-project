package com.euler.problems.p003x

import com.euler.common.{FamousSequences, NumberConstant}
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Circular primes<br>
  * Problem 35
  * <p>
  * The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
  * <p>
  * There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
  * <p>
  * How many circular primes are there below one million?
  */
object CircularPrimes extends Problem {
  def isCircularPrime(prime: Long): Boolean = {
    val digits = prime.toDigits
    digits.indices.map(d => (digits.slice(d, digits.size) ++ digits.slice(0, d)).toLong).forall(_.isPrime)
  }

  def solve(limit: Long) = FamousSequences.primes.takeWhile(_ < limit).count(isCircularPrime)

  override def solve(): Any = solve(NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.PRIMES, Tag.DIGITS, Tag.COMBINATORICS)

  override def answer(): Any = 55
}
