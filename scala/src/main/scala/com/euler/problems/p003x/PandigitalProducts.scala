package com.euler.problems.p003x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.utils._

/**
  * Pandigital products<br>
  * Problem 32
  * <p>
  * We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.
  * <p>
  * The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.
  * <p>
  * Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.<br>
  * HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.
  */
object PandigitalProducts extends Problem {

  def generateSplitLists(l: IndexedSeq[Int]) =
    for (a <- 1 to l.size / 3;
         b <- a + 1 to a + 1 + l.size / 3)
      yield (l.slice(0, a).toLong, l.slice(a, b).toLong, l.slice(b, l.size).toLong)

  def solve(i: Int): Long =
    (for (l <- (1 to i).permutations; sl <- generateSplitLists(l); if sl._1 * sl._2 == sl._3)
      yield sl._3).toSet.sum

  override def solve(): Any = solve(9)

  override def tags(): Set[Tag] = Set(Tag.COMBINATORICS, Tag.PANDIGITAL)

  override def answer(): Any = 45228
}
