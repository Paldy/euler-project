package com.euler.problems.p003x

import com.euler.common.{Fraction, NumberConstant}
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Digit cancelling fractions<br>
  * Problem 33
  * <p>
  * The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.
  * <p>
  * We shall consider fractions like, 30/50 = 3/5, to be trivial examples.
  * <p>
  * There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.
  * <p>
  * If the product of these four fractions is given in its lowest common terms, find the value of the denominator.
  */
object DigitCancellingFractions extends Problem {
  override def solve(): Int =
    Fraction((for (a <- NumberConstant.DecimalDigitsWoZero; b <- NumberConstant.DecimalDigitsWoZero; d <- NumberConstant.DecimalDigitsWoZero
                   if 9 * a * b == d * (10 * a - b) && 10 * d + b != 0 && 10 * a + d < 10 * d + b)
      yield (10 * a + d, 10 * d + b)).foldLeft((1, 1))((p, n) => (p._1 * n._1, p._2 * n._2))).reducedDenominator

  override def tags(): Set[Tag] = Set(Tag.FRACTIONS)

  override def answer(): Any = 100
}
