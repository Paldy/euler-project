package com.euler.problems.p003x

import com.euler.common.Triangle
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Integer right triangles<br>
  * Problem 39
  * <p>
  * If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly three solutions for p = 120.
  * <p>
  * {20,48,52}, {24,45,51}, {30,40,50}
  * <p>
  * For which value of p ≤ 1000, is the number of solutions maximised?
  */
object IntegerRightTriangles extends Problem {

  def solve(max: Long) = (1L to max).map(p => (p, Triangle.generateFromPerimeter(p).count(_.isPythagorean))).maxBy(_._2)._1

  override def solve(): Any = solve(1000)

  override def tags(): Set[Tag] = Set(Tag.TRIANGLES)

  override def answer(): Any = 840
}
