package com.euler.problems.p003x

import com.euler.common.FamousSequences
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Truncatable primes<br>
  * Problem 37
  * <p>
  * The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.
  * <p>
  * Find the sum of the only eleven primes that are both truncatable from left to right and right to left.
  * <p>
  * NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
  */
object TruncatablePrimes extends Problem {

  def isTruncatable(p: Long): Boolean = {
    def isTruncatableLeftToRight(l: IndexedSeq[Int]): Boolean = {
      if (l.isEmpty) true
      else if (l.toLong.isPrime) isTruncatableLeftToRight(l.tail)
      else false
    }

    def isTruncatableRightToLeft(l: IndexedSeq[Int]): Boolean = {
      if (l.isEmpty) true
      else if (l.toLong.isPrime) isTruncatableRightToLeft(l.init)
      else false
    }

    val digits = p.toDigits
    isTruncatableLeftToRight(digits.tail) && isTruncatableRightToLeft(digits.init)
  }

  def solve(numberOfPrimes: Int) = FamousSequences.primes.dropWhile(_ < 10).filter(isTruncatable).take(numberOfPrimes).sum

  override def solve(): Any = solve(11)

  override def tags(): Set[Tag] = Set(Tag.PRIMES, Tag.DIGITS)

  override def answer(): Any = 748317
}
