package com.euler.problems.p003x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.utils._

/**
  * Digit fifth powers<br>
  * Problem 30
  * <p>
  * Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:
  * <p>
  * 1634 = 1<sup>4</sup> + 6<sup>4</sup> + 3<sup>4</sup> + 4<sup>4</sup><br>
  * 8208 = 8<sup>4</sup> + 2<sup>4</sup> + 0<sup>4</sup> + 8<sup>4</sup><br>
  * 9474 = 9<sup>4</sup> + 4<sup>4</sup> + 7<sup>4</sup> + 4<sup>4</sup>
  * <p>
  * As 1 = 1<sup>4</sup> is not a sum it is not included.
  * <p>
  * The sum of these numbers is 1634 + 8208 + 9474 = 19316.
  * <p>
  * Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
  */
object DigitFifthPower extends Problem {
  def maxNumber(numberOfDigits: Int): Long = {
    val n = scala.math.pow(9, numberOfDigits).toLong

    def digits(d: Int, maxN: Long): Long = {
      if (scala.math.log10(maxN) <= d) maxN
      else digits(d + 1, maxN + n)
    }

    digits(1, n)
  }

  def solve(numberOfDigits: Int): Long =
    Stream.from(2).takeWhile(_ < maxNumber(numberOfDigits))
      .filter(n => n.toLong.toDigits.map(scala.math.pow(_, numberOfDigits).toInt).sum == n)
      .sum

  override def solve(): Any = solve(5)

  override def tags(): Set[Tag] = Set(Tag.DIGITS)

  override def answer(): Any = 443839
}
