package com.euler.problems.p003x

import com.euler.format.Problem

/**
  * Coin sums<br>
  * Problem 31
  * <p>
  * In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:
  * <p>
  * 1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
  * <p>
  * It is possible to make £2 in the following way:
  * <p>
  * 1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
  * <p>
  * How many different ways can £2 be made using any number of coins?
  */
object CoinSums extends Problem {
  def solve(coins: List[Int], finalDesiredSum: Int): Long = {
    val sortedCoins = coins.sorted

    def innerSolve(desiredSum: Int, min: Int): Int =
      sortedCoins.filter(c => c >= min && c <= desiredSum).map(c => if (desiredSum - c == 0) 1 else innerSolve(desiredSum - c, c)).sum

    innerSolve(finalDesiredSum, sortedCoins.head)
  }

  override def solve(): Any = solve(List(1, 2, 5, 10, 20, 50, 100, 200), 200)

  override def answer(): Any = 73682
}
