package com.euler.problems.p003x

import com.euler.common.NumberConstant
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.utils._

/**
  * Pandigital multiples<br>
  * Problem 38
  * <p>
  * Take the number 192 and multiply it by each of 1, 2, and 3:
  * <p>
  *     192 × 1 = 192<br>
  *     192 × 2 = 384<br>
  *     192 × 3 = 576
  * <p>
  * By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated product of 192 and (1,2,3)
  * <p>
  * The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).
  * <p>
  * What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?
  */
object PandigitalMultiples extends Problem {
  def pandigitalMultiply(l: Long): (Boolean, Long) = {
    def tryNext(concat: IndexedSeq[Int], n: Long = 2): (Boolean, Long) = {
      val nextConcat = concat ++ (l * n).toDigits

      if (nextConcat.size > 9) (false, 0)
      else if (nextConcat.sorted == NumberConstant.DecimalDigitsWoZero) (true, nextConcat.toLong)
      else tryNext(nextConcat, n + 1)
    }

    tryNext(l.toDigits)
  }

  def solve(limit: Long) = (1L until limit).map(pandigitalMultiply).filter(_._1).maxBy(_._2)._2

  override def solve(): Any = solve(10000)

  override def tags(): Set[Tag] = Set(Tag.PANDIGITAL)

  override def answer(): Any = 932718654
}
