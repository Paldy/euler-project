package com.euler.problems.p003x

import com.euler.common.NumberConstant
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Digit factorials<br>
  * Problem 34
  * <p>
  * 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
  * <p>
  * Find the sum of all numbers which are equal to the sum of the factorial of their digits.
  * <p>
  * Note: as 1! = 1 and 2! = 2 are not sums they are not included.
  */
object DigitFactorials extends Problem {
  val numberToFactorial = (0L to 9).map(n => (n, n.factorial.toLong)).toMap

  def isCurious(n: BigInt): Boolean = n == n.toDigits.map(numberToFactorial(_)).sum

  def solve(max: BigInt): Long = (BigInt(3) to max).filter(isCurious).sum.toLong

  override def tags(): Set[Tag] = Set(Tag.COMBINATORICS, Tag.DIGITS)

  override def solve(): Any = solve(NumberConstant.MaxDigitFactorial)

  override def answer(): Any = 40730
}
