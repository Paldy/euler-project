package com.euler.problems.p002x

import com.euler.common.NumberConstant
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Lexicographic permutations<br>
  * Problem 24
  * <p>
  * A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:
  * <p>
  * 012   021   102   120   201   210
  * <p>
  * What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
  */
object LexicographicPermutations extends Problem {

  def solve(digits: List[Int], index: Int) = {
    def findPermutationByIndex(digits: List[Int], index: Int, ret: Long = 0): Long = {
      if (digits.isEmpty) ret
      else {
        val numberOfPermutationsInNextLevel = (digits.length - 1).toLong.factorial
        val nextDigit = digits.zipWithIndex.takeWhile(di => di._2 * numberOfPermutationsInNextLevel < index).last._1
        findPermutationByIndex(digits.filter(_ != nextDigit), index - numberOfPermutationsInNextLevel.toInt * digits.indexOf(nextDigit), ret * 10 + nextDigit)
      }
    }

    findPermutationByIndex(digits, index)
  }

  override def solve(): Any = solve((0 to 9).toList, NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.DIGITS, Tag.COMBINATORICS)

  override def answer(): Any = 2783915460L
}
