package com.euler.problems.p002x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

/**
  * Reciprocal cycles<br>
  * Problem 26
  * <p>
  * A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:
  * <p>
  * 1/2	= 	0.5<br>
  * 1/3	= 	0.(3)<br>
  * 1/4	= 	0.25<br>
  * 1/5	= 	0.2<br>
  * 1/6	= 	0.1(6)<br>
  * 1/7	= 	0.(142857)<br>
  * 1/8	= 	0.125<br>
  * 1/9	= 	0.(1)<br>
  * 1/10	= 	0.1
  * <p>
  * Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.
  * <p>
  * Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
  */
object ReciprocalCycles extends Problem {

  def cycleLength(i: Int): Int = {
    def findCycle(modulos: List[Int]): Int = {
      val modulo = modulos.last * 10 % i
      val moduleBefore = modulos.indexOf(modulo)

      if (moduleBefore == -1) findCycle(modulos :+ modulo)
      else modulos.size - moduleBefore
    }

    findCycle(List(1))
  }

  def solve(limit: Int) = (1 to limit).map(n => (n, cycleLength(n))).maxBy(_._2)._1

  override def solve(): Any = solve(1000)

  override def tags(): Set[Tag] = Set(Tag.FRACTIONS)

  override def answer(): Any = 983
}
