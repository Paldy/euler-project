package com.euler.problems.p002x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Quadratic primes<br>
  * Problem 27
  * <p>
  * Euler discovered the remarkable quadratic formula:
  * <p>
  * n<sup>2</sup> + n + 41
  * <p>
  * It turns out that the formula will produce 40 primes for the consecutive values n = 0 to 39. However, when n = 40, 40<sup>2</sup> + 40 + 41 = 40(40 + 1) + 41 is divisible by 41, and certainly when n = 41, 41<sup>2</sup> + 41 + 41 is clearly divisible by 41.
  * <p>
  * The incredible formula  n² − 79n + 1601 was discovered, which produces 80 primes for the consecutive values n = 0 to 79. The product of the coefficients, −79 and 1601, is −126479.
  * <p>
  * Considering quadratics of the form:
  * <p>
  * n<sup>2</sup> + an + b, where |a| < 1000 and |b| < 1000
  * <p>
  * where |n| is the modulus/absolute value of n<br>
  * e.g. |11| = 11 and |−4| = 4
  * <p>
  * Find the product of the coefficients, a and b, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n = 0.
  */
object QuadraticPrimes extends Problem {

  def consecutivePrimes(a: Int, b: Int): Int = Stream.from(0).takeWhile(n => (n * n + a * n + b).toLong.isPrime).size

  def solve(i: Int) = (for (a <- -i + 1 until i; b <- -i + 1 until i) yield (a * b, consecutivePrimes(a, b))).maxBy(_._2)._1

  override def solve(): Any = solve(1000)

  override def tags(): Set[Tag] = Set(Tag.PRIMES)

  override def answer(): Any = -59231
}
