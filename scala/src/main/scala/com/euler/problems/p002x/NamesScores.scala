package com.euler.problems.p002x

import com.euler.format.Problem
import com.euler.utils._

import scala.io.Source

/**
  * Names scores<br>
  * Problem 22
  * <p>
  * Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.
  * <p>
  * For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.
  * <p>
  * What is the total of all the name scores in the file?
  */
object NamesScores extends Problem {
  val ZERO_LETTER: Int = 'A'.toInt - 1

  def parseNames(namesString: String) = namesString.replace("\"", "").split(",").sorted

  def nameValue(name: String, position: Int) = position.toLong * name.map(_.letterIndex).sum

  def solve(namesString: String): Long = parseNames(namesString).zip(Stream.from(1)).map(pn => nameValue(pn._1, pn._2)).sum

  override def solve(): Any = solve(Source.fromFile("resources/input/p022_names.txt").getLines.mkString)

  override def answer(): Any = 871198282
}
