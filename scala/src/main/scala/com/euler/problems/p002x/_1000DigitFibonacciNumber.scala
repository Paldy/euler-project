package com.euler.problems.p002x

import com.euler.common.FamousSequences
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * 1000-digit Fibonacci number<br>
  * Problem 25
  * <p>
  * The Fibonacci sequence is defined by the recurrence relation:
  * <p>
  * F<sub>n</sub> = F<sub>n−1</sub> + F<sub>n−2</sub>, where F<sub>1</sub> = 1 and F<sub>2</sub> = 1.
  * <p>
  * Hence the first 12 terms will be:
  * <p>
  * F<sub>1</sub> = 1<br>
  * F<sub>2</sub> = 1<br>
  * F<sub>3</sub> = 2<br>
  * F<sub>4</sub> = 3<br>
  * F<sub>5</sub> = 5<br>
  * F<sub>6</sub> = 8<br>
  * F<sub>7</sub> = 13<br>
  * F<sub>8</sub> = 21<br>
  * F<sub>9</sub> = 34<br>
  * F<sub>10</sub> = 55<br>
  * F<sub>11</sub> = 89<br>
  * F<sub>12</sub> = 144<br>
  * <p>
  * The 12th term, F<sub>12</sub>, is the first term to contain three digits.
  * <p>
  * What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
  */
object _1000DigitFibonacciNumber extends Problem {
  def solve(numberOfDigits: Int) = FamousSequences.fibonacciBigInt(1, 1).takeWhile(_ < BigInt(10).pow(numberOfDigits - 1)).size + 1

  override def solve(): Any = solve(1000)

  override def tags(): Set[Tag] = Set(Tag.FIBONACCI_NUMBERS)

  override def answer(): Any = 4783
}
