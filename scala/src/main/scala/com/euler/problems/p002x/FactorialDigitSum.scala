package com.euler.problems.p002x

import com.euler.utils._
import com.euler.common.Combinatorics
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Factorial digit sum<br>
  * Problem 20
  * <p>
  * n! means n × (n − 1) × ... × 3 × 2 × 1
  * <p>
  * For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800, and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
  * <p>
  * Find the sum of the digits in the number 100!
  */
object FactorialDigitSum extends Problem {
  def solve(n: Int): Long = n.toLong.factorial.toDigits.sum

  override def solve(): Any = solve(100)

  override def tags(): Set[Tag] = Set(Tag.COMBINATORICS)

  override def answer(): Any = 648
}
