package com.euler.problems.p002x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Number spiral diagonals<br>
  * Problem 28
  * <p>
  * Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:
  * <p>
  * 21 22 23 24 25<br>
  * 20  7  8  9 10<br>
  * 19  6  1  2 11<br>
  * 18  5  4  3 12<br>
  * 17 16 15 14 13
  * <p>
  * It can be verified that the sum of the numbers on the diagonals is 101.
  * <p>
  * What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
  */
object NumberSpiralDiagonals extends Problem {

  def solve(size: Long) = 1 + Stream.from(3, 2).takeWhile(_ <= size).map(n => 4 * n * n - 6 * n + 6).sum

  override def solve(): Any = solve(1001)

  override def tags(): Set[Tag] = Set(Tag.NUMBER_SPIRAL)

  override def answer(): Any = 669171001
}
