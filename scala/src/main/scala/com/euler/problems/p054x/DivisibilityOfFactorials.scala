package com.euler.problems.p054x

import com.euler.common.NumberConstant
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Divisibility of factorials<br>
  * Problem 549
  * <p>
  * The smallest number m such that 10 divides m! is m=5.<br>
  * The smallest number m such that 25 divides m! is m=10.
  * <p>
  * Let s(n) be the smallest number m such that n divides m!.<br>
  * So s(10)=5 and s(25)=10.<br>
  * Let S(n) be ∑s(i) for 2 ≤ i ≤ n.<br>
  * S(100)=2012.
  * <p>
  * Find S(10<sup>8</sup>).
  */
object DivisibilityOfFactorials extends Problem {
  def solve(max: Int): Long = {
    val smallestFactorial: Array[Long] = Array.fill(max + 1)(-1)
    smallestFactorial(0) = 0
    smallestFactorial(1) = 0

    (2 to max).foreach(p => {
      if (smallestFactorial(p) == -1) {
        var pMultiple = p
        var pOnPower = 1
        while (pOnPower <= max / p) {
          var pMultipleCopy = pMultiple
          while (pMultipleCopy % p == 0 && pOnPower <= max / p) {
            pOnPower *= p
            (pOnPower to max by pOnPower).foreach(pOnPowerMultiple => {
              if (smallestFactorial(pOnPowerMultiple) < pMultiple) smallestFactorial(pOnPowerMultiple) = pMultiple
            })

            pMultipleCopy /= p
          }

          pMultiple += p
        }
      }
    })

    smallestFactorial.sum
  }

  override def solve(): Any = solve(100 * NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.DIVISORS, Tag.COMBINATORICS)

  override def answer(): Any = 476001479068717L
}
