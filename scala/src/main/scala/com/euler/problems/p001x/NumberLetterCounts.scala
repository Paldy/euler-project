package com.euler.problems.p001x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.ibm.icu.text.RuleBasedNumberFormat

/**
  * Number letter counts<br>
  * Problem 17
  * <p>
  * If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
  * <p>
  * If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
  * <p>
  * NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.
  */
object NumberLetterCounts extends Problem {
  val formattingRule =
    """-x: minus >>;
      |x.x: << point >>;
      |zero; one; two; three; four; five; six; seven; eight; nine;
      |ten; eleven; twelve; thirteen; fourteen; fifteen; sixteen; seventeen; eighteen; nineteen;
      |20: twenty[->>];
      |30: thirty[->>];
      |40: forty[->>];
      |50: fifty[->>];
      |60: sixty[->>];
      |70: seventy[->>];
      |80: eighty[->>];
      |90: ninety[->>];
      |100: << hundred[ and >>];
      |1000: << thousand[ >>];
      |1,000,000: << million[ >>];
      |1,000,000,000: << billion[ >>];
      |1,000,000,000,000: << trillion[ >>];
      |1,000,000,000,000,000: =#,##0=;
    """.stripMargin

  val numberFormat = new RuleBasedNumberFormat(formattingRule)

  def solve(max: Int): Long = (1 to max).foldLeft("")((merged, n) => merged + numberFormat.format(n)).replaceAll("\\W", "").length

  override def solve(): Any = solve(1000)

  override def tags(): Set[Tag] = Set(Tag.SPELL_OUT)

  override def answer(): Any = 21124
}
