package com.euler.problems.p001x

import com.euler.utils._
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

/**
  * Power digit sum<br>
  * Problem 16
  * <p>
  * 2<sup>15</sup> = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
  * <p>
  * What is the sum of the digits of the number 2<sup>1000</sup>?
  */
object PowerDigitSum extends Problem {
  def solve(base: Int, exponent: Int): Long = BigInt(base).pow(exponent).toDigits.sum

  override def solve(): Any = solve(2, 1000)

  override def tags(): Set[Tag] = Set(Tag.DIGITS)

  override def answer(): Any = 1366
}
