package com.euler.problems.p001x

import com.euler.common.NumberConstant
import com.euler.common.fast.FastPrimes
import com.euler.utils._
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Summation of primes<br>
  * Problem 10
  * <p>
  * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
  * <p>
  * Find the sum of all the primes below two million.
  */
object SummationOfPrimes extends Problem {
  def solve(limit: Int) = {
    val fastPrimes = FastPrimes(limit)
    (1 until limit).filter(fastPrimes.isPrime).sumLong()
  }

  override def solve(): Any = solve(2 * NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.PRIMES)

  override def answer(): Any = 142913828922L
}
