package com.euler.problems.p001x

import com.euler.common.NumberConstant
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

import scala.collection.mutable

/**
  * Longest Collatz sequence<br>
  * Problem 14
  * <p>
  * The following iterative sequence is defined for the set of positive integers:
  * <p>
  * n → n/2 (n is even)<br>
  * n → 3n + 1 (n is odd)
  * <p>
  * Using the rule above and starting with 13, we generate the following sequence:<br>
  * 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
  * <p>
  * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
  * <p>
  * Which starting number, under one million, produces the longest chain?
  * <p>
  * NOTE: Once the chain starts the terms are allowed to go above one million.
  */
object LongestCollatzSequence extends Problem {
  val numberToCollatzLength = mutable.Map[Long, Int](1L -> 0)

  def nextCollatz(n: Long): Long = if (n % 2 == 0) n/2 else 3 * n + 1

  def findCollatzLength(n: Long): Int = numberToCollatzLength.get(n) match {
    case Some(length) => length
    case None =>
      val length = findCollatzLength(nextCollatz(n)) + 1
      numberToCollatzLength(n) = length
      length
  }

  def solve(max: Long): Long = (1L to max).map(n => n -> findCollatzLength(n))
    .foldLeft((1L, 0))((maxLength, kv) => if (maxLength._2 < kv._2) kv else maxLength)._1

  override def solve(): Any = solve(NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.COLLATZ_SEQUENCE)

  override def answer(): Any = 837799
}
