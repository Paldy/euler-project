package com.euler.problems.p001x

import com.euler.common.Combinatorics
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

/**
  * Lattice paths<br>
  * Problem 15
  * <p>
  * Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.
  * <p>
  * How many such routes are there through a 20×20 grid?
  */
object LatticePaths extends Problem {
  def solve(size: Long) = Combinatorics.computePermutationOfMultisets(List(size, size))

  override def solve(): Any = solve(20)

  override def tags(): Set[Tag] = Set(Tag.GRID, Tag.COMBINATORICS)

  override def answer(): Any = 137846528820L
}
