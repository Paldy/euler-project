package com.euler.problems.p001x

import java.time.{DayOfWeek, LocalDate}

import com.euler.common.FamousSequences
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

/**
  * Counting Sundays<br>
  * Problem 19
  * <p>
  * You are given the following information, but you may prefer to do some research for yourself.
  * <p>
  * 1 Jan 1900 was a Monday.<br>
  * Thirty days has September,<br>
  * April, June and November.<br>
  * All the rest have thirty-one,<br>
  * Saving February alone,<br>
  * Which has twenty-eight, rain or shine.<br>
  * And on leap years, twenty-nine.<br>
  * A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
  * <p>
  * How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
  */
object CountingSundays extends Problem {

  def solve(from: LocalDate, to: LocalDate): Long =
    FamousSequences.days(from).takeWhile(_.isBefore(to.plusDays(1))).count(x => x.getDayOfMonth == 1 && x.getDayOfWeek == DayOfWeek.SUNDAY)

  override def solve(): Any = solve(LocalDate.of(1901, 1, 1), LocalDate.of(2000, 12, 31))

  override def tags(): Set[Tag] = Set(Tag.TIME)

  override def answer(): Any = 171
}
