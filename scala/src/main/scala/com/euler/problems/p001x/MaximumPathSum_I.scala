package com.euler.problems.p001x

import com.euler.format.Problem

import scala.annotation.tailrec

/**
  * Maximum path sum I<br>
  * Problem 18
  * <p>
  * By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
  * <p>
  * 3<br>
  * 7 4<br>
  * 2 4 6<br>
  * 8 5 9 3
  * <p>
  * That is, 3 + 7 + 4 + 9 = 23.
  * <p>
  * Find the maximum total from top to bottom of the triangle below:
  * <p>
  * 75<br>
  * 95 64<br>
  * 17 47 82<br>
  * 18 35 87 10<br>
  * 20 04 82 47 65<br>
  * 19 01 23 75 03 34<br>
  * 88 02 77 73 07 63 67<br>
  * 99 65 04 28 06 16 70 92<br>
  * 41 41 26 56 83 40 80 70 33<br>
  * 41 48 72 33 47 32 37 16 94 29<br>
  * 53 71 44 65 25 43 91 52 97 51 14<br>
  * 70 11 33 28 77 73 17 78 39 68 17 57<br>
  * 91 71 52 38 17 14 91 43 58 50 27 29 48<br>
  * 63 66 04 68 89 53 67 30 73 16 69 87 40 31<br>
  * 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
  * <p>
  * NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67, is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)
  */
object MaximumPathSum_I extends Problem {
  def loadRows(row: String) = row.split(" ").map(_.toLong)

  def loadTriangle(input: String) = input.split("\n").map(loadRows).reverse

  def reduce(triangle: Array[Array[Long]]): Long = {
    @tailrec
    def tailReduce(t: Array[Array[Long]]): Long = {
      if (t.length == 1) t(0)(0)
      else {
        t.tail.head.indices.foreach(i => t.tail.head(i) += scala.math.max(t.head(i), t.head(i+1)))
        tailReduce(t.tail)
      }
    }

    tailReduce(triangle)
  }

  def solve(s: String): Long = reduce(loadTriangle(s))

  override def solve(): Any = solve("75\n95 64\n17 47 82\n18 35 87 10\n20 04 82 47 65\n19 01 23 75 03 34\n88 02 77 73 07 63 67\n99 65 04 28 06 16 70 92\n41 41 26 56 83 40 80 70 33\n41 48 72 33 47 32 37 16 94 29\n53 71 44 65 25 43 91 52 97 51 14\n70 11 33 28 77 73 17 78 39 68 17 57\n91 71 52 38 17 14 91 43 58 50 27 29 48\n63 66 04 68 89 53 67 30 73 16 69 87 40 31\n04 62 98 27 23 09 70 98 73 93 38 53 60 04 23")

  override def answer(): Any = 1074
}
