package com.euler.problems.p035x

import com.euler.common.NumberConstant
import com.euler.common.fast.FastPrimes
import com.euler.utils._
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Prime generating integers<br>
  * Problem 357<br>
  * <p>
  * Consider the divisors of 30: 1,2,3,5,6,10,15,30.<br>
  * It can be seen that for every divisor d of 30, d+30/d is prime.
  * <p>
  * Find the sum of all positive integers n not exceeding 100 000 000<br>
  * such that for every divisor d of n, d+n/d is prime.
  */
object PrimeGeneratingIntegers extends Problem {
  def solve(max: Int) = {
    val fastPrimes = FastPrimes(max + 1)

    def checkPrimes(n: Int, ff: List[Int]): Boolean = {
      ff.toSet.powerSet.forall(ps => {
        val divisor = ps.product
        fastPrimes.isPrime(divisor + n / divisor)
      })
    }

    def isPrimeGeneratingInteger(n: Int): Boolean = {
      val pf = fastPrimes.computePrimeFactors(n).toList
      (pf.size == pf.distinct.size) && checkPrimes(n, pf)
    }

    (1 to max)
      .filter(n => fastPrimes.isPrime(n + 1) && fastPrimes.isPrime(n / 2 + 2))
      .filter(isPrimeGeneratingInteger)
      .sumLong()
  }

  override def solve(): Any = solve(100 * NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.PRIMES, Tag.DIVISORS)

  override def answer(): Any = 1739023853137L
}
