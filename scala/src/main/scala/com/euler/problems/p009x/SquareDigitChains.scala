package com.euler.problems.p009x

import com.euler.common.NumberConstant
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

import scala.collection.mutable

/**
  * Square digit chains<br>
  * Problem 92
  * <p>
  * A number chain is created by continuously adding the square of the digits in a number to form a new number until it has been seen before.
  * <p>
  * For example,
  * <p>
  * 44 → 32 → 13 → 10 → 1 → 1<br>
  * 85 → 89 → 145 → 42 → 20 → 4 → 16 → 37 → 58 → 89
  * <p>
  * Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop. What is most amazing is that EVERY starting number will eventually arrive at 1 or 89.
  * <p>
  * How many starting numbers below ten million will arrive at 89?
  */
object SquareDigitChains extends Problem {
  def solve(limit: Int) = {
    val chainsEndingWith89 = new mutable.BitSet(limit)
    chainsEndingWith89 += 89

    def nextNumberInChain(n: Int): Int = n.toLong.foldDigits(0)((sum, digit) => sum + digit * digit)

    def fillChainsEndingWith89(n: Int, start: Int): Boolean = {
      if (chainsEndingWith89.contains(n)) true
      else if (n < start) false
      else if (fillChainsEndingWith89(nextNumberInChain(n), start)) {
        chainsEndingWith89 += n
        true
      }
      else false
    }

    (2 until limit).foreach(n => fillChainsEndingWith89(n, n))

    chainsEndingWith89.count(_ < limit)
  }

  override def solve(): Any = solve(10 * NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.DIGITS)

  override def answer(): Any = 8581146
}
