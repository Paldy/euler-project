package com.euler.problems.p009x

import com.euler.common.NumberConstant
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

/**
  * Large non-Mersenne prime<br>
  * Problem 97
  * <p>
  * The first known prime found to exceed one million digits was discovered in 1999, and is a Mersenne prime of the form 26972593−1; it contains exactly 2,098,960 digits. Subsequently other Mersenne primes, of the form 2p−1, have been found which contain more digits.
  * <p>
  * However, in 2004 there was found a massive non-Mersenne prime which contains 2,357,207 digits: 28433×2<sup>7830457</sup>+1.
  * <p>
  * Find the last ten digits of this prime number.
  */
object LargeNonMersennePrime extends Problem {
  override def solve() = (28433 * BigInt(2).pow(7830457) + 1) % NumberConstant._10Digits

  override def tags(): Set[Tag] = Set(Tag.PRIMES)

  override def answer(): Any = 8739992577L
}
