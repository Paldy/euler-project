package com.euler.problems.p009x

import com.euler.format.Problem

import scala.io.Source

/**
  * Largest exponential<br>
  * Problem 99
  * <p>
  * Comparing two numbers written in index form like 2<sup>11</sup> and 3<sup>7</sup> is not difficult, as any calculator would confirm that 2<sup>11</sup> = 2048 < 3<sup>7</sup> = 2187.
  * <p>
  * However, confirming that 632382<sup>518061</sup> > 519432<sup>525806</sup> would be much more difficult, as both numbers contain over three million digits.
  * <p>
  * Using base_exp.txt (right click and 'Save Link/Target As...'), a 22K text file containing one thousand lines with a base/exponent pair on each line, determine which line number has the greatest numerical value.
  * <p>
  * NOTE: The first two lines in the file represent the numbers in the example given above.
  */
object LargestExponential extends Problem {
  def firstPairLarger(pair1: (Int, Int), pair2: (Int, Int)): Boolean = pair1._2 * scala.math.log(pair1._1) > pair2._2 * scala.math.log(pair2._1)

  def solve(baseExponentPairs: Iterator[(Int, Int)]) =
    baseExponentPairs.zip(Stream.from(1).iterator).foldLeft(((1, 1), 0))((largest, pairWithIndex) => if (firstPairLarger(largest._1, pairWithIndex._1)) largest else pairWithIndex)._2

  override def solve(): Any = solve(Source.fromFile("resources/input/p099_base_exp.txt").getLines.map(line => line.split(",")).map(pairs => (pairs(0).toInt, pairs(1).toInt)))

  override def answer(): Any = 709
}
