package com.euler.problems.p006x

import com.euler.common.FamousSequences
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.utils._

import scala.annotation.tailrec
import scala.collection.mutable

/**
  * Prime pair sets<br>
  * Problem 60
  * <p>
  * The primes 3, 7, 109, and 673, are quite remarkable. By taking any two primes and concatenating them in any order the result will always be prime. For example, taking 7 and 109, both 7109 and 1097 are prime. The sum of these four primes, 792, represents the lowest sum for a set of four primes with this property.
  * <p>
  * Find the lowest sum for a set of five primes for which any two primes concatenate to produce another prime.
  */
object PrimePairSets extends Problem {
  def solve(numberOfPrimes: Int): Long = {
    val remarkableGroups = mutable.Set[Set[Long]]()

    val possiblePrimes = 3 #:: FamousSequences.primes.dropWhile(_ < 7)

    def producesPrimes(p1: Long, p2: Long): Boolean =
      (p1.toString + p2.toString).toLong.isPrime && (p2.toString + p1.toString).toLong.isPrime

    @tailrec
    def findRemarkableGroups(primes: Stream[Long]): Long = {
      val prime = primes.head
      val pairablePrimes = possiblePrimes.takeWhile(_ < prime).filter(producesPrimes(_, prime)).toSet
      val additionalGroups = remarkableGroups.filter(_.forall(pairablePrimes.contains)).map(_ + prime)
      if (additionalGroups.exists(_.size == numberOfPrimes)) {
        additionalGroups.filter(_.size == numberOfPrimes).map(_.sum).min
      } else {
        remarkableGroups ++= additionalGroups
        remarkableGroups ++= pairablePrimes.map(Set(_, prime))
        findRemarkableGroups(primes.tail)
      }
    }

    findRemarkableGroups(FamousSequences.primes.dropWhile(_ < 7))
  }

  override def solve(): Any = solve(5)

  override def tags(): Set[Tag] = Set(Tag.PRIMES, Tag.DIGITS)

  override def answer(): Any = 26033
}
