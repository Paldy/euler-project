package com.euler.problems.p006x

import com.euler.common.NumberConstant
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.utils._

/**
  * Totient maximum<br>
  * Problem 69
  * <p>
  * Euler's Totient function, φ(n) [sometimes called the phi function], is used to determine the number of numbers less than n which are relatively prime to n. For example, as 1, 2, 4, 5, 7, and 8, are all less than nine and relatively prime to nine, φ(9)=6.<br>
  * n 	Relatively Prime 	φ(n) 	n/φ(n)<br>
  * 2 	1 	1 	2<br>
  * 3 	1,2 	2 	1.5<br>
  * 4 	1,3 	2 	2<br>
  * 5 	1,2,3,4 	4 	1.25<br>
  * 6 	1,5 	2 	3<br>
  * 7 	1,2,3,4,5,6 	6 	1.1666...<br>
  * 8 	1,3,5,7 	4 	2<br>
  * 9 	1,2,4,5,7,8 	6 	1.5<br>
  * 10 	1,3,7,9 	4 	2.5
  * <p>
  * It can be seen that n=6 produces a maximum n/φ(n) for n ≤ 10.
  * <p>
  * Find the value of n ≤ 1,000,000 for which n/φ(n) is a maximum.<br>
  */
object TotientMaximum extends Problem {
  def solve(max: Int): Int = (2 to max).map(n => (n, 1.0 * n / n.toLong.totient)).maxBy(_._2)._1

  override def solve(): Any = solve(NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.TOTIENT_FUNCTION)

  override def answer(): Any = 510510
}
