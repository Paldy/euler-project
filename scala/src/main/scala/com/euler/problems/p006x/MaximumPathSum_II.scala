package com.euler.problems.p006x

import com.euler.format.Problem
import com.euler.problems.p001x.MaximumPathSum_I

import scala.io.Source

/**
  * Maximum path sum II<br>
  * Problem 67
  * <p>
  * By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
  * <p>
  * 3<br>
  * 7 4<br>
  * 2 4 6<br>
  * 8 5 9 3
  * <p>
  * That is, 3 + 7 + 4 + 9 = 23.
  * <p>
  * Find the maximum total from top to bottom in triangle.txt (right click and 'Save Link/Target As...'), a 15K text file containing a triangle with one-hundred rows.
  * <p>
  * NOTE: This is a much more difficult version of Problem 18. It is not possible to try every route to solve this problem, as there are 299 altogether! If you could check one trillion (1012) routes every second it would take over twenty billion years to check them all. There is an efficient algorithm to solve it. ;o)
  */
object MaximumPathSum_II extends Problem {

  def solve(s: String): Long = MaximumPathSum_I.solve(s)

  override def solve(): Any = solve(Source.fromFile("resources/input/p067_triangle.txt").getLines.mkString("\n"))

  override def answer(): Any = 7273
}
