package com.euler.problems.p006x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

import scala.collection.mutable

/**
  * Cubic permutations<br>
  * Problem 62
  * <p>
  * The cube, 41063625 (345<sup>3</sup>), can be permuted to produce two other cubes: 56623104 (384<sup>3</sup>) and 66430125 (405<sup>3</sup>). In fact, 41063625 is the smallest cube which has exactly three permutations of its digits which are also cube.
  * <p>
  * Find the smallest cube for which exactly five permutations of its digits are cube.
  */
object CubicPermutations extends Problem {
  def solve(numberOfPermutations: Int): BigInt = {
    var numberOfDigits = 0
    var n = BigInt(1)
    var cubicGroups = mutable.Map[String, List[BigInt]]()

    while (!cubicGroups.exists(_._2.size == numberOfPermutations)) {
      cubicGroups = mutable.Map[String, List[BigInt]]()
      var cubicN = n.pow(3)
      var cubicNString = cubicN.toString.sorted
      numberOfDigits = cubicNString.length
      while (numberOfDigits == cubicNString.length) {
        cubicGroups(cubicNString) = cubicGroups.getOrElse(cubicNString, List()) :+ cubicN
        n += 1
        cubicN = n.pow(3)
        cubicNString = cubicN.toString.sorted
      }
    }

    cubicGroups
      .filter(_._2.size == numberOfPermutations)
      .minBy(_._2.head)
      ._2.head
  }

  override def solve(): Any = solve(5)

  override def tags(): Set[Tag] = Set(Tag.DIGITS)

  override def answer(): Any = 127035954683L
}
