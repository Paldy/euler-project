package com.euler.problems.p006x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.utils._

import scala.annotation.tailrec

/**
  * Convergents of e<br>
  * Problem 65
  * <p>
  * The square root of 2 can be written as an infinite continued fraction.<br>
  * √2 = 1 + 1 / (2 + 1 / (2 + 1 / (2 + 1 / (2 + ...))))<br>
  * <p>
  * The infinite continued fraction can be written, √2 = [1;(2)], (2) indicates that 2 repeats ad infinitum. In a similar way, √23 = [4;(1,3,1,8)].
  * <p>
  * It turns out that the sequence of partial values of continued fractions for square roots provide the best rational approximations. Let us consider the convergents for √2.<br>
  * 1 + 1 / 2	= 3 / 2<br>
  * <p>
  * 1 + 1 / (2 + 1 / 2) = 7 / 5<br>
  * <p>
  * 1 + 1 / (2 + 1 / (2 + 1 / 2)) = 17 / 12<br>
  * <p>
  * 1 + 1 / (2 + 1 / (2 + 1 / (2 + 1 / 2))) = 41 / 29<br>
  * <p>
  * Hence the sequence of the first ten convergents for √2 are:<br>
  * 1, 3/2, 7/5, 17/12, 41/29, 99/70, 239/169, 577/408, 1393/985, 3363/2378, ...
  * <p>
  * What is most surprising is that the important mathematical constant,<br>
  * e = [2; 1,2,1, 1,4,1, 1,6,1 , ... , 1,2k,1, ...].
  * <p>
  * The first ten terms in the sequence of convergents for e are:<br>
  * 2, 3, 8/3, 11/4, 19/7, 87/32, 106/39, 193/71, 1264/465, 1457/536, ...
  * <p>
  * The sum of digits in the numerator of the 10th convergent is 1+4+5+7=17.
  * <p>
  * Find the sum of digits in the numerator of the 100th convergent of the continued fraction for e.
  */
object ConvergentsOfE extends Problem {
  def solve(index: Int): Int = {
    def computeAddForE(index: Int): Int = {
      if (index == 1) 2
      else if (index % 3 == 0) 2 * index / 3
      else 1
    }

    @tailrec
    def eulerFractions(index: Int, ret: (BigInt, BigInt) = (1, 0)): (BigInt, BigInt) =
      if (index == 0) ret
      else eulerFractions(index - 1, (ret._1 * computeAddForE(index) + ret._2, ret._1))

    eulerFractions(index)._1.toDigits.sum
  }

  override def solve(): Any = solve(100)

  override def tags(): Set[Tag] = Set(Tag.DIGITS, Tag.CONTINUED_FRACTIONS, Tag.EULER_NUMBER)

  override def answer(): Any = 272
}
