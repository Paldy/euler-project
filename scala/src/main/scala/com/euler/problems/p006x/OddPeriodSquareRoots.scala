package com.euler.problems.p006x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

import scala.annotation.tailrec

/**
  * Odd period square roots<br>
  * Problem 64
  * <p>
  * All square roots are periodic when written as continued fractions and can be written in the form:<br>
  * √N = a<sub>0</sub> + (1 / (a<sub>1</sub> + 1 / (a<sub>2</sub> + 1 / (a<sub>3</sub> + ...))))
  * <p>
  * For example, let us consider √23:<br>
  * √23 = 4 + √23 - 4 = 4 + 1 / (1 / (√23 - 4)) = 4 + 1 / (1 + (√23 - 3) / 7)
  * <p>
  * If we continue we would get the following expansion:<br>
  * √23 = 4 + 1 / (1 + 1 / (3 + 1 / (1 + 1 / (8 + ...))))
  * <p>
  * The process can be summarised as follows:<br>
  * a<sub>0</sub> = 4, 1 / (√23 - 4) = (√23 + 4) / 7 = 1 + (√23 - 3) / 7<br>
  * a<sub>1</sub> = 1, 7 / (√23 - 3) = 7 * (√23 + 3) / 14 = 3 + (√23 - 3) / 2<br>
  * a<sub>2</sub> = 3, 2 / (√23 - 3) = 2 * (√23 + 3) / 14 = 1 + (√23 - 4) / 7<br>
  * a<sub>3</sub> = 1, 7 / (√23 - 4) = 7 * (√23 + 4) / 7 = 8 + √23 - 4<br>
  * a<sub>4</sub> = 8, 1 / (√23 - 4) = (√23 + 4) / 7 = 1 + (√23 - 3) / 7<br>
  * a<sub>5</sub> = 1, 7 / (√23 - 3) = 7 * (√23 + 3) / 14 = 3 + (√23 - 3) / 2<br>
  * a<sub>6</sub> = 3, 2 / (√23 - 3) = 2 * (√23 + 3) / 14 = 1 + (√23 - 4) / 7<br>
  * a<sub>7</sub> = 1, 7 / (√23 - 4) = 7 * (√23 + 4) / 7 = 8 + √23 - 4<br>
  * <p>
  * It can be seen that the sequence is repeating. For conciseness, we use the notation √23 = [4;(1,3,1,8)], to indicate that the block (1,3,1,8) repeats indefinitely.
  * <p>
  * The first ten continued fraction representations of (irrational) square roots are:
  * <p>
  * √2=[1;(2)], period=1<br>
  * √3=[1;(1,2)], period=2<br>
  * √5=[2;(4)], period=1<br>
  * √6=[2;(2,4)], period=2<br>
  * √7=[2;(1,1,1,4)], period=4<br>
  * √8=[2;(1,4)], period=2<br>
  * √10=[3;(6)], period=1<br>
  * √11=[3;(3,6)], period=2<br>
  * √12= [3;(2,6)], period=2<br>
  * √13=[3;(1,1,1,1,6)], period=5
  * <p>
  * Exactly four continued fractions, for N ≤ 13, have an odd period.
  * <p>
  * How many continued fractions for N ≤ 10000 have an odd period?
  */
object OddPeriodSquareRoots extends Problem {
  def solve(max: Int): Int = {
    def hasOddPeriod(n: Int): Boolean = {
      val a_0 = math.sqrt(n).floor.toInt
      val num = 1
      val denom = a_0

      @tailrec
      def findPeriod(expansion: List[(Int, Int, Int)]): Int = {
        val a = expansion.last._1
        val num = expansion.last._2
        val denom = expansion.last._3

        // a_x = a, num / (√n - denom)
        // a0 = 4, 1 / (√23 - 4), a = 4, num = 1, denom = 4, nextNum = 7, nextA = 1, nextDenom = 3
        // a1 = 1, 7 / (√23 - 3), a = 1, num = 7, denom = 3, nextNum = 2, nextA = 3, nextDenom = 3
        // a2 = 3, 2 / (√23 - 3), a = 3, num = 2, denom = 3, nextNum = 7, nextA = 1, nextDenom = 4
        val nextNum = (n - denom * denom) / num
        val nextA = (a_0 + denom) / nextNum
        val nextDenom = nextA * nextNum - denom

        val nextStep = (nextA, nextNum, nextDenom)
        val indexOfStep = expansion.indexOf(nextStep)

        if (indexOfStep == -1) findPeriod(expansion :+ nextStep)
        else expansion.size - indexOfStep
      }

      if (a_0 * a_0 == n) false
      else findPeriod(List((a_0, num, denom))) % 2 == 1
    }

    (1 to max).count(hasOddPeriod)
  }

  override def solve(): Any = solve(10000)

  override def tags(): Set[Tag] = Set(Tag.CONTINUED_FRACTIONS)

  override def answer(): Any = 1322
}
