package com.euler.problems.p006x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Powerful digit counts<br>
  * Problem 63
  * <p>
  * The 5-digit number, 16807=7<sup>5</sup>, is also a fifth power. Similarly, the 9-digit number, 134217728=8<sup>9</sup>, is a ninth power.
  * <p>
  * How many n-digit positive integers exist which are also an nth power?
  */
object PowerfulDigitCounts extends Problem {

  def countPowerfulDigit(i: Int) = Stream.from(1).takeWhile(power => BigInt(i).pow(power).toString.length == power).size

  override def solve() = Stream.from(1).takeWhile(scala.math.log10(_) < 2).map(countPowerfulDigit).sum

  override def tags(): Set[Tag] = Set(Tag.DIGITS)

  override def answer(): Any = 49
}
