package com.euler.problems.p038x

import com.euler.common.NumberConstant
import com.euler.common.fast.FastPrimes
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * (prime-k) factorial<br>
  * Problem 381
  * <p>
  * For a prime p let S(p) = (∑(p-k)!) mod(p) for 1 ≤ k ≤ 5.
  * <p>
  * For example, if p=7,<br>
  * (7-1)! + (7-2)! + (7-3)! + (7-4)! + (7-5)! = 6! + 5! + 4! + 3! + 2! = 720+120+24+6+2 = 872.<br>
  * As 872 mod(7) = 4, S(7) = 4.
  * <p>
  * It can be verified that ∑S(p) = 480 for 5 ≤ p < 100.
  * <p>
  * Find ∑S(p) for 5 ≤ p < 10<sup>8</sup>.
  */
object PrimeKFactorial extends Problem {
  def solve(limit: Int): Long =  {
    val fastPrimes = FastPrimes(limit)

    def getModN4(n: Int, dividend: Long): Long =
      if ((n - 1) % 3 == 0) n - dividend / 3
      else dividend / 3 + 1

    def getModN5(n: Int, dividend: Long): Long = ((n - 1) % 4, dividend % 4) match {
      case (_, 0) => n - dividend / 4
      case (0, mod) => mod * (n - 1) / 4 - dividend / 4
      case (_, 3) => (n - 1) / 4 - dividend / 4
      case (_, mod) => (n - 1) / 4 + ((n - 1) / 4 + 1) * (2 / mod) - dividend / 4
    }

    // modN1 = (n - 1)! mod n = n - 1
    // modN2 = (n - 2)! mod n = 1
    // modN3 = (n - 3)! mod n = (n - 1) / 2
    // modN4 = (n - 4)! mod n
    // modN5 = (n - 5)! mod n
    def sumOfRemainders(n: Int): Long = {
      val modN3 = (n - 1) / 2
      val modN4 = getModN4(n, modN3)
      val modN5 = getModN5(n, modN4)
      // modN1 + modN2 = n - 1 + 1 = n (% n == 0)
      (modN3 + modN4 + modN5) % n
    }

    (5 until limit).filter(fastPrimes.isPrime).map(n => sumOfRemainders(n)).sum
  }

  override def solve(): Any = solve(100 * NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.COMBINATORICS, Tag.PRIMES)

  override def answer(): Any = 139602943319822L
}
