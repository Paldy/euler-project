package com.euler.problems.p038x

import com.euler.common.NumberConstant
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

import scala.collection.mutable
import com.euler.utils._

/**
  * Harshad Numbers<br>
  * Problem 387
  * <p>
  * A Harshad or Niven number is a number that is divisible by the sum of its digits.<br>
  * 201 is a Harshad number because it is divisible by 3 (the sum of its digits.)<br>
  * When we truncate the last digit from 201, we get 20, which is a Harshad number.<br>
  * When we truncate the last digit from 20, we get 2, which is also a Harshad number.<br>
  * Let's call a Harshad number that, while recursively truncating the last digit, always results in a Harshad number a right truncatable Harshad number.
  * <p>
  * Also:<br>
  * 201/3=67 which is prime.<br>
  * Let's call a Harshad number that, when divided by the sum of its digits, results in a prime a strong Harshad number.
  * <p>
  * Now take the number 2011 which is prime.<br>
  * When we truncate the last digit from it we get 201, a strong Harshad number that is also right truncatable.<br>
  * Let's call such primes strong, right truncatable Harshad primes.
  * <p>
  * You are given that the sum of the strong, right truncatable Harshad primes less than 10000 is 90619.
  * <p>
  * Find the sum of the strong, right truncatable Harshad primes less than 10<sup>14</sup>.
  */
object HarshadNumbers extends Problem {
  def solve(digits: Int): Long = {
    val strongHarshadNumbers = mutable.MutableList[Long]()

    def generateHarshads(harshad: Long, digitSum: Int, digitsLeft: Int): Unit = {
      if (digitsLeft > 0) {
        NumberConstant.DecimalDigits
          .map(d => (harshad * 10 + d, digitSum + d))
          .filter(h => h._1 % h._2 == 0)
          .foreach(h => {
            if ((h._1 / h._2).isPrime) strongHarshadNumbers += h._1
            generateHarshads(h._1, h._2, digitsLeft - 1)
          })
      }
    }

    NumberConstant.DecimalDigitsWoZero.foreach(d => generateHarshads(d, d, digits - 2))

    (for (harshad <- strongHarshadNumbers;
          lastDigit <- List(1, 3, 7, 9))
      yield harshad * 10 + lastDigit)
        .filter(_.isPrime)
        .sum
  }

  override def solve(): Any = solve(14)

  override def tags(): Set[Tag] = Set(Tag.PRIMES, Tag.DIGITS, Tag.HARSHAD_NUMBERS)

  override def answer(): Any = 696067597313468L
}
