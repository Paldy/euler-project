package com.euler.problems.p049x

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

/**
  * Problem 493
  * <p>
  * 70 colored balls are placed in an urn, 10 for each of the seven rainbow colors.
  * <p>
  * What is the expected number of distinct colors in 20 randomly picked balls?
  * <p>
  * Give your answer with nine digits after the decimal point (a.bcdefghij).
  */
object UnderTheRainbow extends Problem {
  def solve(colors: Int, balls: Long, picked: Int) = {

    val urn = for (color <- 1 to colors; ball <- 1L to balls) yield color

    val stats = urn
      .combinations(picked).foldLeft((BigInt(0), BigInt(0)))((s, c) => {
        val times = (1 to colors).map(color => balls.choose(c.count(_ == color))).product
        val numberOfColors = c.toSet.size
        (s._1 + times, s._2 + times * numberOfColors)
      })

    (BigDecimal(stats._2) / BigDecimal(stats._1)).setScale(9, BigDecimal.RoundingMode.FLOOR)
  }

  override def solve(): Any = solve(7, 10, 20)

  override def tags(): Set[Tag] = Set(Tag.COMBINATORICS)

  override def answer(): Any = "6.818741802"
}