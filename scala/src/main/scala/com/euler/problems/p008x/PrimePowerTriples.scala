package com.euler.problems.p008x

import com.euler.common.NumberConstant._
import com.euler.common.fast.FastPrimes
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

import scala.collection.mutable

/**
  * Prime power triples<br>
  * Problem 87
  * <p>
  * The smallest number expressible as the sum of a prime square, prime cube, and prime fourth power is 28. In fact, there are exactly four numbers below fifty that can be expressed in such a way:
  * <p>
  * 28 = 2<sup>2</sup> + 2<sup>3</sup> + 2<sup>4</sup><br>
  * 33 = 3<sup>2</sup> + 2<sup>3</sup> + 2<sup>4</sup><br>
  * 49 = 5<sup>2</sup> + 2<sup>3</sup> + 2<sup>4</sup><br>
  * 47 = 2<sup>2</sup> + 3<sup>3</sup> + 2<sup>4</sup>
  * <p>
  * How many numbers below fifty million can be expressed as the sum of a prime square, prime cube, and prime fourth power?
  */
object PrimePowerTriples extends Problem {
  def solve(limit: Int, powers: List[Int]): Int = {
    val powersSorted = powers.sorted.reverse
    val fastPrimes = FastPrimes(limit)

    val primePowers = mutable.Map[(Int, Int), Int]()
    powersSorted.foreach(power => {
      Stream.from(2)
        .filter(fastPrimes.isPrime)
        .map(n => (n, BigInt(n).pow(power).toInt))
        .takeWhile(_._2 < limit)
        .foreach(nPowered => {
          primePowers((nPowered._1, power)) = nPowered._2
        })
    })

    def getMax(limit: Int, power: Int): Int = {
      Stream.from(1)
        .filter(fastPrimes.isPrime)
        .takeWhile(n => primePowers.getOrElse((n, power), limit+1) <= limit)
        .last
    }

    val primePowerTriples = mutable.BitSet()

    def countGroups(limit: Int, powers: List[Int], sum: Int = 0): Unit = powers match {
      case Nil => 0
      case power :: Nil =>
        (1 to getMax(limit, power))
          .filter(fastPrimes.isPrime)
          .foreach(n => {
            primePowerTriples += (sum + primePowers((n, power)))
          })
      case power :: pp =>
        (1 to getMax(limit, power))
          .filter(fastPrimes.isPrime)
          .foreach(n => {
            countGroups(limit - primePowers((n, power)), pp, sum + primePowers((n, power)))
          })
    }

    countGroups(limit, powersSorted)

    primePowerTriples.size
  }

  override def solve(): Any = solve(50 * Million, List(2, 3, 4))

  override def tags(): Set[Tag] = Set(Tag.PRIMES)

  override def answer(): Any = 1097343
}
