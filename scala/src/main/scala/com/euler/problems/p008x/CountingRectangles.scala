package com.euler.problems.p008x

import com.euler.common.NumberConstant
import com.euler.format.Problem

import scala.annotation.tailrec

/**
  * Counting rectangles<br>
  * Problem 85
  * <p>
  * By counting carefully it can be seen that a rectangular grid measuring 3 by 2 contains eighteen rectangles:
  * <p>
  * Although there exists no rectangular grid that contains exactly two million rectangles, find the area of the grid with the nearest solution.
  */
object CountingRectangles extends Problem {

  def solve(desiredRectangles: Int): Int = {
    def countRectangles(a: Int, b: Int): Int = (for {
      x <- 1 to a
      y <- 1 to b
    } yield x * y).sum

    @tailrec
    def findNearestSide(a: Int, nearest: (Int, Int), b: Int = 1): (Int, Int) = {
      val rectanglesDifference = countRectangles(a, b) - desiredRectangles
      val rectanglesDistance = math.abs(rectanglesDifference)
      if (a < b || rectanglesDifference > nearest._2) nearest
      else if (rectanglesDistance < nearest._2) findNearestSide(a, (a * b, rectanglesDistance), b + 1)
      else findNearestSide(a, nearest, b +1)
    }

    def findNearestArea(a: Int = 2, nearest: (Int, Int) = (1, desiredRectangles)): Int = {
      if (countRectangles(a, 1) - desiredRectangles > nearest._2) nearest._1
      else findNearestArea(a + 1, findNearestSide(a, nearest))
    }

    findNearestArea()
  }

  override def solve(): Any = solve(2 * NumberConstant.Million)

  override def answer(): Any = 2772
}
