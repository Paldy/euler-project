package com.euler.problems.p008x

import com.euler.format.Problem

import scala.annotation.tailrec
import scala.io.Source

/**
  * Path sum: two ways<br>
  * Problem 81
  * <p>
  * In the 5 by 5 matrix below, the minimal path sum from the top left to the bottom right, by only moving to the right and down, is indicated in bold red and is equal to 2427.
  * <p>
  * 	131	673	234	103	18	<br>
  * 	201	96	342	965	150	<br>
  * 	630	803	746	422	111	<br>
  * 	537	699	497	121	956	<br>
  * 	805	732	524	37	331
  * <p>
  * <p>
  * Find the minimal path sum, in matrix.txt (right click and "Save Link/Target As..."), a 31K text file containing a 80 by 80 matrix, from the top left to the bottom right by only moving right and down.
  */
object PathSumTwoWays extends Problem {
  def loadRows(row: String) = row.split(",").map(_.toLong)

  def loadMatrix(input: String) = input.split("\n").map(loadRows)

  def findMinPathInMatrix(matrix: Array[Array[Long]]): Long = {
    @tailrec
    def tailReduce(matrix: Array[Array[Long]]): Long = {
      if (matrix.length == 1) matrix.head.last
      else {
        matrix.head.indices.foreach(
          i => matrix.tail.head(i) += (if (i == 0) matrix.head(i) else scala.math.min(matrix.head(i), matrix.tail.head(i - 1)))
        )
        tailReduce(matrix.tail)
      }
    }

    matrix.head.indices.foreach(i => if (i != 0) matrix.head(i) += matrix.head(i - 1))
    tailReduce(matrix)
  }

  def solve(input: String): Long = findMinPathInMatrix(loadMatrix(input))

  override def solve(): Any = solve(Source.fromFile("resources/input/p081_matrix.txt").getLines.mkString("\n"))

  override def answer(): Any = 427337
}
