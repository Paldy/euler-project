package com.euler.problems.p008x

import com.euler.common.graph.Algorithms
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  * Path sum: four ways<br>
  * Problem 83
  * <p>
  * NOTE: This problem is a significantly more challenging version of Problem 81.
  * <p>
  * In the 5 by 5 matrix below, the minimal path sum from the top left to the bottom right, by moving left, right, up, and down, is indicated in bold red and is equal to 2297.<br>
  * 131	673	234	103	18<br>
  * 201	96	342	965	150<br>
  * 630	803	746	422	111<br>
  * 537	699	497	121	956<br>
  * 805	732	524	37	331<br>
  * <p>
  * Find the minimal path sum, in matrix.txt (right click and "Save Link/Target As..."), a 31K text file containing a 80 by 80 matrix, from the top left to the bottom right by moving left, right, up, and down.
  */
object PathSumFourWays extends Problem {
  def loadRows(row: String) = row.split(",").map(_.toInt)

  def loadMatrix(input: String) = input.split("\n").map(loadRows)

  def findMinPathInMatrix(matrix: Array[Array[Int]]): Int = {
    val numberOfRows = matrix.length
    val numberOfCols = matrix.head.length

    val end = numberOfRows * numberOfCols

    val graph = mutable.Map[Int, List[(Int, Int)]]()
    graph += end -> Nil

    for {
      row <- matrix.indices
      col <- matrix(row).indices
    } {
      val index = row * numberOfCols + col
      val value = matrix(row)(col)

      var nextEdges = ArrayBuffer[(Int, Int)]()

      if (row == numberOfRows - 1 && col == numberOfCols - 1) nextEdges += ((value, end))

      if (col != 0) nextEdges += ((value, index - 1))
      if (col != numberOfCols - 1) nextEdges += ((value, index + 1))

      if (row != 0) nextEdges += ((value, index - numberOfCols))
      if (row != numberOfRows - 1) nextEdges += ((value, index + numberOfCols))

      graph(index) = nextEdges.toList
    }

    Algorithms.dijkstra[Int](graph.toMap, 0, end)._1
  }

  def solve(input: String): Int = findMinPathInMatrix(loadMatrix(input))

  override def solve(): Any = solve(Source.fromFile("resources/input/p083_matrix.txt").getLines.mkString("\n"))

  override def tags(): Set[Tag] = Set(Tag.GRAPH)

  override def answer(): Any = 425185
}
