package com.euler.problems.p008x

import java.math.MathContext

import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}

/**
  * Square root digital expansion<br>
  * Problem 80
  * <p>
  * It is well known that if the square root of a natural number is not an integer, then it is irrational. The decimal expansion of such square roots is infinite without any repeating pattern at all.
  * <p>
  * The square root of two is 1.41421356237309504880..., and the digital sum of the first one hundred decimal digits is 475.
  * <p>
  * For the first one hundred natural numbers, find the total of the digital sums of the first one hundred decimal digits for all the irrational square roots.
  */
object SquareRootDigitalExpansion extends Problem {
  def solve(max: Int, digits: Int): Int = {
    val mc = new MathContext(digits + math.log(math.sqrt(max)).ceil.toInt + 1)

    def sqrt(n: BigDecimal, precision: BigDecimal) = {
      def sqrtIter(guess: BigDecimal): BigDecimal =
        if (isGoodEnough(guess)) guess
        else sqrtIter(improve(guess))

      def improve(guess: BigDecimal) = {
        (guess + n / guess) / 2
      }

      def isGoodEnough(guess: BigDecimal) =
        (guess * guess - n).abs < precision

      sqrtIter(BigDecimal(1, mc))
    }

    def sumOfDigits(n: BigDecimal): Int =
      n.toString
        .filter(_ != '.')
        .take(digits)
        .map(_.asDigit)
        .sum

    def isSquare: (Int) => Boolean = {
      n => {
        val sqrt = math.sqrt(n).toInt
        sqrt * sqrt != n
      }
    }

    val precision = BigDecimal(10, mc).pow(-digits)

    (1 to max)
      .filter(isSquare)
      .map(n => sqrt(BigDecimal(n, mc), precision))
      .map(sumOfDigits)
      .sum
  }

  override def solve(): Any = solve(100, 100)

  override def tags(): Set[Tag] = Set(Tag.DIGITS)

  override def answer(): Any = 40886
}
