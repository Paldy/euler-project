package com.euler.problems.p008x

import com.euler.common.numerals.Roman
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

import scala.io.Source

/**
  * Roman numerals<br>
  * Problem 89
  * <p>
  * For a number written in Roman numerals to be considered valid there are basic rules which must be followed. Even though the rules allow some numbers to be expressed in more than one way there is always a "best" way of writing a particular number.
  * <p>
  * For example, it would appear that there are at least six ways of writing the number sixteen:
  * <p>
  * IIIIIIIIIIIIIIII<br>
  * VIIIIIIIIIII<br>
  * VVIIIIII<br>
  * XIIIIII<br>
  * VVVI<br>
  * XVI
  * <p>
  * However, according to the rules only XIIIIII and XVI are valid, and the last example is considered to be the most efficient, as it uses the least number of numerals.
  * <p>
  * The 11K text file, roman.txt (right click and 'Save Link/Target As...'), contains one thousand numbers written in valid, but not necessarily minimal, Roman numerals; see About... Roman Numerals for the definitive rules for this problem.
  * <p>
  * Find the number of characters saved by writing each of these in their minimal form.
  * <p>
  * Note: You can assume that all the Roman numerals in the file contain no more than four consecutive identical units.
  */
object RomanNumerals extends Problem {
  def solve(romanNumerals: List[String]) = {
    val originalLength = romanNumerals.mkString("").length
    val newLength = romanNumerals.map(Roman.parse).filter(_.isDefined).map(n => Roman.create(n.get)).mkString("").length

    originalLength - newLength
  }

  override def solve(): Any = solve(Source.fromFile("resources/input/p089_roman.txt").getLines.toList)

  override def tags(): Set[Tag] = Set(Tag.ROMAN_NUMERALS)

  override def answer(): Any = 743
}
