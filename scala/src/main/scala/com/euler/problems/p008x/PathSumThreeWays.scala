package com.euler.problems.p008x

import com.euler.common.graph.Algorithms
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  * Path sum: three ways<br>
  * Problem 82
  * <p>
  * NOTE: This problem is a more challenging version of Problem 81.
  * <p>
  * The minimal path sum in the 5 by 5 matrix below, by starting in any cell in the left column and finishing in any cell in the right column, and only moving up, down, and right, is indicated in red and bold; the sum is equal to 994.<br>
  * 131	673	234	103	18<br>
  * 201	96	342	965	150<br>
  * 630	803	746	422	111<br>
  * 537	699	497	121	956<br>
  * 805	732	524	37	331<br>
  * <p>
  * Find the minimal path sum, in matrix.txt (right click and "Save Link/Target As..."), a 31K text file containing a 80 by 80 matrix, from the left column to the right column.
  */
object PathSumThreeWays extends Problem {
  def loadRows(row: String) = row.split(",").map(_.toInt)

  def loadMatrix(input: String) = input.split("\n").map(loadRows)

  def findMinPathInMatrix(matrix: Array[Array[Int]]): Int = {
    val numberOfRows = matrix.length
    val numberOfCols = matrix.head.length

    val start = -1
    val end = numberOfRows * numberOfCols

    val graph = mutable.Map[Int, List[(Int, Int)]]()
    graph += start -> Nil
    graph += end -> Nil

    for {
      row <- matrix.indices
      col <- matrix(row).indices
    } {
      val index = row * numberOfCols + col
      val value = matrix(row)(col)
      if (col == 0) graph(start) = (0, index) :: graph(start)

      var nextEdges = ArrayBuffer[(Int, Int)]()

      if (col == numberOfCols - 1) nextEdges += ((value, end))
      else nextEdges += ((value, row * numberOfCols + col + 1))

      if (row != 0) nextEdges += ((value, row * numberOfCols - numberOfCols + col))
      if (row != numberOfRows - 1) nextEdges += ((value, row * numberOfCols + numberOfCols + col))

      graph(index) = nextEdges.toList
    }

    Algorithms.dijkstra[Int](graph.toMap, start, end)._1
  }

  def solve(input: String): Int = findMinPathInMatrix(loadMatrix(input))

  override def solve(): Any = solve(Source.fromFile("resources/input/p082_matrix.txt").getLines.mkString("\n"))

  override def tags(): Set[Tag] = Set(Tag.GRAPH)

  override def answer(): Any = 260324
}
