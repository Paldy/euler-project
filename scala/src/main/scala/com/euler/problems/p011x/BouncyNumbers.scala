package com.euler.problems.p011x

import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag
import com.euler.problems.p011x.BouncyNumbers.GenerateState.GenerateState
import com.euler.utils._

import scala.annotation.tailrec

/**
  * Bouncy numbers<br>
  * Problem 112
  * <p>
  * Working from left-to-right if no digit is exceeded by the digit to its left it is called an increasing number; for example, 134468.
  * <p>
  * Similarly if no digit is exceeded by the digit to its right it is called a decreasing number; for example, 66420.
  * <p>
  * We shall call a positive integer that is neither increasing nor decreasing a "bouncy" number; for example, 155349.
  * <p>
  * Clearly there cannot be any bouncy numbers below one-hundred, but just over half of the numbers below one-thousand (525) are bouncy. In fact, the least number for which the proportion of bouncy numbers first reaches 50% is 538.
  * <p>
  * Surprisingly, bouncy numbers become more and more common and by the time we reach 21780 the proportion of bouncy numbers is equal to 90%.
  * <p>
  * Find the least number for which the proportion of bouncy numbers is exactly 99%.
  */
object BouncyNumbers extends Problem {
  object GenerateState extends Enumeration {
    type GenerateState = Value

    val Decreasing = Value
    val NotDecided = Value
    val Increasing = Value
  }

  def isBouncy(n: Long): Boolean = {
    def isBouncyDigits(digits: IndexedSeq[Int], state: GenerateState = GenerateState.NotDecided): Boolean =
      (digits.size, state) match {
        case (1, _) => false
        case (_, GenerateState.Decreasing) =>
          if (digits.head < digits.tail.head) true
          else isBouncyDigits(digits.tail, GenerateState.Decreasing)
        case (_, GenerateState.Increasing) =>
          if (digits.head > digits.tail.head) true
          else isBouncyDigits(digits.tail, GenerateState.Increasing)
        case (_, GenerateState.NotDecided) =>
          if (digits.head < digits.tail.head) isBouncyDigits(digits.tail, GenerateState.Increasing)
          else if (digits.head == digits.tail.head) isBouncyDigits(digits.tail, GenerateState.NotDecided)
          else isBouncyDigits(digits.tail, GenerateState.Decreasing)
      }

    isBouncyDigits(n.toDigits)
  }

  def solve(proportion: Int): Int = {
    @tailrec
    def findProportion(n: Int, bounces: Int = 0): Int = {
      val newBounces = if (isBouncy(n)) bounces + 1 else bounces
      if (newBounces * 100 % n == 0 && newBounces * 100 / n == proportion) n
      else findProportion(n + 1, newBounces)
    }

    findProportion(1)
  }

  override def solve(): Any = solve(99)

  override def tags(): Set[Tag] = Set(Tag.DIGITS)

  override def answer(): Any = 1587000
}
