package com.euler.problems.p034x

import com.euler.common.NumberConstant
import com.euler.format.{Problem, Tag}
import com.euler.format.Tag.Tag

import scala.annotation.tailrec

/**
  * Strong Repunits<br>
  * Problem 346
  * <p>
  * The number 7 is special, because 7 is 111 written in base 2, and 11 written in base 6<br>
  * (i.e. 7<sub>10</sub> = 11<sub>6</sub> = 111<sub>2</sub>). In other words, 7 is a repunit in at least two bases b > 1.
  * <p>
  * We shall call a positive integer with this property a strong repunit. It can be verified that there are 8 strong repunits below 50: {1,7,13,15,21,31,40,43}.<br>
  * Furthermore, the sum of all strong repunits below 1000 equals 15864.<br>
  * Find the sum of all strong repunits below 10<sup>12</sup>.
  */
object StrongRepunits extends Problem {
  def solve(limit: Long): Long = {
    def generateRepunitsInBase(base: Long): List[Long] = {
      @tailrec
      def inner(next: Long, powered: Long, repunits: List[Long]): List[Long] = {
        if (next >= limit) repunits
        else {
          val nextPowered = base * powered
          inner(nextPowered + next, nextPowered, next +: repunits)
        }
      }

      inner(base * base + base + 1, base * base, List())
    }

    1 + (2L to math.sqrt(limit).round)
      .flatMap(generateRepunitsInBase)
      .toSet
      .sum
  }

  override def solve(): Any = solve(100 * NumberConstant._10Digits)

  override def tags(): Set[Tag] = Set(Tag.POSITIONAL_NUMERAL_SYSTEMS)

  override def answer(): Any = 336108797689259276L
}
