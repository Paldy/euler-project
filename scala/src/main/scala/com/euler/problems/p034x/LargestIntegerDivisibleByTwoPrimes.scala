package com.euler.problems.p034x

import com.euler.common.NumberConstant
import com.euler.common.fast.FastPrimes
import com.euler.format.Tag.Tag
import com.euler.format.{Problem, Tag}
import com.euler.utils._

import scala.collection.mutable

/**
  * Largest integer divisible by two primes<br>
  * Problem 347
  * <p>
  * The largest integer ≤ 100 that is only divisible by both the primes 2 and 3 is 96, as 96=32*3=2<sup>5</sup>*3. For two distinct primes p and q let M(p,q,N) be the largest positive integer ≤N only divisible by both p and q and M(p,q,N)=0 if such a positive integer does not exist.
  * <p>
  * E.g. M(2,3,100)=96.<br>
  * M(3,5,100)=75 and not 90 because 90 is divisible by 2 ,3 and 5.<br>
  * Also M(2,73,100)=0 because there does not exist a positive integer ≤ 100 that is divisible by both 2 and 73.
  * <p>
  * Let S(N) be the sum of all distinct M(p,q,N). S(100)=2262.
  * <p>
  * Find S(10 000 000).
  */
object LargestIntegerDivisibleByTwoPrimes extends Problem {
  def solve(max: Int) = {
    val fastPrimes = FastPrimes(max)

    val largestIntegers = mutable.Map[(Int, Int), Int]()

    (6 to max)
      .foreach(n => {
        val primeFactorsDistinct = fastPrimes.computePrimeFactors(n).toList.distinct
        if (primeFactorsDistinct.size == 2) {
          val pq = (primeFactorsDistinct.head, primeFactorsDistinct(1))
          largestIntegers(pq) = n
        }
      })

    largestIntegers.values.sumLong()
  }

  override def solve(): Any = solve(10 * NumberConstant.Million)

  override def tags(): Set[Tag] = Set(Tag.PRIME_FACTORS)

  override def answer(): Any = 11109800204052L
}
