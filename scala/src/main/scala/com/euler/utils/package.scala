package com.euler

import com.euler.common.{FamousSequences, PrimeFactors}

import scala.annotation.tailrec
import scala.collection.GenTraversableOnce

package object utils {
  val ZeroLetter = 'A'.toInt - 1

  implicit class StringImprovements(s: String) {
    def isPalindrome = s.indices.take(s.length / 2).forall(i => s.charAt(i) == s.charAt(s.length - i - 1))
  }

  implicit class LongImprovements(l: Long) {
    lazy val isAbundant: Boolean = properDivisors.sum > l

    lazy val isAmicable: Boolean = {
      def sumOfProperDivisors(n: Long): Long = n.properDivisors.sum

      val sumOfProperDivisorsL = sumOfProperDivisors(l)

      l > 1 && sumOfProperDivisorsL != l && sumOfProperDivisors(sumOfProperDivisorsL) == l
    }

    lazy val isPalindrome: Boolean = l.toString.isPalindrome

    lazy val isPrime: Boolean = (l > 1) && FamousSequences.primes.takeWhile(p => p * p <= l).forall(l % _ != 0)

    lazy val divisors: List[Long] = primeFactors.allDivisors

    lazy val factorial: BigInt = factorialUntil(1)

    lazy val primeFactors: PrimeFactors[Long] = PrimeFactors(l)

    lazy val numberOfAllDivisors: Long = primeFactors.numberOfAllDivisors

    lazy val properDivisors: List[Long] = divisors.filter(_ != l)

    lazy val totient: Long = primeFactors.toMap.keys.foldLeft(l)((product, p) => product / p * (p - 1))

    lazy val toDigits: IndexedSeq[Int] = l.toString.map(_.asDigit)

    def choose(k: Long) = l.factorialUntil(scala.math.max(k, l - k)) / scala.math.min(k, l - k).factorial

    def foldDigits[B](z: B)(f: (B, Int) => B): B = {
      @tailrec
      def foldLeft(rest: Long = l, ret: B = z): B =
        if (rest == 0) ret
        else foldLeft(rest / 10, f(ret, (rest % 10).toInt))

      foldLeft()
    }

    def factorialUntil(min: Long): BigInt = {
      @tailrec
      def tailFactorial(n: BigInt, r: BigInt = 1): BigInt =
        if (n <= min) r
        else tailFactorial(n - 1, r * n)

      tailFactorial(l)
    }

    def highestCommonDivisor(other: Long): Long = {
      @tailrec
      def hcd(a: Long, b: Long): Long = b match {
        case 0 => a
        case _ => hcd(b, a % b)
      }

      hcd(l, other)
    }

    def leastCommonMultiple(other: Long): Long = primeFactors.leastCommonMultiple(other).toLong
  }

  implicit class BigIntImprovements(b: BigInt) {
    lazy val toDigits: IndexedSeq[Int] = b.toString.map(_.asDigit)
  }

  implicit class IndexedSeqIntImprovements(l: IndexedSeq[Int]) {
    lazy val toBigInt: BigInt = {
      def innerToNum(l: IndexedSeq[Int], r: BigInt): BigInt = {
        if (l.isEmpty) r
        else innerToNum(l.tail, r * 10 + l.head)
      }

      innerToNum(l, 0)
    }

    lazy val toLong: Long = toBigInt.toLong
  }

  implicit class CharImprovements(c: Char) {
    lazy val letterIndex: Int = c.toInt - ZeroLetter
  }

  implicit class SetImprovements[A](s: Set[A]) {
    // http://rosettacode.org/wiki/Power_set#Scala
    lazy val powerSet: Set[Set[A]] = s.foldLeft(Set(Set.empty[A])) { case (ss, el) => ss ++ ss.map(_ + el) } - Set.empty[A]
  }

  implicit class GenTraversableOnceIntImprovements(s: GenTraversableOnce[Int]) {
    def sumLong(): Long = s.foldLeft(0L)(_ + _)
  }
}
