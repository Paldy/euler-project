package com.euler.common.poker

import com.euler.common.poker.CardSuit.CardSuit
import com.euler.common.poker.CardValue.CardValue

import scala.annotation.switch

case class Card(suit: CardSuit, value: CardValue)

object Card {
  def apply(s: String): Option[Card] =
    if (s.length != 2) None
    else for (suit <- parseSuit(s(1)); value <- parseValue(s(0))) yield Card(suit, value)

  def parseSuit(c: Char): Option[CardSuit] = (c: @switch) match {
    case 'H' => Some(CardSuit.HEARTS)
    case 'D' => Some(CardSuit.DIAMONDS)
    case 'C' => Some(CardSuit.CLUBS)
    case 'S' => Some(CardSuit.SPADES)
    case _ => None
  }

  def parseValue(c: Char): Option[CardValue] = (c: @switch) match {
    case '2' => Some(CardValue._2)
    case '3' => Some(CardValue._3)
    case '4' => Some(CardValue._4)
    case '5' => Some(CardValue._5)
    case '6' => Some(CardValue._6)
    case '7' => Some(CardValue._7)
    case '8' => Some(CardValue._8)
    case '9' => Some(CardValue._9)
    case 'T' => Some(CardValue._10)
    case 'J' => Some(CardValue.JACK)
    case 'Q' => Some(CardValue.QUEEN)
    case 'K' => Some(CardValue.KING)
    case 'A' => Some(CardValue.ACE)
    case _ => None
  }
}