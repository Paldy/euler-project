package com.euler.common.poker

object CardSuit extends Enumeration {
  type CardSuit = Value

  val HEARTS,
  DIAMONDS,
  CLUBS,
  SPADES = Value
}
