package com.euler.common.poker

object CardValue extends Enumeration {
  type CardValue = Value

  val _2 = Value(2)
  val _3 = Value(3)
  val _4 = Value(4)
  val _5 = Value(5)
  val _6 = Value(6)
  val _7 = Value(7)
  val _8 = Value(8)
  val _9 = Value(9)
  val _10 = Value(10)
  val JACK = Value(11)
  val QUEEN = Value(12)
  val KING = Value(13)
  val ACE = Value(14)

  implicit class CardValueImprovements(cardValue: CardValue) {
    def isLooselyNext(that: CardValue): Boolean = cardValue.id + 1 == that.id || (cardValue == ACE && that == _2)
  }
}
