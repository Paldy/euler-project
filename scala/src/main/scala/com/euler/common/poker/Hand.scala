package com.euler.common.poker

import com.euler.common.poker.CardValue.CardValue
import com.euler.common.poker.Rank.Rank

case class Hand(card1: Card, card2: Card, card3: Card, card4: Card, card5: Card) extends Ordered[Hand] {

  private val sortedCards: List[Card] = List(card1, card2, card3, card4, card5).sortBy(_.value).reverse

  private var rank: Rank = Rank.HIGH_CARD

  private var processState = Rank.HIGH_CARD

  private var firstRepeatingValue: Option[CardValue] = None
  private var secondRepeatingValue: Option[CardValue] = None

  private var cardsRanked: List[CardValue] = sortedCards.map(_.value)

  private var isFlush = true

  private var isStraight = true

  private def processSameValues(cardValue: CardValue) = processState match {
    case Rank.HIGH_CARD =>
      processState = Rank.ONE_PAIR
      firstRepeatingValue = Some(cardValue)
    case Rank.ONE_PAIR =>
      if (firstRepeatingValue.get == cardValue) processState = Rank.THREE_OF_A_KIND
      else {
        processState = Rank.TWO_PAIR
        secondRepeatingValue = Some(cardValue)
      }
    case Rank.TWO_PAIR =>
      secondRepeatingValue = firstRepeatingValue
      firstRepeatingValue = Some(cardValue)
      processState = Rank.FULL_HOUSE
    case Rank.THREE_OF_A_KIND =>
      if (firstRepeatingValue.get == cardValue) processState = Rank.FOUR_OF_A_KIND
      else {
        secondRepeatingValue = Some(cardValue)
        processState = Rank.FULL_HOUSE
      }
  }

  var previousCardOption: Option[Card] = None
  for (card <- sortedCards) {
    for (previousCard <- previousCardOption) {
      isFlush &= (card.suit == previousCard.suit)

      isStraight &= card.value.isLooselyNext(previousCard.value)

      if (card.value == previousCard.value) processSameValues(card.value)
    }
    previousCardOption = Some(card)
  }

  if (processState != Rank.HIGH_CARD) rank = processState
  else if (isFlush && !isStraight) rank = Rank.FLUSH
  else if (isStraight && !isFlush) rank = Rank.STRAIGHT
  else if (isFlush && isStraight)
    if (cardsRanked(4) == CardValue._10) rank = Rank.ROYAL_FLUSH
    else rank = Rank.STRAIGHT_FLUSH

  rank match {
    case Rank.STRAIGHT | Rank.STRAIGHT_FLUSH =>
      if (cardsRanked(4) == CardValue._2) cardsRanked = cardsRanked.tail :+ cardsRanked.head
    case Rank.ONE_PAIR | Rank.THREE_OF_A_KIND | Rank.FOUR_OF_A_KIND =>
      cardsRanked = firstRepeatingValue.get +: cardsRanked.filter(_ != firstRepeatingValue.get)
    case Rank.TWO_PAIR =>
      cardsRanked = firstRepeatingValue.get +: (secondRepeatingValue.get +: cardsRanked.filter(c => c != firstRepeatingValue.get && c != secondRepeatingValue.get))
    case Rank.FULL_HOUSE =>
      cardsRanked = List(firstRepeatingValue.get, secondRepeatingValue.get)
    case _ => Nil
  }

  private def compareCardsValue(cards1: List[CardValue], cards2: List[CardValue]): Int = cards1 match {
    case Nil => 0
    case c::cs if c != cards2.head => c.compare(cards2.head)
    case _ => compareCardsValue(cards1.tail, cards2.tail)
  }

  override def compare(that: Hand): Int = {
    if (rank == that.rank) compareCardsValue(cardsRanked, that.cardsRanked)
    else rank.compare(that.rank)
  }
}
