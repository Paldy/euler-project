package com.euler.common

case class Triangle (a: Long, b: Long, c: Long) {
  val toTriplet = (a, b, c)

  def isPythagorean: Boolean = a * a + b * b == c * c
}

object Triangle {

  def generateFromPerimeter(perimeter: Long) = {
    for (c <- perimeter / 3 to perimeter / 2; b <- (perimeter - c) / 2 to c
         if 2 * c + b > perimeter && 2 * b + c >= perimeter && 2 * c < perimeter)
      yield new Triangle(perimeter - b - c, b, c)
  }
}