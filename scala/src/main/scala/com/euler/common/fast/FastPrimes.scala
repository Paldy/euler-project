package com.euler.common.fast

import scala.annotation.tailrec

case class FastPrimes(max: Int) {
  private val smallestPrimeFactor = {
    val smallestPrimeFactor = Array.fill(max + 1)(1)

    (2 to math.sqrt(max + 1).toInt)
      .foreach(n => {
        (2 * n to max by n)
          .foreach(primeMultiple => if (smallestPrimeFactor(primeMultiple) == 1) smallestPrimeFactor(primeMultiple) = n)
      })

    smallestPrimeFactor
  }

  def totient(n: Int): Int =
    computePrimeFactors(n)
      .toList
      .distinct
      .foldLeft(n)((product, p) => product / p * (p - 1))

  def isPrime(n: Int): Boolean = smallestPrimeFactor(n) == 1 && n > 1

  def computePrimeFactors(n: Int): FastPrimeFactors = {
    @tailrec
    def primeFactors(n: Int, factors: List[Int] = List()): List[Int] = (n, smallestPrimeFactor(n)) match {
      case (1, _) => factors
      case (_, 1) => n :: factors
      case (_, spf) => primeFactors(n / spf, spf :: factors)
    }

    new FastPrimeFactors(primeFactors(n), this)
  }
}
