package com.euler.common.fast

import com.euler.common.PrimeFactors

import scala.annotation.tailrec

class FastPrimeFactors private[fast](pf: List[Int], private val fastPrimes: FastPrimes) extends PrimeFactors[Int] {
  override val toList: List[Int] = pf

  override lazy val numberOfAllDivisors: Int = toMap.foldLeft(1)((acc, kv) => acc * (kv._2 + 1))

  override lazy val allDivisors: List[Int] = {
    def generateDivisors(divisors: List[Int], pf: List[(Int, Int)]): List[Int] = {
      if (pf.isEmpty) divisors
      else generateDivisors(
        for (div <- divisors; pow <- 0L to pf.head._2)
          yield div * scala.math.pow(pf.head._1, pow).toInt
        , pf.tail
      )
    }

    generateDivisors(List(1), toMap.toList)
  }

  override lazy val toInt: Int = pf.product

  override lazy val toLong: Long = toInt.toLong

  override lazy val toMap: Map[Int, Int] = pf.groupBy(identity).map(l => l._1 -> l._2.size)

  override def leastCommonMultiple(n: Int): PrimeFactors[Int] = {
    @tailrec
    def innerLCM(list1: List[Int], list2: List[Int], ret: List[Int] = List()): List[Int] = (list1, list2) match {
      case (Nil, l) => ret ++ l
      case (l, Nil) => ret ++ l
      case (x :: xs, y :: ys) if x == y => innerLCM(xs, ys, x :: ret)
      case (x :: xs, y :: _) if x > y => innerLCM(xs, list2, x :: ret)
      case (x :: _, y :: ys) if x < y => innerLCM(list1, ys, y :: ret)
    }

    new FastPrimeFactors(innerLCM(pf, fastPrimes.computePrimeFactors(n).toList), fastPrimes)
  }
}