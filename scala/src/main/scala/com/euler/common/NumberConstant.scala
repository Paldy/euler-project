package com.euler.common

object NumberConstant {
  val Million = 1000000

  val Billion = 1000000000

  val _10Digits = 10000000000L

  val DecimalDigits = 0 to 9

  val DecimalDigitsWoZero = 1 to 9

  val MaxDigitFactorial = 2177280
}
