package com.euler.common

import com.euler.utils._

import scala.reflect.ClassTag

object Combinatorics {

  /**
    * If M is a finite multiset, then a multiset permutation is an ordered arrangement of elements of M in which
    * each element appears exactly as often as is its multiplicity in M. An anagram of a word having some repeated
    * letters is an example of a multiset permutation.
    *
    * @param ms multiplicities of elements in M
    */
  def computePermutationOfMultisets(ms: List[Long]): BigInt =
    ms.sum.factorial / ms.map(_.factorial).product

  def permutationsWithRepetition[T: ClassTag](elements: List[T], length: Int): List[List[T]] = length match {
    case 0 => List(List())
    case _ => for (element <- elements; permutation <- permutationsWithRepetition(elements, length - 1)) yield element +: permutation
  }
}
