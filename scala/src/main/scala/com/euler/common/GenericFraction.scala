package com.euler.common

import scala.annotation.tailrec

class GenericFraction[A](val numerator: A, val denominator: A)(implicit ev: Integral[A]) extends Fraction[A] {
  override def +(that: Fraction[A]): Fraction[A] =
    if (numerator == 0) that
    else if (that.numerator == 0) this
    else new GenericFraction[A](ev.plus(ev.times(numerator, that.denominator), ev.times(denominator, that.numerator)), ev.times(denominator, that.denominator))

  private lazy val hcd: A = {
    @tailrec
    def hcd(a: A, b: A): A = b match {
      case 0 => a
      case _ => hcd(b, ev.rem(a, b))
    }

    hcd(numerator, denominator)
  }

  override lazy val reciprocal: Fraction[A] = new GenericFraction[A](denominator, numerator)
  override lazy val reducedNumerator: A = ev.quot(numerator, hcd)
  override lazy val reducedDenominator: A = ev.quot(denominator, hcd)
}

object GenericFraction {
  def apply[A](numerator: A, denominator: A) ( implicit ev: Integral[A]): GenericFraction[A] = new GenericFraction[A](numerator, denominator)
  def apply[A](fraction: (A, A)) ( implicit ev: Integral[A]): GenericFraction[A] = new GenericFraction[A](fraction._1, fraction._2)
}