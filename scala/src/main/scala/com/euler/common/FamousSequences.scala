package com.euler.common

import java.time.LocalDate

import com.euler.utils._

import scala.reflect.ClassTag

object FamousSequences {
  def fibonacci(i: Long, j: Long): Stream[Long] = i #:: fibonacci(j, i + j)

  def fibonacciBigInt(i: BigInt, j: BigInt): Stream[BigInt] = i #:: fibonacciBigInt(j, i + j)

  lazy val primes = 2L #:: StreamLong.from(3, 2).filter(_.isPrime)

  lazy val composites = 4L #:: StreamLong.from(6).filter(!_.isPrime)

  private def generateStreamByFunction[T: ClassTag](f: Long => T): Stream[T] = {
    def inner(n: Long = 1): Stream[T] = f(n) #:: inner(n + 1)

    inner(1)
  }

  lazy val triangleNumbers = generateStreamByFunction(n => (n * n + n) / 2)
  lazy val squareNumbers = generateStreamByFunction(n => n * n)
  lazy val pentagonNumbers = generateStreamByFunction(n => (3 * n * n - n) / 2)
  lazy val hexagonNumbers = generateStreamByFunction(n => 2 * n * n - n)
  lazy val heptagonNumbers = generateStreamByFunction(n => n * (5 * n - 3) / 2)
  lazy val octagonNumbers = generateStreamByFunction(n => n * (3 * n - 2))

  lazy val abundants = (12L to 28123).filter(_.isAbundant)

  def days(from: LocalDate): Stream[LocalDate] = from #:: days(from.plusDays(1))
}
