package com.euler.common

trait Fraction[A] {
  def +(that: Fraction[A]): Fraction[A]

  val reciprocal: Fraction[A]

  val numerator: A
  val denominator: A
  val reducedNumerator: A
  val reducedDenominator: A

  override def hashCode(): Int = 31 * reducedNumerator.hashCode() + reducedDenominator.hashCode()

  override def equals(that: scala.Any): Boolean = that match {
    case that: Fraction[A] => that.reducedNumerator == reducedNumerator && that.reducedDenominator == reducedDenominator
    case _ => false
  }
}

object Fraction {
  def apply[A](numerator: A, denominator: A) ( implicit ev: Integral[A]): Fraction[A] = GenericFraction[A](numerator, denominator)
  def apply[A](fraction: (A, A)) ( implicit ev: Integral[A]): Fraction[A] = GenericFraction[A](fraction._1, fraction._2)
}
