package com.euler.common.numerals

import scala.annotation.tailrec

object Roman {
  def parse(roman: String): Option[Int] = {
    @tailrec
    def innerParse(roman: List[Char], prev: Char = 0, sum: Int = 0): Option[Int] = roman match {
      case 'I' :: 'V' :: rest if !"I".contains(prev) => innerParse(rest, 'V', sum + 4)
      case 'I' :: 'X' :: rest if !"IV".contains(prev) => innerParse(rest, 'X', sum + 9)
      case 'X' :: 'L' :: rest if !"IVX".contains(prev) => innerParse(rest, 'L', sum + 40)
      case 'X' :: 'C' :: rest if !"IVXL".contains(prev) => innerParse(rest, 'C', sum + 90)
      case 'C' :: 'D' :: rest if !"IVXLC".contains(prev) => innerParse(rest, 'D', sum + 400)
      case 'C' :: 'M' :: rest if !"IVXLCD".contains(prev) => innerParse(rest, 'M', sum + 900)
      case 'I' :: rest => innerParse(rest, 'I', sum + 1)
      case 'V' :: rest if !"I".contains(prev) => innerParse(rest, 'V', sum + 5)
      case 'X' :: rest if !"IV".contains(prev) => innerParse(rest, 'X', sum + 10)
      case 'L' :: rest if !"IVX".contains(prev) => innerParse(rest, 'L', sum + 50)
      case 'C' :: rest if !"IVXL".contains(prev) => innerParse(rest, 'C', sum + 100)
      case 'D' :: rest if !"IVXLC".contains(prev) => innerParse(rest, 'D', sum + 500)
      case 'M' :: rest if !"IVXLCD".contains(prev) => innerParse(rest, 'M', sum + 1000)
      case Nil => Some(sum)
      case _ => None
    }

    innerParse(roman.toList)
  }

  def create(number: Int): String = {
    @tailrec
    def innerCreate(number: Int, roman: String = ""): String = number match {
      case n if n >= 1000 => innerCreate(number - 1000, roman + "M")
      case n if n >= 900 => innerCreate(number - 900, roman + "CM")
      case n if n >= 500 => innerCreate(number - 500, roman + "D")
      case n if n >= 400 => innerCreate(number - 400, roman + "CD")
      case n if n >= 100 => innerCreate(number - 100, roman + "C")
      case n if n >= 90 => innerCreate(number - 90, roman + "XC")
      case n if n >= 50 => innerCreate(number - 50, roman + "L")
      case n if n >= 40 => innerCreate(number - 40, roman + "XL")
      case n if n >= 10 => innerCreate(number - 10, roman + "X")
      case n if n >= 9 => innerCreate(number - 9, roman + "IX")
      case n if n >= 5 => innerCreate(number - 5, roman + "V")
      case n if n >= 4 => innerCreate(number - 4, roman + "IV")
      case n if n >= 1 => innerCreate(number - 1, roman + "I")
      case _ => roman
    }

    innerCreate(number)
  }
}
