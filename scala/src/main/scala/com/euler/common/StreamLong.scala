package com.euler.common

object StreamLong {
  def from(start: Long, step: Long): Stream[Long] = start #:: from(start+step, step)

  def from(start: Long): Stream[Long] = from(start, 1)
}
