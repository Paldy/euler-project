package com.euler.common

trait PrimeFactors[A] {
  val numberOfAllDivisors: A

  val allDivisors: List[A]

  val toInt: Int

  val toList: List[A]

  val toLong: Long

  val toMap: Map[A, A]

  def leastCommonMultiple(n: A): PrimeFactors[A]
}

object PrimeFactors {
  def apply(n: Long): PrimeFactors[Long] = CommonPrimeFactors(n)
}

class CommonPrimeFactors private(pf: Map[Long, Long]) extends PrimeFactors[Long] {
  override lazy val numberOfAllDivisors = pf.foldLeft(1L)((acc, kv) => acc * (kv._2 + 1))

  override lazy val allDivisors = {
    def generateDivisors(divisors: List[Long], pf: List[(Long, Long)]): List[Long] = {
      if (pf.isEmpty) divisors
      else generateDivisors(
        for (div <- divisors; pow <- 0L to pf.head._2)
          yield div * scala.math.pow(pf.head._1, pow).toLong
        , pf.tail
      )
    }

    generateDivisors(List(1), pf.toList)
  }

  override lazy val toLong = pf.foldLeft(1L)((acc, kv) => acc * scala.math.pow(kv._1, kv._2).toLong)

  override val toMap = pf

  override def leastCommonMultiple(n: Long): CommonPrimeFactors =
    new CommonPrimeFactors(pf ++ CommonPrimeFactors.create(n).foldLeft(Map[Long, Long]())((m, kv) => m + (kv._1 -> scala.math.max(kv._2, pf.getOrElse(kv._1, 0L)))))

  override lazy val toInt: Int = toLong.toInt
  override lazy val toList: List[Long] = pf.toList.flatMap(kv => List.fill(kv._2.toInt)(kv._1)).sorted.reverse
}

object CommonPrimeFactors {

  def apply(n: Long): CommonPrimeFactors = new CommonPrimeFactors(create(n))

  private def create(n: Long) = {
    def primeFactors(n: Long, pf: Map[Long, Long] = Map.empty): Map[Long, Long] = {
      if (n == 1) pf
      else {
        val p = FamousSequences.primes.takeWhile(p => p * p <= n).find(n % _ == 0).getOrElse(n)
        primeFactors(n / p, pf + (p -> (pf.getOrElse(p, 0L) + 1)))
      }
    }

    primeFactors(n)
  }
}