package com.euler.common.graph

import scala.annotation.tailrec


object Algorithms {

  type Path[K] = (Int, List[K])

  /**
    * http://rosettacode.org/wiki/Dijkstra%27s_algorithm#Scala
    */
  def dijkstra[K](lookup: Map[K, List[(Int, K)]], start: K, end: K): Path[K] = {
    @tailrec
    def inner(lookup: Map[K, List[(Int, K)]], fringe: List[Path[K]], dest: K, visited: Set[K]): Path[K] =
      fringe match {
        case (dist, path) :: fringe_rest => path match {
          case key :: path_rest =>
            if (key == dest) (dist, path.reverse)
            else {
              val paths = lookup(key).flatMap { case (d, nextKey) => if (!visited.contains(nextKey)) List((dist + d, nextKey :: path)) else Nil }
              val sorted_fringe = (paths ++ fringe_rest).sortWith { case ((d1, _), (d2, _)) => d1 < d2 }
              inner(lookup, sorted_fringe, dest, visited + key)
            }
          case Nil => (0, List())
        }
        case Nil => (0, List())
      }

    inner(lookup, List((0, List(start))), end, Set())
  }
}
